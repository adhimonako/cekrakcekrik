<?php
class MY_Controller extends CI_Controller
{
	public $data = [];
	public $userdata;

	function __construct()
	{
		parent::__construct();
		$this->userdata = $this->session->userdata('user_fornt');
		$this->data['userdata'] = $this->userdata;
	}
}
