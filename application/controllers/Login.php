<?php 
class Login extends MY_Controller{
	function __construct(){
		parent::__construct();

		$this->load->model('m_pengunjung');
		$this->load->model('m_kontak');
		$this->load->model('model_datauser');
        $this->m_pengunjung->count_visitor();
	}

	function index(){
		if(!empty($this->userdata)){
			redirect('home','refresh');
		}

		$this->load->view('v_login',$this->data);
	}

	function login(){
		$post = $_POST;
		$username = $post['username'];
		$password = md5($post['password']);
		$user = $this->model_datauser->login($username,$password);
		if(!empty($user)){
			echo $this->session->set_flashdata('msg',"<div class='alert alert-success'>Login Sukses.</div>");
			$this->session->set_userdata(['user_fornt'=>$user]);
		}else{
			echo $this->session->set_flashdata('msg',"<div class='alert alert-danger'>User/Password Salah.</div>");
		}
		redirect('login');
	}

	function logout(){
        $this->session->sess_destroy();
		$this->session->set_userdata(['user_fornt'=>null]);
		echo $this->session->set_flashdata('msg',"<div class='alert alert-info'>Terimakasih.</div>");
		// var_dump('expression');die;
		redirect('login');
	}
}