<?php 
require_once(FCPATH . '/Veritrans.php');
Veritrans_Config::$isProduction = false;
Veritrans_Config::$serverKey = 'SB-Mid-server-DLILJrAEeMNF8Q7YYpffDKdw';


class Paynotif extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_datatagihan');
		$this->load->model('model_datapembayaran');
		$this->load->model('model_datareservasi');

	}

	function index(){
		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			 try {
			  $notif = new Veritrans_Notification();
			} catch (Exception $e) {
			  echo "Exception: ".$e->getMessage()."\r\n";
			  echo "Notification received: ".file_get_contents("php://input");
			  exit();
			} 
			// var_dump($notif);
			$transaction = $notif->transaction_status;
			$type = $notif->payment_type;
			$order_id = $notif->order_id;
			$fraud = $notif->fraud_status;

			if ($transaction == 'capture') {
			  // For credit card transaction, we need to check whether transaction is challenge by FDS or not
			  if ($type == 'credit_card'){
			    if($fraud == 'challenge'){
				      // TODO set payment status in merchant's database to 'Challenge by FDS'
				      // TODO merchant should decide whether this transaction is authorized or not in MAP
		  			$datax['statustagihan'] = 10;
		  			$arrIDTag = explode('T-',$order_id);
		  			$idtagihan = $arrIDTag[0];
		  			$save = $this->model_datatagihan->updatesave($datax,$idtagihan);
					if($save){
						$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
					}else{
						$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
					}
				    echo "Transaction order_id: " . $order_id ." is challenged by FDS";
			      }
			      else {
			      	$datax['statustagihan'] = 1;
		  			$arrIDTag = explode('T-',$order_id);
		  			$idtagihan = $arrIDTag[0];
		  			$save = $this->model_datatagihan->updatesave($datax,$idtagihan);
					if($save){
						$datatagihan = $this->model_datatagihan->detail($idtagihan);
						$data['idtagihan'] = $idtagihan;
						$data['nominal'] = $datatagihan['nominal'];
						$data['tglbayar'] = date('Y-m-d H:i:s');
						$data['tgl_dibuat'] = date('Y-m-d H:i:s');
						$data['dibuat'] = 'veritrans';
						$save = $this->model_datapembayaran->addsave($data);
						// ,b.idreservasi
				      	$datar['statusreservasi'] = 1;
						$save = $this->model_datareservasi->updatesave($datar,$datatagihan['idreservasi']);

						$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
					}else{
						$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
					}
					// T
				      // TODO set payment status in merchant's database to 'Success'
				      echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
			      }
			    }
			  }else if ($transaction == 'settlement'){
			      	$datax['statustagihan'] = 1;
		  			$arrIDTag = explode('T-',$order_id);
		  			$idtagihan = $arrIDTag[0];
		  			$save = $this->model_datatagihan->updatesave($datax,$idtagihan);
					if($save){
						$datatagihan = $this->model_datatagihan->detail($idtagihan);
						$data['idtagihan'] = $idtagihan;
						$data['nominal'] = $datatagihan['nominal'];
						$data['tglbayar'] = date('Y-m-d H:i:s');
						$data['tgl_dibuat'] = date('Y-m-d H:i:s');
						$data['dibuat'] = 'veritrans';
						$save = $this->model_datapembayaran->addsave($data);

						// ,b.idreservasi
				      	$datar['statusreservasi'] = 1;
						$save = $this->model_datareservasi->updatesave($datar,$datatagihan['idreservasi']);
						$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
					}else{
						$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
					}
				  // TODO set payment status in merchant's database to 'Settlement'
				  echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
			  }
			  else if($transaction == 'pending'){ //10
	  			$datax['statustagihan'] = 10;
	  			$arrIDTag = explode('T-',$order_id);
	  			$idtagihan = $arrIDTag[0];
	  			$save = $this->model_datatagihan->updatesave($datax,$idtagihan);
				if($save){
					$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
				}else{
					$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
				}
				// TODO set payment status in merchant's database to 'Pending'
				echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
			  }
			  else if ($transaction == 'deny') {
  		  			$datax['statustagihan'] = 0;
		  			$arrIDTag = explode('T-',$order_id);
		  			$idtagihan = $arrIDTag[0];
		  			$save = $this->model_datatagihan->updatesave($datax,$idtagihan);
					if($save){
						$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
					}else{
						$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
					}
				  // TODO set payment status in merchant's database to 'Denied'
				  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
			  }
			  else if ($transaction == 'expire') {
			  		$datax['statustagihan'] = 0;
		  			$arrIDTag = explode('T-',$order_id);
		  			$idtagihan = $arrIDTag[0];
		  			$save = $this->model_datatagihan->updatesave($datax,$idtagihan);
					if($save){
						$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
					}else{
						$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
					}
				  // TODO set payment status in merchant's database to 'expire'
				  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is expired.";
			  }
			  else if ($transaction == 'cancel') {
			  		$datax['statustagihan'] = 0;
		  			$arrIDTag = explode('T-',$order_id);
		  			$idtagihan = $arrIDTag[0];
		  			$save = $this->model_datatagihan->updatesave($datax,$idtagihan);
					if($save){
						$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
					}else{
						$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
					}
				  // TODO set payment status in merchant's database to 'Denied'
				  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
			}


		} else {

		    $order_id = $_GET['order_id'];
		    $statusCode = $_GET['status_code'];
		    $transaction  = $_GET['transaction_status'];

		    if($transaction == 'capture') {
		      echo "<p>Transaksi berhasil.</p>";
		      echo "<p>Status transaksi untuk order id : " . $order_id;

		    }
		    // Deny
		    else if($transaction == 'deny') {
		      echo "<p>Transaksi ditolak.</p>";
		      echo "<p>Status transaksi untuk order id .: " . $order_id;

		    }
		    // Challenge
		    else if($transaction == 'challenge') {
		      echo "<p>Transaksi challenge.</p>";
		      echo "<p>Status transaksi untuk order id : " . $order_id;

		    }
		    // Error
		    else {
		      echo "<p>Terjadi kesalahan pada data transaksi yang dikirim.</p>";
		      echo "<p>Status message: [$response->status_code] " . $transaction;
		    }

		}
	}

}