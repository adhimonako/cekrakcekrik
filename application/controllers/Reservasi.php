<?php 
class Reservasi extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('model_datareservasi');
		$this->load->model('model_datapaket');

        $this->m_pengunjung->count_visitor();
        if(empty($this->userdata)){
			echo $this->session->set_flashdata('msg',"<div class='alert alert-danger'>Silakan Login untuk melanjutkan reservasi.</div>");

        	redirect('login');
        }

	}

	function index(){
		$this->data['idpaket'] = $_REQUEST['idpaket'];
		$this->data['tampilpaket'] = $this->model_datapaket->tampilpaket()->result();

		$this->load->view('v_reservasi',$this->data);
	}

	function save(){
		$data['idpaket'] = $this->input->post('idpaket');
		$data['idcustomer'] = $this->userdata['idcustomer'];
		$data['jumlahpeserta'] = $this->input->post('jumlahpeserta');
		$data['tglkeberangkatan'] = $this->input->post('tglkeberangkatan');
		$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		$data['dibuat'] = 'frontend';

		$save = $this->model_datareservasi->addsave($data);
		if($save){			
			echo $this->session->set_flashdata('msg',"<div class='alert alert-success'>Berhasil melakukan reservasi, silakan lanjutkan pembayaran.</div>");
			redirect('history');
		}else{
			echo $this->session->set_flashdata('msg',"<div class='alert alert-danger'>Gagal melakukan reservasi.</div>");
			redirect('reservasi');
		}

	}

}