<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datapegawai extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function datapegawai()
	{
		$this->load->model('model_datapegawai');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_datapegawai->tampilpegawai()->result();
		$data['tampilkanjabatan']=$this->model_datapegawai->tampiljabatan()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/datapegawai',$data);
		$this->load->view('/admin/footer',$data);
	}

	public function addpegawai()
	{
		$this->load->model('model_datapegawai');
		$data = array('nama' => $this->input->post('nama'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'nip' => ($this->input->post('nip')),
					'nik' => ($this->input->post('nik')),
					'jk' => ($this->input->post('jk')),
					'tgllahir' => ($this->input->post('tgllahir')),
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'email' => $this->input->post('email'),
					'idjabatan' => ($this->input->post('idjabatan')),
					'foto' => ($this->input->post('foto')),

				);

		$this->session->set_flashdata('add_success', 'Pegawai Berhasil Ditambahkan');
		$this->model_datapegawai->addpegawai($data);
		redirect('admin/cont_datapegawai/datapegawai');
	}

	


	public function getpegawaiid(){
	$this->load->view('/admin/getpegawai');
		

	}



	public function ubahpegawai()
	{
		$this->load->model('model_datapegawai');
		$data = array('kodepegawai' => $this->input->post('kodepegawai'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'nama' => ($this->input->post('nama')),
					'jk' => ($this->input->post('jk')),
					'tgllahir' => $this->input->post('tgllahir'),
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'email' => $this->input->post('email'),
					'diubah' => $this->input->post('diubah'),
					'tgl_diubah' => $this->input->post('tgl_diubah')
				);
		$this->model_datapegawai->updatepegawai($data);
		$this->load->view('/admin/updatepegawai');
	}

	public function deletepegawai($idpegawai)
	{
		$this->load->model('model_datapegawai');
		
		$this->model_datapegawai->deletepegawai($idpegawai);
		redirect('admin/cont_datapegawai/datapegawai');
	}
}