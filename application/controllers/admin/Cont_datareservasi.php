<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datareservasi extends CI_Controller {

	function __construct(){
        parent::__construct();

		$this->data['controller_title'] = 'Reservasi';
		$this->data['controller_class'] = 'admin/Cont_datareservasi';
		$this->data['controller_index'] = 'admin/Cont_datareservasi/datareservasi';
		$this->load->model('model_datareservasi');
		$this->load->model('model_datapaket');
		$this->load->model('model_datacustomer');

	}

	public function datareservasi()
	{
		$filter = [];
		$status = $_POST['filter_status'];
		if(isset($status)){
			if($status != 'all'){
				$filter[] = " a.statusreservasi = '$status' ";
			}
			$this->data['filter_status'] = $status;
		}
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_datareservasi->tampilreservasi($filter)->result();
		$this->data['tampilkanjenis']=$this->model_datareservasi->tampilreservasi()->result();
		$this->data['tampilpaket'] = $this->model_datapaket->tampilpaket()->result();
		$this->data['tampilcustomer'] = $this->model_datacustomer->tampilcustomer()->result();
		
		
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/datareservasi',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	
	public function addsave()
	{
		$tgl_dibuat = $this->input->post('tgl_dibuat');
		$data['idpaket'] = $this->input->post('idpaket');
		$data['idcustomer'] = $this->input->post('idcustomer');
		$data['jumlahpeserta'] = $this->input->post('jumlahpeserta');
		$data['tglkeberangkatan'] = $this->input->post('tglkeberangkatan');
		if(empty($tgl_dibuat)){
			$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		}else{
			$data['tgl_dibuat'] = $tgl_dibuat.' '.date('H:i:s');
		}
		$data['dibuat'] = $this->session->userdata('username');

		$this->data = $data;

		$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
		$this->model_datareservasi->addsave($data);
		redirect('admin/cont_datareservasi/datareservasi');
	}

	
	public function getdetail(){
		$this->data['tampilpaket'] = $this->model_datapaket->tampilpaket()->result();
		$this->data['tampilcustomer'] = $this->model_datacustomer->tampilcustomer()->result();
		$id = $_POST['id'];
		$this->data['detail'] = $this->model_datareservasi->detail($id);
		$this->load->view('/admin/getreservasi',$this->data);
	}



	public function updatesave()
	{
		$id = $this->input->post('idreservasi');
		$data['idpaket'] = $this->input->post('idpaket');
		$data['idcustomer'] = $this->input->post('idcustomer');
		$data['jumlahpeserta'] = $this->input->post('jumlahpeserta');
		$data['tglkeberangkatan'] = $this->input->post('tglkeberangkatan');
		$data['tgl_diubah'] = date('Y-m-d H:i:s');
		$data['diubah'] = $this->session->userdata('username');

		$this->model_datareservasi->updatesave($data,$id);
		$this->session->set_flashdata('add_success', 'Data Berhasil Diubah');

		redirect('admin/cont_datareservasi/datareservasi');
	}

	public function delete($idreservasi)
	{
		$this->load->model('model_datareservasi');
		
		$this->model_datareservasi->delete($idreservasi);
		redirect('admin/cont_datareservasi/datareservasi');
	}

}