<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datajobsheet extends CI_Controller {

	function __construct(){
        parent::__construct();

		$this->data['controller_title'] = 'Jobsheet';
		$this->data['controller_index'] = 'admin/Cont_datajobsheet/datajobsheet';
		$this->data['controller_class'] = 'admin/Cont_datajobsheet';
		$this->load->model('model_datajobsheet');
		$this->load->model('model_datareservasi');

	}
	public function index(){
		return $this->datajobsheet();
	}
	public function datajobsheet()
	{
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_datajobsheet->tampiljobsheet()->result();
		$this->data['tampilreservasi']=$this->model_datareservasi->tampilreservasi()->result();
		
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/datajobsheet',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	

	public function getdatareservasi(){
		$id = $_POST['id'];
		$data = $this->model_datareservasi->detail($id);
		$data['kodejobsheet'] = $this->model_datajobsheet->getmaxkodejob();
		$data['totaltagihan'] = ($data['jumlahpeserta']*$data['hargaperpax']);
		echo json_encode($data);
	}
	
	public function formjobsheet($idjobsheet='')
	{
		// ini_set("display_errors", -1);
		$this->data['idjobsheet'] = $idjobsheet;
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_datajobsheet->tampiljobsheet()->result();
		$this->data['detail']=$this->model_datajobsheet->detail($idjobsheet);
		$this->data['tampilreservasi']=$this->model_datareservasi->tampilreservasi()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/detailjobsheet',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	
	public function addsave()
	{
		$idjobsheet = $this->input->post('idjobsheet');
		$data['totaltagihan'] = $this->input->post('totaltagihan');
		$data['idreservasi'] = $this->input->post('idreservasi');
		$data['kodejobsheet'] = $this->input->post('kodejobsheet');
		$filterjb = [];
		$filterjb[] = "a.idreservasi='".$_POST['idreservasi']."'";
		$check = $this->model_datajobsheet->tampiljobsheet($filterjb)->result();
		// var_dump($this->db->last_query());die;
		// var_dump(empty($check));die;
		if(!empty($check)){
			$this->session->set_flashdata('add_failed', 'Jobsheet sudah pernah Dibuat');
			redirect('admin/cont_datajobsheet/datajobsheet');
		}else{
			if(empty($idjobsheet)){
				$data['tgl_dibuat'] = date('Y-m-d H:i:s');
				$data['dibuat'] = $this->session->userdata('username');			
				$save = $this->model_datajobsheet->addsave($data);
				$last_id = $this->db->insert_id();
				if($save){
					$this->session->set_flashdata('add_success', 'Data Berhasil Ditambahkan');
					//update status reservasi
					$this->model_datareservasi->updatesave(['statusreservasi' => 2],$data['idreservasi']);
					redirect('admin/cont_datajobsheet/formjobsheet/'.$last_id);
				}else{
					$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
					redirect('admin/cont_datajobsheet/formjobsheet');
				}
			}else{
				$data['tgl_diubah'] = date('Y-m-d H:i:s');
				$data['diubah'] = $this->session->userdata('username');
				$save = $this->model_datajobsheet->updatesave($data);
				if($save){
					$this->session->set_flashdata('add_success', 'Data Berhasil Diupdate');
					redirect('admin/cont_datajobsheet/datajobsheet');
				}else{
					$this->session->set_flashdata('add_failed', 'Data Gagal Diupdate');
					redirect('admin/cont_datajobsheet/datajobsheet');
				}
			}
		}

	}

	public function getdetail(){
		$this->data['tampilpaket'] = $this->model_datapaket->tampilpaket()->result();
		$this->data['tampilcustomer'] = $this->model_datacustomer->tampilcustomer()->result();
		$id = $_POST['id'];
		$this->data['detail'] = $this->model_datajobsheet->detail($id);
		$this->load->view('/admin/getreservasi',$this->data);
	}

	public function getdetailjob($id=''){
		ini_set("display_errors", -1);
		if(empty($id)){
			$id = $_POST['id'];
		}
		$data = $this->model_datajobsheet->getdetailjobsheet($id);

		$return = "";
		if(!empty($data)){
			foreach ($data as $key => $value) {
				$return .= "<tr>";
				$return .= "<td>";
				$return .= $value->namatagihan;
				$return .= "</td>";
				$return .= "<td>";
				$return .= $value->nominal;
				$return .= "</td>";
				$return .= "<td>";
				$return .= $value->vendor;
				$return .= "</td>";
				$return .= "<td>";
				$return .= '<a href="javascript:void(0)";"><button type="button" class="btn btn-sm btn-danger delDetail" data-id="'.$value->idjobsheetdetail.'" >Hapus</button>';
				$return .= "</td>";
				$return .= "</tr>";
			}
		}

		echo $return;

	}

	public function delete($idreservasi)
	{
		
		$this->model_datajobsheet->delete($idreservasi);
		redirect('admin/cont_datajobsheet/datajobsheet');
	}

	public function savedetail(){
		$idjobsheet = $this->input->post('idjobsheet');
		$data['nominal'] = $this->input->post('nominal');
		$data['vendor'] = $this->input->post('vendor');
		$data['namatagihan'] = $this->input->post('namatagihan');
		$data['idjobsheet'] = $this->input->post('idjobsheet');
		$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		$data['dibuat'] = $this->session->userdata('username');			
		$save = $this->model_datajobsheet->addsavedetail($data);

		$return = [];

		if($save){
			$return['status'] = true;
		}else{
			$return['status'] = false;
		}
		echo json_encode($return);

	}


	public function deletedetail($idreservasi)
	{		
		$del = $this->model_datajobsheet->deletedetail($idreservasi);

		$return = [];

		if($del){
			$return['status'] = true;
		}else{
			$return['status'] = false;
		}
		echo json_encode($return);
	}

}