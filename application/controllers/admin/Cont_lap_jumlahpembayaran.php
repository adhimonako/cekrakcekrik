<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_lap_jumlahpembayaran extends CI_Controller {

	function __construct(){
        parent::__construct();

		$this->data['controller_title'] = 'Laporan Pembayaran';
		$this->data['controller_class'] = 'admin/Cont_lap_jumlahpembayaran';
		$this->data['controller_index'] = 'admin/Cont_lap_jumlahpembayaran/index';
		$this->load->model('model_datareservasi');
		$this->load->model('model_datapaket');
		$this->load->model('model_datacustomer');
		$this->load->model('model_datapembayaran');

	}
	public function index(){
		$this->data['no'] = 0;
		$this->data['tampilkanjenis']=$this->model_datareservasi->tampilreservasi()->result();
		$this->data['tampilpaket'] = $this->model_datapaket->tampilpaket()->result();
		$this->data['tampilcustomer'] = $this->model_datacustomer->tampilcustomer()->result();
		
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/lapjumlahpembayaran',$this->data);
	}

	public function jumlahpembayaran()
	{
		$filter = [];
		$status = $_POST['filter_status'];
		if(isset($status)){
			if($status != 'all'){
				$filter[] = " a.statusreservasi = '$status' ";
			}
			$this->data['filter_status'] = $status;
		}

		if(!empty($_POST['idpaket'])){
			$idpaket = $_POST['idpaket'];
			$filter[] = " a.idpaket = '$idpaket' ";
		}
		if(!empty($_POST['idcustomer'])){
			$idcustomer = $_POST['idcustomer'];
			$filter[] = " d.idcustomer = '$idcustomer' ";
		}

		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_datapembayaran->tampilpembayaran($filter)->result();
		$this->data['tampilkanjenis']=$this->model_datareservasi->tampilreservasi()->result();

		$this->load->view('/admin/rep_jumlahpembayaran',$this->data);
	}
	
}