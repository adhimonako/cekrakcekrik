<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datajabatan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function datajabatan()
	{
		$this->load->model('model_datajabatan');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_datajabatan->tampiljabatan()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/datajabatan',$data);
		$this->load->view('/admin/footer',$data);
	}

	public function addjabatan()
	{
		$this->load->model('model_datajabatan');
		$data = array('namajabatan' => $this->input->post('namajabatan'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'keterangan' => ($this->input->post('keterangan'))
				);

		$this->session->set_flashdata('add_success', 'Jabatan Berhasil Ditambahkan');
		$this->model_datajabatan->addjabatan($data);
		redirect('admin/cont_datajabatan/datajabatan');
	}

	public function getjabatanid(){
	$this->load->view('/admin/getjabatan');
		

	}

	public function ubahjabatan()
	{
		$this->load->model('model_datajabatan');
		$data = array('idjabatan' => $this->input->post('idjabatan'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'namajabatan' => ($this->input->post('namajabatan')),
					'keterangan' => $this->input->post('keterangan')
				);
		$this->model_datajabatan->updatejabatan($data);
		$this->load->view('/admin/updatejabatan');
	}

	public function deletejabatan($idjabatan)
	{
		$this->load->model('model_datajabatan');
		
		$this->model_datajabatan->deletejabatan($idjabatan);
		redirect('admin/cont_datajabatan/datajabatan');
	}
}