<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datauser extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
	parent::__construct();
	$this->load->helper(array('form', 'url'));
	}

	public function datauser()
	{
		$this->load->model('model_datauser');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_datauser->tampiluser()->result();
		//$data['tampilkanwhere']=$this->model_datauser->get_data_where()->result();
		
		$this->data['sesi']= $this->session->userdata();

		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/datauser',$data);
		$this->load->view('/admin/footer',$data);
	}
	public function getuserid(){
	$this->load->view('/admin/getUser');
		

	}
	public function adduser()
	{
		$this->load->model('model_datauser');
		$data = array('username' => $this->input->post('username'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'password' => MD5($this->input->post('password')),
					'level' => $this->input->post('level'),
				);

			

		$cek = $this->model_datauser->adduser($data);
		if($cek){
			$this->model_datauser->adduser($data);

			$this->session->set_flashdata('add_success','User berhasil ditambahkan !');
			redirect('admin/cont_datauser/datauser');
		}
		else{

		$this->session->set_flashdata('add_failed','Nama user sudah digunakan !');
		redirect('admin/cont_datauser/datauser');
		}
	}

	public function upload(){
		$config['upload_path']          = './admin/dist/img/';
		$config['allowed_types']        = 'jpg|png';
		$config['file_name']        	= $this->session->userdata('username');
		$config['overwrite']             = true;
		$config['max_size']             = 1500;
		


		$this->load->library('upload', $config);
			/*
			$resize['image_library'] = 'gd2';
	        $resize['source_image'] = 'E:\foto\Jogja/20160923_090930.jpg';
	        $resize['maintain_ratio'] = false;
	        $resize['create_thumb'] = TRUE;
	        $resize['width']     = 50;
	        $resize['height']   = 50;

	        $this->load->library('image_lib', $resize); 
	        ini_set('gd.jpeg_ignore_warning', true);
	        $this->image_lib->resize();
	        if ( ! $this->image_lib->resize())
			{
			        echo "<script>alert('".$this->image_lib->display_errors()."')</script>";
			}
			*/
	 
		if ( ! $this->upload->do_upload('foto_user')){
			echo "<script>alert('Size Gambar maksimal 1MB')</script>";
			echo "<meta http-equiv='refresh' content='0;url=datauser' />";
		}else{

			$data = array('upload_data' => $this->upload->data());
			
			$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
			$this->model_datauser->uploadfoto($file_name);

		redirect('admin/cont_datauser/datauser');
		}

	}

	public function ubahuser()
	{
		$this->load->model('model_datauser');
		$this->load->view('/admin/updateuser');
	}

	public function resetPW($id_user)
	{
		$this->load->model('model_datauser');
		
		$this->model_datauser->resetPW($id_user);
		redirect('admin/cont_datauser/datauser');
	}

	public function deleteuser($id_user)
	{
		$this->load->model('model_datauser');
		
		$this->model_datauser->deleteuser($id_user);
		redirect('admin/cont_datauser/datauser');
	}
	public function login()
	{
		$this->form_validation->set_rules('username','Username','trim|required');
		$this->form_validation->set_rules('password','Password','trim|required');
		if($this->form_validation->run() == false){

		}
		else{
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$result = $this->model_datauser->login_user($username,$password);

			if ($result) {
			foreach ($result->result_array() as $row)
			{
			        $row_iduser =  $row['id_user'];
			        $row_username = $row['username'];
			        $rowlevel = $row['level'];
			        $rowfoto = $row['foto'];
			        $rowlastlogin = $row['terakhir_login'];
			}	
				
				$user_data = array(
					'id_user'=>$row_iduser,
					'username'=>$row_username,
					'level'=>$rowlevel,					
					'foto'=>$rowfoto,					
					'terakhir_login'=>$rowlastlogin,
					'logged_in'=>true
					);
				$this->session->set_userdata($user_data);
				$this->session->set_flashdata('login_success','Anda telah login');
				redirect('welcome/kosong');
				# code...
			}
			else{
				$this->session->set_flashdata('login_failed','Username dan Password kurang tepat');
				redirect('welcome/index');
			}
		}
	}
	public function logout(){
		//$this->session->set_flashdata('logout','Anda telah keluar');
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		$this->session->unset_userdata('foto');
		$this->session->sess_destroy();
		redirect('welcome/index');

	}
}