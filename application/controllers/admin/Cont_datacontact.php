<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datacontact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function datacontact()
	{
		$this->load->model('model_datacontact');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_datacontact->tampilcontact()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/datacontact',$data);
		$this->load->view('/admin/footer',$data);
	}

	public function addcontact()
	{
		$this->load->model('model_datacontact');
		$data = array('idcontact' => $this->input->post('idcontact'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'deskripsi' => $this->input->post('deskripsi'),
					'telp' => ($this->input->post('telp')),
					'telp2' => ($this->input->post('telp2')),
					'email' => ($this->input->post('email')),
					'email2' => ($this->input->post('email2')),
					'wa' => ($this->input->post('wa')),
					'wa2' => ($this->input->post('wa2')),
					'instagram' => ($this->input->post('instagram')),
					'line' => ($this->input->post('line'))
				);

		$this->session->set_flashdata('add_success', 'Contact Berhasil Ditambahkan');
		$this->model_datacontact->addcontact($data);
		redirect('admin/cont_datacontact/datacontact');
	}

	public function getcontactid(){
				$id = $_POST['id'];
	$this->load->model('model_datacontact');

	$data['detail'] = $this->model_datacontact->detail($id);
	$this->load->view('/admin/getcontact',$data);
		

	}

	public function ubahcontact()
	{
		$this->load->model('model_datacontact');
		$data = array('kodecontact' => $this->input->post('kodecontact'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'deskripsi' => ($this->input->post('deskripsi')),
					'telp' => ($this->input->post('telp')),
					'telp2' => ($this->input->post('telp2')),
					'email' => ($this->input->post('email')),
					'email2' => ($this->input->post('email2')),
					'wa' => ($this->input->post('wa')),
					'wa2' => ($this->input->post('wa2')),
					'instagram' => ($this->input->post('instagram')),
					'line' => ($this->input->post('line'))
				);
		$this->model_datacontact->updatecontact($data);
		redirect('admin/cont_datacontact/datacontact');
	}

	public function deletecontact($idcontact)
	{
		$this->load->model('model_datacontact');
		
		$this->model_datacontact->deletecontact($idcontact);
		redirect('admin/cont_datacontact/datacontact');
	}
}