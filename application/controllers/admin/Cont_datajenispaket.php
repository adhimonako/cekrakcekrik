<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datajenispaket extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function datajenispaket()
	{
		$this->load->model('model_datajenispaket');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_datajenispaket->tampiljenispaket()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/datajenispaket',$data);
		$this->load->view('/admin/footer',$data);
	}

	public function addjenispaket()
	{
		$this->load->model('model_datajenispaket');
		$data = array('namajenispaket' => $this->input->post('namajenispaket'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'keterangan' => ($this->input->post('keterangan'))
				);

		$this->session->set_flashdata('add_success', 'Jenis Paket Berhasil Ditambahkan');
		$this->model_datajenispaket->addjenispaket($data);
		redirect('admin/cont_datajenispaket/datajenispaket');
	}

	public function getjenispaketid(){
				$id = $_POST['id'];
	$this->load->model('model_datajenispaket');

	$data['detail'] = $this->model_datajenispaket->detail($id);
	$this->load->view('/admin/getjenispaket',$data);
		

	}

	public function ubahjenispaket()
	{
		$this->load->model('model_datajenispaket');
		$data = array('idjenispaket' => $this->input->post('idjenispaket'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'namajenispaket' => ($this->input->post('namajenispaket')),
					'keterangan' => $this->input->post('keterangan')
				);
		$this->model_datajenispaket->updatejenispaket($data);
		redirect('admin/cont_datajenispaket/datajenispaket');
	}

	public function deletejenispaket($idjenispaket)
	{
		$this->load->model('model_datajenispaket');
		
		$this->model_datajenispaket->deletejenispaket($idjenispaket);
		redirect('admin/cont_datajenispaket/datajenispaket');
	}
}