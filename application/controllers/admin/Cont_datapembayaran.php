<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datapembayaran extends CI_Controller {

	function __construct(){
        parent::__construct();

		$this->data['controller_title'] = 'Pembayaran';
		$this->data['controller_index'] = 'admin/Cont_datapembayaran/datapembayaran';
		$this->data['controller_class'] = 'admin/Cont_datapembayaran';
		$this->load->model('model_datapembayaran');
		$this->load->model('model_datatagihan');
		$this->load->model('model_datajobsheet');
		$this->load->model('model_datareservasi');

		$this->load->library('upload');

	}

	public function datapembayaran()
	{
		$this->load->model('model_datapembayaran');
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_datapembayaran->tampilpembayaran()->result();
		
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/datapembayaran',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	
	public function addsave()
	{
		
		$data['idtagihan'] = $this->input->post('idtagihan');
		$data['nominal'] = $this->input->post('nominal');
		$data['tglbayar'] = $this->input->post('tglbayar');
		$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		$data['dibuat'] = $this->session->userdata('username');

		$config['upload_path'] = FCPATH.'assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        $error = false;
        if(!empty($_FILES['filefoto']['name'])){
        	if ($this->upload->do_upload('filefoto'))
            {
            	$gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 710;
                $config['height']= 455;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];
	    		$data['filefoto'] = $gambar;
            }else{
            	$error = true;
            }
        }

		$save = $this->model_datapembayaran->addsave($data);
		if($save){
	
	      	$datax['statustagihan'] = 1;
			$save = $this->model_datatagihan->updatesave($datax,$data['idtagihan']);

			$this->session->set_flashdata('add_success', 'Data Berhasil Ditambahkan');
		}else{
			$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
			$this->session->set_flashdata('add_failed', $this->db->last_query());
		}
		redirect('admin/Cont_datapembayaran/datapembayaran');
	}


	public function updatesave()
	{
		$id = $this->input->post('idpembayaran');
		$data['nominal'] = $this->input->post('nominal');
		$data['tgl_diubah'] = date('Y-m-d H:i:s');
		$data['diubah'] = $this->session->userdata('username');


		$config['upload_path'] = FCPATH.'assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        $error = false;
        if(!empty($_FILES['filefoto']['name'])){
        	if ($this->upload->do_upload('filefoto'))
            {
            	$gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 710;
                $config['height']= 455;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];
	    		$data['filefoto'] = $gambar;
            }else{
            	$error = true;
            }
        }
        
		$this->model_datapembayaran->updatesave($data,$id);
		$this->session->set_flashdata('add_success', 'Data Berhasil Diubah');

		redirect('admin/Cont_datapembayaran/datapembayaran');
	}

	public function index(){
		return $this->datapembayaran();
	}
	
	
	public function getdetail(){
		$this->data['tampiltagihan']=$this->model_datatagihan->tampiltagihan()->result();
		$id = $_POST['id'];
		$this->data['detail'] = $this->model_datapembayaran->detail($id);
		$this->load->view('/admin/getpembayaran',$this->data);
	}

	public function delete($idpembayaran)
	{
		$this->load->model('model_datapembayaran');
		
		$this->model_datapembayaran->delete($idpembayaran);
		redirect('admin/cont_datapembayaran/datapembayaran');
	}


	function cetaknota($id=''){
		$this->data['detail'] = $this->model_datapembayaran->detail($id);

		$this->load->view('/admin/rep_nota',$this->data);

	}
}