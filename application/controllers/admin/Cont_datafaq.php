<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datafaq extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function datafaq()
	{
		$this->load->model('model_datafaq');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_datafaq->tampilfaq()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/datafaq',$data);
		$this->load->view('/admin/footer',$data);
	}

	public function addfaq()
	{
		$this->load->model('model_datafaq');
		$data = array('tanya' => $this->input->post('tanya'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'jawab' => ($this->input->post('jawab')),
					'keterangan' => ($this->input->post('keterangan'))
				);

		$this->session->set_flashdata('add_success', 'FAQ Berhasil Ditambahkan');
		$this->model_datafaq->addfaq($data);
		redirect('admin/cont_datafaq/datafaq');
	}

	public function getfaqid(){
	$this->load->view('/admin/getfaq');
		

	}

	public function ubahfaq()
	{
		$this->load->model('model_datafaq');
		$data = array('idfaq' => $this->input->post('idfaq'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'tanya' => ($this->input->post('tanya')),
					'jawab' => ($this->input->post('jawab')),
					'keterangan' => $this->input->post('keterangan')
				);
		$this->model_datafaq->updatefaq($data);
		$this->load->view('/admin/updatefaq');
	}

	public function deletefaq($idfaq)
	{
		$this->load->model('model_datafaq');
		
		$this->model_datafaq->deletefaq($idfaq);
		redirect('admin/cont_datafaq/datafaq');
	}
}