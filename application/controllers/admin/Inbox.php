<?php
class Inbox extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_kontak');
	}

	function index(){
		$this->m_kontak->update_status_kontak();
		$x['data']=$this->m_kontak->get_all_inbox();
		$this->load->view('admin/v_inbox',$x);
	}

	function hapus_inbox(){
		$kode=$this->input->post('kode');
		$this->m_kontak->hapus_kontak($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/inbox');
	}


	function kirim_pesan(){
		$nama=htmlspecialchars($this->input->post('nama',TRUE),ENT_QUOTES);
		$email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
		$pesan=htmlspecialchars(trim($this->input->post('pesan',TRUE)),ENT_QUOTES);
		//inbox_nama,inbox_email,inbox_pesan
		$data['inbox_pesan'] = $pesan;
		$data['inbox_email'] = 'admin';
		$data['inbox_nama'] = 'admin';
		$data['dariinbox'] = 'admin';
		$data['keinbox'] = $email;
		$save = $this->m_kontak->addsave($data);
		if($save){
		echo $this->session->set_flashdata('msg',"<div class='alert alert-info'>Terima kasih telah menghubungi kami.</div>");
		}else{
			echo $this->session->set_flashdata('msg',"<div class='alert alert-danger'>Pesan Gagal Terkirim.</div>");
	
		}
		redirect('admin/inbox');
	}

}