<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_databatal extends CI_Controller {

	function __construct(){
        parent::__construct();

		$this->data['controller_title'] = 'Pembatalan';
		$this->data['controller_index'] = 'admin/Cont_databatal/databatal';
		$this->data['controller_class'] = 'admin/Cont_databatal';
		$this->load->model('model_databatal');
		$this->load->model('model_datareservasi');
		$this->load->model('model_datareservasi');

	}
	public function index(){
		return $this->databatal();
	}
	public function databatal()
	{
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_databatal->tampilbatal()->result();
		$this->data['tampilreservasi']=$this->model_datareservasi->tampilreservasi()->result();
		
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/databatal',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	

	public function getdatareservasi(){
		$id = $_POST['id'];
		$data = $this->model_datareservasi->detail($id);
		$data['kodebatal'] = $this->model_databatal->getmaxkodejob();
		$data['totaltagihan'] = ($data['jumlahpeserta']*$data['hargaperpax']);
		echo json_encode($data);
	}
	
	public function formbatal($idbatal='')
	{
		// ini_set("display_errors", -1);
		$this->data['idbatal'] = $idbatal;
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_databatal->tampilbatal()->result();
		$this->data['detail']=$this->model_databatal->detail($idbatal);
		$this->data['tampilreservasi']=$this->model_datareservasi->tampilreservasi()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/detailbatal',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	
	public function addsave()
	{
		$statusbatal = $this->input->post('statusbatal');
		$data['idreservasi'] = $this->input->post('idreservasi');
		$data['alasanbatal'] = $this->input->post('alasanbatal');
		$data['nominalkembali'] = $this->input->post('nominalkembali');
		$data['statusbatal'] = $this->input->post('statusbatal');
		$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		$data['dibuat'] = $this->session->userdata('username');

		$save = $this->model_databatal->addsave($data);
		if($save){
			if($statusbatal == '0'){
		      	$datax['statusreservasi'] = 60;
		    }
			else if($statusbatal == '2'){
		    	$datax['statusreservasi'] = 62;
		    }else if($statusbatal == '1'){
		    	$datax['statusreservasi'] = 61;
		    }
			$save = $this->model_datareservasi->updatesave($datax,$data['idreservasi']);

			$this->session->set_flashdata('add_success', 'Data Berhasil Ditambahkan');
		}else{
			$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
		}

		redirect('admin/Cont_databatal');

	}


	public function updatesave()
	{
		$id = $this->input->post('idbatal');
		$statusbatal = $this->input->post('statusbatal');
		$data['idreservasi'] = $this->input->post('idreservasi');
		$data['alasanbatal'] = $this->input->post('alasanbatal');
		$data['nominalkembali'] = $this->input->post('nominalkembali');
		$data['statusbatal'] = $this->input->post('statusbatal');
		$data['tgl_diubah'] = date('Y-m-d H:i:s');
		$data['diubah'] = $this->session->userdata('username');
        
		$save = $this->model_databatal->updatesave($data,$id);
		if($save){
			if($statusbatal == '0'){
		      	$datax['statusreservasi'] = 60;
		    }
			else if($statusbatal == '2'){
		    	$datax['statusreservasi'] = 62;
		    }else if($statusbatal == '1'){
		    	$datax['statusreservasi'] = 61;
		    }
			$save = $this->model_datareservasi->updatesave($datax,$data['idreservasi']);

			$this->session->set_flashdata('add_success', 'Data Berhasil Ditambahkan');
		}else{
			$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
		}
		$this->session->set_flashdata('add_success', 'Data Berhasil Diubah');

		redirect('admin/Cont_databatal');
	}

	public function getdetail(){
		$id = $_POST['id'];
		$this->data['detail'] = $this->model_databatal->detail($id);
		$this->load->view('/admin/getbatal',$this->data);
	}

	public function getdetailjob($id=''){
		ini_set("display_errors", -1);
		if(empty($id)){
			$id = $_POST['id'];
		}
		$data = $this->model_databatal->getdetailbatal($id);

		$return = "";
		if(!empty($data)){
			foreach ($data as $key => $value) {
				$return .= "<tr>";
				$return .= "<td>";
				$return .= $value->namatagihan;
				$return .= "</td>";
				$return .= "<td>";
				$return .= $value->nominal;
				$return .= "</td>";
				$return .= "<td>";
				$return .= $value->vendor;
				$return .= "</td>";
				$return .= "<td>";
				$return .= '<a href="javascript:void(0)";"><button type="button" class="btn btn-sm btn-danger delDetail" data-id="'.$value->idbataldetail.'" >Hapus</button>';
				$return .= "</td>";
				$return .= "</tr>";
			}
		}

		echo $return;

	}

	public function delete($idreservasi)
	{
		
		$this->model_databatal->delete($idreservasi);
		redirect('admin/cont_databatal/databatal');
	}

	public function savedetail(){
		$idbatal = $this->input->post('idbatal');
		$data['nominal'] = $this->input->post('nominal');
		$data['vendor'] = $this->input->post('vendor');
		$data['namatagihan'] = $this->input->post('namatagihan');
		$data['idbatal'] = $this->input->post('idbatal');
		$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		$data['dibuat'] = $this->session->userdata('username');			
		$save = $this->model_databatal->addsavedetail($data);

		$return = [];

		if($save){
			$return['status'] = true;
		}else{
			$return['status'] = false;
		}
		echo json_encode($return);

	}


	public function deletedetail($idreservasi)
	{		
		$del = $this->model_databatal->deletedetail($idreservasi);

		$return = [];

		if($del){
			$return['status'] = true;
		}else{
			$return['status'] = false;
		}
		echo json_encode($return);
	}

}