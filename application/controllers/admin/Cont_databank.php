<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_databank extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function databank()
	{
		$this->load->model('model_databank');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_databank->tampilbank()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/databank',$data);
		$this->load->view('/admin/footer',$data);
	}

	public function addbank()
	{
		$this->load->model('model_databank');
		$data = array('namabank' => $this->input->post('namabank'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'keterangan' => ($this->input->post('keterangan'))
				);

		$this->session->set_flashdata('add_success', 'Data Bank Berhasil Ditambahkan');
		$this->model_databank->addbank($data);
		redirect('admin/cont_databank/databank');
	}

	public function getbankid(){
	$this->load->view('/admin/getbank');
		

	}

	public function ubahbank()
	{
		$this->load->model('model_databank');
		$data = array('idbank' => $this->input->post('idbank'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'namabank' => ($this->input->post('namabank')),
					'keterangan' => $this->input->post('keterangan')
				);
		$this->model_databank->updatebank($data);
		$this->load->view('/admin/updatebank');
	}

	public function deletebank($idbank)
	{
		$this->load->model('model_databank');
		
		$this->model_databank->deletebank($idbank);
		redirect('admin/cont_databank/databank');
	}
}