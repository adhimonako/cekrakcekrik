<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_dataprofile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function dataprofile()
	{
		$this->load->model('model_dataprofile');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_dataprofile->tampilprofile()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/dataprofile',$data);
		$this->load->view('/admin/footer',$data);
	}

	public function addprofile()
	{
		$this->load->model('model_dataprofile');
		$data = array('deskripsi' => $this->input->post('deskripsi'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'status' => ($this->input->post('status'))
				);

		$this->session->set_flashdata('add_success', 'Profile Berhasil Ditambahkan');
		$this->model_dataprofile->addprofile($data);
		redirect('admin/cont_dataprofile/dataprofile');
	}

	public function getprofileid(){
			$this->load->model('model_dataprofile');

	$data['detail'] = $this->model_dataprofile->detail($id);
	$this->load->view('/admin/getprofile',$data);
		

	}

	public function ubahprofile()
	{
		$this->load->model('model_dataprofile');
		$data = array('idprofile' => $this->input->post('idprofile'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'deskripsi' => ($this->input->post('deskripsi')),
					'status' => $this->input->post('status')
				);
		$this->model_dataprofile->updateprofile($data);
		$this->load->view('/admin/updateprofile');
	}

	public function deleteprofile($idprofile)
	{
		$this->load->model('model_dataprofile');
		
		$this->model_dataprofile->deleteprofile($idprofile);
		redirect('admin/cont_dataprofile/dataprofile');
	}
}