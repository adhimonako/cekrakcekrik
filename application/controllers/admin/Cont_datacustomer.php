<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datacustomer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function datacustomer()
	{
		$this->load->model('model_datacustomer');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_datacustomer->tampilcustomer()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/datacustomer',$data);
		$this->load->view('/admin/footer',$data);
	}

	public function addcustomer()
	{
		$this->load->model('model_datacustomer');
		$data = array('nama' => $this->input->post('nama'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'jk' => ($this->input->post('jk')),
					'tgllahir' => ($this->input->post('tgllahir')),
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'email' => $this->input->post('email')
					
				);

		$this->session->set_flashdata('add_success', 'Customer Berhasil Ditambahkan');
		$this->model_datacustomer->addcustomer($data);
		redirect('admin/cont_datacustomer/datacustomer');
	}

	


	public function getcustomerid(){
						$id = $_POST['id'];
	$this->load->model('model_datacustomer');

	$data['detail'] = $this->model_datacustomer->detail($id);
	$this->load->view('/admin/getcustomer',$data);
		

	}



	public function ubahcustomer()
	{
		$this->load->model('model_datacustomer');
		$data = array('idcustomer' => $this->input->post('idcustomer'), //Didapatkan dari form yang disubmit pada file update_product_view.php
					'nama' => ($this->input->post('nama')),
					'jk' => ($this->input->post('jk')),
					'tgllahir' => $this->input->post('tgllahir'),
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'email' => $this->input->post('email'),
					'diubah' => $this->input->post('diubah'),
					'tgl_diubah' => $this->input->post('tgl_diubah')
				);
		$this->model_datacustomer->updatecustomer($data);
		$this->load->view('/admin/updatecustomer');
	}

	public function deletecustomer($idcustomer)
	{
		$this->load->model('model_datacustomer');
		
		$this->model_datacustomer->deletecustomer($idcustomer);
		redirect('admin/cont_datacustomer/datacustomer');
	}
}