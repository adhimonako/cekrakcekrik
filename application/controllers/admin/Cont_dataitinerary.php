<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_dataitinerary extends CI_Controller {

	function __construct(){
        parent::__construct();

		$this->data['controller_title'] = 'itinerary';
		$this->data['controller_index'] = 'admin/Cont_dataitinerary/dataitinerary';
		$this->data['controller_class'] = 'admin/Cont_dataitinerary';
		$this->load->model('model_dataitinerary');
		$this->load->model('model_datajobsheet');
		$this->load->model('model_datareservasi');

	}

	public function dataitinerary()
	{
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_dataitinerary->tampilitinerary()->result();
		
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/dataitinerary',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	public function index(){
		return $this->dataitinerary();
	}
	
	public function getdatareservasi(){
		$id = $_POST['id'];
		$data = $this->model_datareservasi->detail($id);
		$data['kodeitinerary'] = $this->model_dataitinerary->getmaxkodejob();
		$data['totaltagihan'] = ($data['jumlahpeserta']*$data['hargaperpax']);
		echo json_encode($data);
	}
	
	public function formitinerary($iditinerary='')
	{
		// ini_set("display_errors", -1);
		$this->data['iditinerary'] = $iditinerary;
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_dataitinerary->tampilitinerary()->result();
		$this->data['detail']=$this->model_dataitinerary->detail($iditinerary);
		$this->data['tampilreservasi']=$this->model_datajobsheet->tampiljobsheet()->result();
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/detailitinerary',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	public function cetakitinerary($iditinerary=''){
		$this->data['iditinerary'] = $iditinerary;
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_dataitinerary->tampilitinerary()->result();
		$this->data['detail']=$this->model_dataitinerary->detail($iditinerary);
		$this->data['tampilreservasi']=$this->model_datajobsheet->tampiljobsheet()->result();
		$this->data['kegiatan'] = $this->model_dataitinerary->getdetailitinerary($iditinerary);

		$this->data['sesi']= $this->session->userdata();

		$this->load->view('/admin/rep_itinerary',$this->data);
	}
	
	public function addsave()
	{
		$data['idjobsheet'] = $this->input->post('idjobsheet');
		if(empty($iditinerary)){
			$data['tgl_dibuat'] = date('Y-m-d H:i:s');
			$data['dibuat'] = $this->session->userdata('username');			
			$save = $this->model_dataitinerary->addsave($data);
			$last_id = $this->db->insert_id();
			if($save){
				$this->session->set_flashdata('add_success', 'Data Berhasil Ditambahkan');
				redirect('admin/cont_dataitinerary/formitinerary/'.$last_id);
			}else{
				$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
				redirect('admin/cont_dataitinerary/formitinerary');
			}
		}else{
			$data['tgl_diubah'] = date('Y-m-d H:i:s');
			$data['diubah'] = $this->session->userdata('username');
			$save = $this->model_dataitinerary->updatesave($data);
			if($save){
				$this->session->set_flashdata('add_success', 'Data Berhasil Diupdate');
				redirect('admin/cont_dataitinerary/dataitinerary');
			}else{
				$this->session->set_flashdata('add_failed', 'Data Gagal Diupdate');
				redirect('admin/cont_dataitinerary/dataitinerary');
			}
		}

	}

	public function getdetail(){
		$this->data['tampilpaket'] = $this->model_datapaket->tampilpaket()->result();
		$this->data['tampilcustomer'] = $this->model_datacustomer->tampilcustomer()->result();
		$id = $_POST['id'];
		$this->data['detail'] = $this->model_dataitinerary->detail($id);
		$this->load->view('/admin/getreservasi',$this->data);
	}

	public function getdetailjob($id=''){
		ini_set("display_errors", -1);
		if(empty($id)){
			$id = $_POST['id'];
		}
		$data = $this->model_dataitinerary->getdetailitinerary($id);

		$return = "";
		if(!empty($data)){
			foreach ($data as $key => $value) {
				$return .= "<tr>";
				$return .= "<td>";
				$return .= $value->waktumulai;
				$return .= "</td>";
				$return .= "<td>";
				$return .= $value->waktuselesai;
				$return .= "</td>";
				$return .= "<td>";
				$return .= $value->deskripsi;
				$return .= "</td>";
				$return .= "<td>";
				$return .= $value->keterangan;
				$return .= "</td>";
				$return .= "<td>";
				$return .= '<a href="javascript:void(0)";"><button type="button" class="btn btn-sm btn-danger delDetail" data-id="'.$value->iditinerarydetail.'" >Hapus</button>';
				$return .= "</td>";
				$return .= "</tr>";
			}
		}

		echo $return;

	}

	public function delete($idreservasi)
	{
		
		$this->model_dataitinerary->delete($idreservasi);
		redirect('admin/cont_dataitinerary/dataitinerary');
	}

	public function savedetail(){
		$iditinerary = $this->input->post('iditinerary');
		$data['idjobsheet'] = $this->input->post('idjobsheet');
		$data['iditinerary'] = $this->input->post('iditinerary');
		$data['waktumulai'] = $this->input->post('waktumulai');
		$data['waktuselesai'] = $this->input->post('waktuselesai');
		$data['deskripsi'] = $this->input->post('deskripsi');
		$data['keterangan'] = $this->input->post('keterangan');
		$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		$data['dibuat'] = $this->session->userdata('username');			
		$save = $this->model_dataitinerary->addsavedetail($data);

		$return = [];

		if($save){
			$return['status'] = true;
		}else{
			$return['status'] = false;
		}
		echo json_encode($return);

		// if(empty($iditinerary)){
		// 	$last_id = $this->db->insert_id();
		// 	if($save){
		// 		$this->session->set_flashdata('add_success', 'Data Berhasil Ditambahkan');
		// 		redirect('admin/cont_dataitinerary/formitinerary/'.$last_id);
		// 	}else{
		// 		$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
		// 		redirect('admin/cont_dataitinerary/formitinerary');
		// 	}
		// }else{
		// 	$data['tgl_diubah'] = date('Y-m-d H:i:s');
		// 	$data['diubah'] = $this->session->userdata('username');
		// 	$save = $this->model_dataitinerary->updatesave($data);
		// 	if($save){
		// 		$this->session->set_flashdata('add_success', 'Data Berhasil Diupdate');
		// 		redirect('admin/cont_dataitinerary/dataitinerary');
		// 	}else{
		// 		$this->session->set_flashdata('add_failed', 'Data Gagal Diupdate');
		// 		redirect('admin/cont_dataitinerary/dataitinerary');
		// 	}
		// }

	}


	public function deletedetail($idreservasi)
	{		
		$del = $this->model_dataitinerary->deletedetail($idreservasi);

		$return = [];

		if($del){
			$return['status'] = true;
		}else{
			$return['status'] = false;
		}
		echo json_encode($return);
	}

}