<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datapaket extends CI_Controller {


	function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->data['controller_class'] = 'admin/cont_datapaket';
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function datapaket()
	{
		$this->load->model('model_datapaket');
		$data['no'] = 0;
		$data['tampilkan']=$this->model_datapaket->tampilpaket()->result();
		$data['tampilkanjenis']=$this->model_datapaket->tampiljenispaket()->result();
		
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$data);
		$this->load->view('/admin/sidebar',$data);
		$this->load->view('/admin/datapaket',$data);
		$this->load->view('/admin/footer',$data);
	}
	
	function index(){
		return $this->datapaket();
	}
	

	public function formpaket($idpaket='')
	{
		// ini_set("display_errors", -1);
		$this->data['idpaket'] = $idpaket;
		$this->data['no'] = 0;
		$this->load->model('model_datapaket');
		// $this->data['detail'] = $this->model_datapaket->detail($id);
		$this->data['tampilkanjenis']=$this->model_datapaket->tampiljenispaket()->result();

		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/formpaket',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	public function detailpaket($idpaket='')
	{
		// ini_set("display_errors", -1);
		$this->data['idpaket'] = $idpaket;
		$this->data['no'] = 0;
		$this->load->model('model_datapaket');
		$this->data['detail'] = $this->model_datapaket->detail($idpaket);
		$this->data['tampilkanjenis']=$this->model_datapaket->tampiljenispaket()->result();
		// var_dump($this->data);
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/detailpaket',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	public function addpaket()
	{
		// var_dump($_POST);die;
		$this->load->model('model_datapaket');

		$data['namapaket'] = $this->input->post('namapaket');
		$data['idjenispaket'] = $this->input->post('idjenispaket');
		$data['detailpaket'] = $this->input->post('detailpaket');
		$data['hargaperpax'] = $this->input->post('hargaperpax');
		$data['minimalpeserta'] = $this->input->post('minimalpeserta');
		$data['dibuat'] = $this->session->userdata('username');

		$config['upload_path'] = FCPATH.'assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        $error = false;
        if(!empty($_FILES['filefoto']['name'])){
        	if ($this->upload->do_upload('filefoto'))
            {
            	$gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 710;
                $config['height']= 455;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];
	    		$data['filefoto'] = $gambar;
            }else{
            	$error = true;
            }
        }

		$save = $this->model_datapaket->addsave($data);
		if($save){
			if($error){
				$this->session->set_flashdata('add_failed', 'Paket Berhasil Ditambahkan, tapi Foto Gagal Diupload');
			}else
				$this->session->set_flashdata('add_success', 'Paket Berhasil Ditambahkan');
		}else{
			$this->session->set_flashdata('add_failed', 'Paket Gagal Ditambahkan');
		}
		redirect('admin/cont_datapaket/datapaket');
	}

	
	public function getpaketid(){
		$id = $_POST['id'];
	$this->load->model('model_datapaket');

	$data['detail'] = $this->model_datapaket->detail($id);

	$this->load->view('/admin/getpaket',$data);
		

	}

	public function ubahpaket()
	{
		$this->load->model('model_datapaket');

		$data['namapaket'] = $this->input->post('namapaket');
		$data['idjenispaket'] = $this->input->post('idjenispaket');
		$data['detailpaket'] = $this->input->post('detailpaket');
		$data['hargaperpax'] = $this->input->post('hargaperpax');
		$data['minimalpeserta'] = $this->input->post('minimalpeserta');
		$data['diubah'] = $this->session->userdata('username');

		$config['upload_path'] = FCPATH.'assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        $error = false;
        if(!empty($_FILES['filefoto']['name'])){
        	if ($this->upload->do_upload('filefoto'))
            {
            	$gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 710;
                $config['height']= 455;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];
	    		$data['filefoto'] = $gambar;
            }else{
            	$error = true;
            }
        }
        // var_dump($_FILES);
        // var_dump($data);die;
		$save = $this->model_datapaket->updatesave($data,$this->input->post('idpaket'));
		// var_dump($save);
		// var_dump($this->db->last_query());die;
		if($save){
			if($error){
				$this->session->set_flashdata('add_failed', 'Paket Berhasil Dirubah, tapi Foto Gagal Diupload');
			}else
				$this->session->set_flashdata('add_success', 'Paket Berhasil Dirubah');
		}else{
			$this->session->set_flashdata('add_failed', 'Paket Gagal Dirubah');
		}
		redirect('admin/cont_datapaket/datapaket');
	}

	public function deletepaket($idpaket)
	{
		$this->load->model('model_datapaket');
		
		$this->model_datapaket->deletepaket($idpaket);
		redirect('admin/cont_datapaket/datapaket');
	}
}