<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cont_datatagihan extends CI_Controller {

	function __construct(){
        parent::__construct();

		$this->data['controller_title'] = 'Tagihan';
		$this->data['controller_index'] = 'admin/Cont_datatagihan/datatagihan';
		$this->data['controller_class'] = 'admin/Cont_datatagihan';
		$this->load->model('model_datatagihan');
		$this->load->model('model_datajobsheet');
		$this->load->model('model_datareservasi');
	}

	public function datatagihan()
	{
		$filter = [];
		$status = $_POST['filter_status'];
		if(isset($status)){
			if($status != 'all'){
				$filter[] = " statustagihan = '$status' ";
			}
			$this->data['filter_status'] = $status;
		}
		$this->load->model('model_datatagihan');
		$this->data['no'] = 0;
		$this->data['tampilkan']=$this->model_datatagihan->tampiltagihan($filter)->result();
		$this->data['tampiljobsheet']=$this->model_datajobsheet->tampiljobsheet()->result();
		
		$this->data['sesi']= $this->session->userdata();
		$this->load->view('/admin/header',$this->data);
		$this->load->view('/admin/sidebar',$this->data);
		$this->load->view('/admin/datatagihan',$this->data);
		$this->load->view('/admin/footer',$this->data);
	}
	
	
	public function addsave()
	{
		
		$data['idjobsheet'] = $this->input->post('idjobsheet');
		$data['jenis_tagihan'] = $this->input->post('jenis_tagihan');
		$data['nominal'] = $this->input->post('nominal');
		$data['tgltagihan'] = $this->input->post('tgltagihan');
		$data['tgldeadline'] = $this->input->post('tgldeadline');
		$data['statustagihan'] = 0;
		$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		$data['dibuat'] = $this->session->userdata('username');
		$save = $this->model_datatagihan->addsave($data);
		if($save){
			$this->session->set_flashdata('add_success', 'Data Berhasil Ditambahkan');
		}else{
			$this->session->set_flashdata('add_failed', 'Data Gagal Ditambahkan');
			$this->session->set_flashdata('add_failed', $this->db->last_query());
		}
		redirect('admin/Cont_datatagihan/datatagihan');
	}


	public function updatesave()
	{
		$id = $this->input->post('idtagihan');
		$data['idjobsheet'] = $this->input->post('idjobsheet');
		$data['jenis_tagihan'] = $this->input->post('jenis_tagihan');
		$data['nominal'] = $this->input->post('nominal');
		$data['tgltagihan'] = $this->input->post('tgltagihan');
		$data['tgldeadline'] = $this->input->post('tgldeadline');
		$data['tgl_diubah'] = date('Y-m-d H:i:s');
		$data['diubah'] = $this->session->userdata('username');

		$this->model_datatagihan->updatesave($data,$id);
		$this->session->set_flashdata('add_success', 'Data Berhasil Diubah');

		redirect('admin/Cont_datatagihan/datatagihan');
	}

	public function index(){
		return $this->datatagihan();
	}
	
	
	public function getdetail(){
		$this->data['tampiljobsheet']=$this->model_datajobsheet->tampiljobsheet()->result();
		$id = $_POST['id'];
		$this->data['detail'] = $this->model_datatagihan->detail($id);
		$this->load->view('/admin/gettagihan',$this->data);
	}

	public function delete($idtagihan)
	{
		$this->load->model('model_datatagihan');
		
		$this->model_datatagihan->delete($idtagihan);
		redirect('admin/cont_datatagihan/datatagihan');
	}

}