<?php 
class History extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('model_datareservasi');
        $this->m_pengunjung->count_visitor();
        if(empty($this->userdata)){
        	redirect('login');
        }
		$this->load->model('model_dataitinerary');
		$this->load->model('model_datajobsheet');
		$this->load->model('model_datareservasi');
		$this->load->model('model_review');
	}

	function index(){
		$this->data['tampilkan']=$this->model_datareservasi->tampilreservasibyuser($this->userdata['idcustomer']);
		// var_dump($this->db->last_query());die;
		$this->load->view('v_history',$this->data);
	}

	function kirim_pesan(){
		$nama=htmlspecialchars($this->input->post('nama',TRUE),ENT_QUOTES);
		$email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
		$pesan=htmlspecialchars(trim($this->input->post('pesan',TRUE)),ENT_QUOTES);
		$this->m_history->kirim_pesan($nama,$email,$pesan);
		echo $this->session->set_flashdata('msg',"<div class='alert alert-info'>Terima kasih telah menghubungi kami.</div>");
		redirect('history');
	}

	function cetakitil($iditinerary=''){
		$this->data['iditinerary'] = $iditinerary;
		$this->data['tampilkan']=$this->model_dataitinerary->tampilitinerary()->result();
		$this->data['detail']=$this->model_dataitinerary->detail($iditinerary);
		$this->data['tampilreservasi']=$this->model_datajobsheet->tampiljobsheet()->result();
		$this->data['kegiatan'] = $this->model_dataitinerary->getdetailitinerary($iditinerary);

		$this->load->view('/admin/rep_itinerary',$this->data);
	}

	function reviewsave(){
		$post = $_POST;
		$data['idreservasi'] = $this->input->post('idreservasi');
		$data['reviewstar'] = $this->input->post('reviewstar');
		$data['reviewtext'] = $this->input->post('reviewtext');

		$save = $this->model_review->addsave($data);
		if($save){			
			echo $this->session->set_flashdata('msg',"<div class='alert alert-success'>Terimakasih telah memberikan penilaian.</div>");
			redirect('history');
		}else{
			echo $this->session->set_flashdata('msg',"<div class='alert alert-danger'>Gagal Menambah Review.</div>");
			// echo $this->session->set_flashdata('msg',$this->db->last_query());
			redirect('history');
		}	
	}
}