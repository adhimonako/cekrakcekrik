<?php 
class Register extends MY_Controller{
	function __construct(){
		parent::__construct();
		if(!empty($this->userdata)){
			redirect('home','refresh');
		}
		$this->load->model('m_pengunjung');
		$this->load->model('m_kontak');
		$this->load->model('model_datacustomer');
		$this->load->model('model_datauser');
        $this->m_pengunjung->count_visitor();
	}

	function index(){
		$this->load->view('v_register',$this->data);
	}

	function daftar(){
		$post = $_POST;
		$data['nama'] = $post['nama'];
		$data['email'] = $post['email']; 
		$data['alamat'] = $post['alamat']; 
		$data['telp'] = $post['telp']; 
		$data['jk'] =  $post['jk'];
		$data['tgl_dibuat'] =  date('Y-m-d H:i:s');
		$data['dibuat'] =  'frontend';
		$save = $this->model_datacustomer->addsave($data);
		if($save){
			$last_id = $this->db->insert_id();
			$user['username'] =  $post['email'];
			$user['password'] =  md5($post['password']);
			$user['status'] =  '1';
			$user['idcustomer'] =  $last_id;
			$user['level'] =  '9';
			$user['tgl_dibuat'] =  date('Y-m-d H:i:s');
			$user['dibuat'] =  'frontend';
			$save = $this->model_datauser->addsave($user);
			echo $this->session->set_flashdata('msg',"<div class='alert alert-success'>Sukses mendaftar.</div>");
			redirect('login');
		}else{
			echo $this->session->set_flashdata('msg',"<div class='alert alert-danger'>Gagal mendaftar.</div>");
			redirect('register');
		}

	}

}