<?php 
class Tagihan extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('model_datatagihan');
		$this->load->model('model_datapembayaran');
		$this->load->model('model_datapaket');

        $this->m_pengunjung->count_visitor();

        if(empty($this->userdata)){
        	redirect('login');
        }
	}

	function index(){
		$order_id = $_GET['order_id'];
		if(!empty($order_id)){
			$idtagihan = explode('T-', $order_id);
			$idtagihan = $idtagihan[0];
		}
		$this->data['tampilkan'] = $this->model_datatagihan->tampiltagihanbyuser($this->userdata['idcustomer'],$idtagihan);

		$this->load->view('v_tagihan',$this->data);
	}

	function save(){
		$data['idpaket'] = $this->input->post('idpaket');
		$data['idcustomer'] = $this->userdata['idcustomer'];
		$data['jumlahpeserta'] = $this->input->post('jumlahpeserta');
		$data['tglkeberangkatan'] = $this->input->post('tglkeberangkatan');
		$data['tgl_dibuat'] = date('Y-m-d H:i:s');
		$data['dibuat'] = 'frontend';

		$save = $this->model_datatagihan->addsave($data);
		if($save){			
			echo $this->session->set_flashdata('msg',"<div class='alert alert-success'>Berhasil melakukan tagihan, silakan lanjutkan pembayaran.</div>");
			redirect('tagihan');
		}else{
			echo $this->session->set_flashdata('msg',"<div class='alert alert-danger'>Gagal melakukan tagihan.</div>");
			redirect('tagihan');
		}

	}

	function cetaknota($id=''){
		$this->data['detail'] = $this->model_datapembayaran->detail($id);

		$this->load->view('/admin/rep_nota',$this->data);

	}	

	function batal(){
		$id = $_POST['id'];
		$datax['ajukanbatal'] = 1;
		$save = $this->model_datapembayaran->updatesave($datax,$id);
		if($save){			
			echo $this->session->set_flashdata('msg',"<div class='alert alert-success'>Pengajuan Batal Berhasil.</div>");
			redirect('tagihan');
		}else{
			echo $this->session->set_flashdata('msg',"<div class='alert alert-success'>Pengajuan Batal Gagal.</div>");
			redirect('tagihan');
		}

	}

}