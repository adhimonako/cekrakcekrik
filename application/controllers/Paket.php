<?php 
class Paket extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('model_datatagihan');
		$this->load->model('model_datapaket');
		$this->load->model('model_review');

        $this->m_pengunjung->count_visitor();
	}

	function index(){
		$this->data['tampilkan'] = $this->model_datapaket->tampilpaket()->result();

		$this->load->view('v_paket',$this->data);
	}

	function detail(){
		$id = $_REQUEST['idpaket'];
		$this->data['idpaket'] = $id;
		$this->data['detail'] = $this->model_datapaket->detail($id);
		$this->data['review'] = $this->model_review->reviewbypaket(["b.idpaket = '$id'"])->result();
		$this->data['reviewstar'] = $this->model_review->reviewstarpaket(["b.idpaket = '$id'"]);
		// var_dump($this->data['reviewstar']);die;

		$this->load->view('v_detailpaket',$this->data);
	}

}