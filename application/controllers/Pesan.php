<?php 
class Pesan extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
        $this->m_pengunjung->count_visitor();
        if(empty($this->userdata)){
        	redirect('login');
        }
		$this->load->model('m_kontak');

	}

	function index(){
		// var_dump($this->userdata);
		$this->data['tampilkan']=$this->m_kontak->get_by_user($this->userdata['username'])->result();
		// var_dump($this->db->last_query());die;
		$this->data['user'] = $this->userdata;
		$this->load->view('v_pesan',$this->data);
	}

	function kirim_pesan(){
		$nama=htmlspecialchars($this->input->post('nama',TRUE),ENT_QUOTES);
		$email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
		$pesan=htmlspecialchars(trim($this->input->post('pesan',TRUE)),ENT_QUOTES);
		//inbox_nama,inbox_email,inbox_pesan
		$data['inbox_kontak'] = $this->userdata['telp'];
		$data['inbox_pesan'] = $pesan;
		$data['inbox_email'] = $email;
		$data['inbox_nama'] = $nama;
		$data['dariinbox'] = $email;
		$data['keinbox'] = 'admin';
		$save = $this->m_kontak->addsave($data);
		if($save){
		echo $this->session->set_flashdata('msg',"<div class='alert alert-info'>Terima kasih telah menghubungi kami.</div>");
		}else{
			echo $this->session->set_flashdata('msg',"<div class='alert alert-danger'>Pesan Gagal Terkirim.".var_dump($this->db->last_query())."</div>");
	
		}
		redirect('pesan');
	}

	function cetakitil($iditinerary=''){
		$this->data['iditinerary'] = $iditinerary;
		$this->data['tampilkan']=$this->model_dataitinerary->tampilitinerary()->result();
		$this->data['detail']=$this->model_dataitinerary->detail($iditinerary);
		$this->data['tampilreservasi']=$this->model_datajobsheet->tampiljobsheet()->result();
		$this->data['kegiatan'] = $this->model_dataitinerary->getdetailitinerary($iditinerary);

		$this->load->view('/admin/rep_itinerary',$this->data);
	}
}