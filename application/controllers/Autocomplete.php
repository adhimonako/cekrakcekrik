<?php 
class Autocomplete extends MY_Controller{
	function __construct(){
		parent::__construct();
	}

	function index(){

		redirect('home');
	}

	function search($type='',$search=''){
		if(empty($type)){
			return "";
		}else{
			if(empty($search)){
				$search = $_GET['term'];
			}
			header("Content-Type: application/json");
			if($type=='customer'){
				$this->load->model('model_datacustomer');
				$result = $this->model_datacustomer->autocomplete($search);
			}
			echo json_encode($result);
		}

	}


	function selectrsv($user='',$selected=''){
		$this->load->model('model_datareservasi');
		$opt = "";
		if(empty($user)){
			$opt .= "<option value=''> -- Pilih Customer Terlebih Dahulu --</option>";
		}else{
			$filter[] = "a.idcustomer = '$user'";
			$filter[] = "a.statusreservasi != '1'";
			$result = $this->model_datareservasi->tampilreservasi($filter)->result();
			if(!empty($result)){
				$opt .= "<option value=''> -- Pilih Reservasi --</option>";
				foreach($result as $key => $val){
					$select ="";
					if(!empty($selected) and ($val->idreservasi == $selected)){
						$select = 'selected=""';
					}
					$opt .= "<option value='".$val->idreservasi."' ".$select."> ".$val->namapaket." </option>";
				}
			}else{
				$opt = "<option value=''> -- Tidak Ada Reservasi --</option>";
			}
		}

		echo $opt;
	}

	function selectrsv1($user='',$selected=''){
		$this->load->model('model_datareservasi');
		$opt = "";
		if(empty($user)){
			$opt .= "<option value=''> -- Pilih Customer Terlebih Dahulu --</option>";
		}else{
			$filter[] = "a.idcustomer = '$user'";
			$filter[] = "a.statusreservasi != '1'";
			$result = $this->model_datareservasi->tampilreservasi1($filter)->result();
			if(!empty($result)){
				$opt .= "<option value=''> -- Pilih Reservasi --</option>";
				foreach($result as $key => $val){
					$select ="";
					if(!empty($selected) and ($val->idreservasi == $selected)){
						$select = 'selected=""';
					}
					$opt .= "<option value='".$val->idreservasi."' ".$select."> ".$val->namapaket." </option>";
				}
			}else{
				$opt = "<option value=''> -- Tidak Ada Reservasi --</option>";
			}
		}

		echo $opt;
	}

	function selectjbst($idreservasi='',$selected=''){
		$this->load->model('model_datajobsheet');
		$opt = "";
		if(empty($idreservasi)){
			$opt .= "<option value=''> -- Pilih Reservasi Terlebih Dahulu --</option>";
		}else{
			$filter[] = "a.idreservasi = '$idreservasi'";
			$result = $this->model_datajobsheet->tampiljobsheet($filter)->result();
			$opt .= "<option value=''> -- Pilih Kode Jobsheet --</option>";
			if(!empty($result)){
				foreach($result as $key => $val){
					$select ="";
					if(!empty($selected) and ($val->idjobsheet == $selected)){
						$select = 'selected=""';
					}
					$opt .= "<option value='".$val->idjobsheet."' ".$select."> ".$val->kodejobsheet." </option>";
				}
			}else{
				$opt = "<option value=''> -- Tidak Ada Jobsheet --</option>";
			}
		}

		echo $opt;
	}

	function selecttagihan($jbsheet='',$selected='',$all=''){
		$this->load->model('model_datatagihan');
		$opt = "";
		if(empty($jbsheet)){
			$opt .= "<option value=''> -- Pilih Jobsheet Terlebih Dahulu --</option>";
		}else{
			$filter[] = "x.idjobsheet = '$jbsheet'";
			if(empty($all)){
				$filter[] = "x.statustagihan = '0'";
			}
			$result = $this->model_datatagihan->tampiltagihan($filter)->result();
			$opt .= "<option value=''> -- Pilih Tagihan --</option>";
			if(!empty($result)){
				foreach($result as $key => $val){
					$select ="";
					if(!empty($selected) and ($val->idtagihan	 == $selected)){
						$select = 'selected=""';
					}
					$opt .= "<option value='".$val->idtagihan."' ".$select."> ".$val->idtagihan." - ".(($val->jenis_tagihan == '1')? 'DP' : 'Pelunasan').' - '.$val->nominal." </option>";
				}
			}else{
				$opt = "<option value=''> -- Tidak Ada Tagihan --</option>";
			}
		}

		echo $opt;
	}

}