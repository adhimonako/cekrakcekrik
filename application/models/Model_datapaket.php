<?php
class Model_datapaket extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	
	function getSesiUser()
	{
		return $this->session->userdata();
	}

	function tampilpaket(){
		$query = $this->db->query("SELECT a.*, b.* FROM ms_paket a inner join ms_jenispaket b on a.idjenispaket=b.idjenispaket " );
		return $query;

	}

	function tampiljenispaket(){
		$query = $this->db->query("SELECT * FROM ms_jenispaket" );
		return $query;

	}
	

	function detail($id=''){

        $this->db->select("a.*");
        $this->db->from('ms_paket as a');
        $this->db->where("a.idpaket = '$id'");
		$query = $this->db->get();
		return $query->row_array();

	}

	function deletepaket($idpaket){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM ms_paket WHERE idpaket='$idpaket'");
		return $query;

	}
	function addpaket($data){
		//$this->db->insert('master_user', $data);
		$dibuat = $this->session->userdata('username');
		$query = $this->db->query("INSERT INTO ms_paket (namapaket,idjenispaket,detailpaket,hargaperpax,minimalpeserta,dibuat,tgl_dibuat) VALUE('$data[namapaket]','$data[idjenispaket]','$data[detailpaket]','$data[hargaperpax]','$data[minimalpeserta]','$dibuat',NOW() )");
		return $query;

	}

	function updatepaket($data){
		$diubah = $this->session->userdata('username');
		$query = $this->db->query(" UPDATE ms_paket set 
			namapaket='$data[namapaket]', 
			idjenispaket='$data[idjenispaket]',
			detailpaket='$data[detailpaket]',
			hargaperpax='$data[hargaperpax]',
			minimalpeserta='$data[hargaperpax]', 
			diubah='$data[diubah]', 
			tgl_diubah=NOW() 
			where idpaket='$data[idpaket]'");
		return $query;
	}

	
	function addsave($data){
		//$this->db->insert('master_user', $data);
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO ms_paket (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}

	function updatesave($data,$id=''){
		$field = [];
		foreach ($data as $key => $val) {
			$field[] = $key."='".$val."'";
		}
		$query = $this->db->query(" UPDATE ms_paket set ".implode(',', $field)." where idpaket='$id' ");
		return $query;
	}

}
?>