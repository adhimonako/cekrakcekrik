<?php
class Model_datareservasi extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	
function getSesiUser()
	{
		return $this->session->userdata();
	}

	function tampilreservasi($filter=[]){
        $this->db->select("a.*,b.namapaket,c.nama as namacustomer,b.hargaperpax,statusreservasi,telp");
        $this->db->from('t_reservasi as a');
        $this->db->join('ms_paket as b ','a.idpaket = b.idpaket');
        $this->db->join('ms_customer as c ','a.idcustomer=c.idcustomer');
        if(!empty($filter)){
        	foreach ($filter as $key => $value) {
        		$this->db->where($value);
        	}
        }
		$query = $this->db->get();
		return $query;

	}

	function tampilreservasi1($filter=[]){
        $this->db->select("a.*,b.namapaket,c.nama as namacustomer,b.hargaperpax,statusreservasi,telp");
        $this->db->from('t_reservasi as a');
        $this->db->join('ms_paket as b ','a.idpaket = b.idpaket');
        $this->db->join('ms_customer as c ','a.idcustomer=c.idcustomer');
        $this->db->join('t_jobsheet as j ','a.idreservasi=j.idreservasi','LEFT');
        $this->db->where('j.idjobsheet is null ');
        if(!empty($filter)){
        	foreach ($filter as $key => $value) {
        		$this->db->where($value);
        	}
        }
		$query = $this->db->get();
		return $query;

	}

	function tampilreservasibyuser($user=''){

        $this->db->select("a.*,b.namapaket,c.nama as namacustomer,b.hargaperpax,statusreservasi,telp,j.idjobsheet,iditinerary,idreview,reviewstar");
        $this->db->from('t_reservasi as a');
        $this->db->join('ms_paket as b ','a.idpaket = b.idpaket');
        $this->db->join('ms_customer as c ','a.idcustomer=c.idcustomer');
        $this->db->join('t_jobsheet as j ','a.idreservasi = j.idreservasi','LEFT');
        $this->db->join('t_itinerary as i ','j.idjobsheet = i.idjobsheet','LEFT');
        $this->db->join('review as r ','a.idreservasi = r.idreservasi','LEFT');
        $this->db->where("a.idcustomer = '$user'");
		$query = $this->db->get();
		return $query->result();

	}

	function tampilpaket(){
		$query = $this->db->query("SELECT * FROM ms_paket" );
		return $query;

	}
	

	function deletereservasi($idreservasi){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_reservasi WHERE idreservasi='$idreservasi'");
		return $query;

	}

	function detail($id=''){

        $this->db->select("a.*,b.namapaket,c.nama as namacustomer,b.hargaperpax,statusreservasi,telp");
        $this->db->from('t_reservasi as a');
        $this->db->join('ms_paket as b ','a.idpaket = b.idpaket');
        $this->db->join('ms_customer as c ','a.idcustomer=c.idcustomer');
        $this->db->where("a.idreservasi = '$id'");
		$query = $this->db->get();
		return $query->row_array();

	}

	function addsave($data){
		//$this->db->insert('master_user', $data);
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO t_reservasi (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}

	function updatesave($data,$id=''){
		$field = [];
		foreach ($data as $key => $val) {
			$field[] = $key."='".$val."'";
		}
		$query = $this->db->query(" UPDATE t_reservasi set ".implode(',', $field)." where idreservasi='$id' ");
		return $query;
	}

	function delete($id=''){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_reservasi WHERE idreservasi='$id'");
		return $query;

	}

}
?>