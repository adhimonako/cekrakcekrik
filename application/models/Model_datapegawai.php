<?php
class Model_datapegawai extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	

	// function adduser($data){
	// 	//$this->db->insert('master_user', $data);
	// 	$query = $this->db->query("INSERT INTO master_user (username,password,level,status,dibuat,tgl_dibuat) VALUE('$data[username]','$data[password]','$data[level]','non aktif','admin',NOW()) ");
	// 	return $query;

	// }

	// function deleteuser($id_user){
	// 	//$this->db->insert('master_user', $data);
	// 	$query = $this->db->query("DELETE FROM master_user WHERE id_user='$id_user'");
	// 	return $query;

	// }

	function tampilpegawai(){
		$query = $this->db->query("SELECT a.*,b.* FROM ms_pegawai a inner join ms_jabatan b on a.idjabatan=b.idjabatan" );
		return $query;

	}

	function tampiljabatan(){
		$query = $this->db->query("SELECT * FROM ms_jabatan" );
		return $query;
	}
	
	function deletepegawai($idpegawai){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM ms_pegawai WHERE idpegawai='$idpegawai'");
		return $query;

	}
	function addpegawai($data){
		//$this->db->insert('master_user', $data);
		$dibuat = $this->session->userdata('username');
		$query = $this->db->query("INSERT INTO ms_pegawai (nip,nik,nama,jk,idjabatan,foto,alamat,telp,email,tgllahir,dibuat,tgl_dibuat) 
			VALUE
			('$data[nip]','$data[nik]','$data[nama]','$data[jk]','$data[idjabatan]','$data[foto]','$data[alamat]','$data[telp]','$data[email]','$data[tgllahir]','$dibuat',NOW())");
		return $query;

	}

	function updatepegawai($data){
		$diubah = $this->session->userdata('username');
		$query = $this->db->query("UPDATE ms_pegawai set 
			nip='$data[nip]',
			nik='$data[nik]',
			nama='$data[nama]',
			jk='$data[jk]',
			idjabatan='$data[idjabatan]',
			foto='$data[foto]',
			alamat='$data[alamat]',
			telp='$data[telp]',
			email='$data[email]',
			tgllahir='$data[tgllahir]',
			diubah='$data[diubah]',
			tgl_diubah=NOW() 
			where idpegawai='$data[idpegawai]'");
		return $query;
	}

}
?>