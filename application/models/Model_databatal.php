<?php
class Model_databatal extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	
function getSesiUser()
	{
		return $this->session->userdata();
	}

	function tampilbatal($filter=[]){

        $this->db->select("a.*,c.namapaket,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan");
        $this->db->from('t_batal as a');
        $this->db->join('t_reservasi as b ','a.idreservasi = b.idreservasi');
        $this->db->join('ms_paket as c ','b.idpaket=c.idpaket');
        $this->db->join('ms_customer as d ','b.idcustomer=d.idcustomer');
        if(!empty($filter)){
        	foreach ($filter as $key => $value) {
        		$this->db->where($value);
        	}
        }
		$query = $this->db->get();
		return $query;

	}
	
	function detail($id=''){
		$query = $this
				->db
				->query("
						SELECT a.*,c.namapaket,b.idcustomer,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan,(coalesce(jumlahpeserta,1) * hargaperpax) as totaltagihan
						FROM t_batal a 
						JOIN t_reservasi b on a.idreservasi = b.idreservasi
						join ms_paket c on b.idpaket=c.idpaket 
						join ms_customer d on b.idcustomer=d.idcustomer 
						where idbatal = '$id'
						" );
		return $query->row_array();

	}

	function addsave($data){
		//$this->db->insert('master_user', $data);
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO t_batal (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}

	function updatesave($data,$id=''){
		$field = [];
		foreach ($data as $key => $val) {
			$field[] = $key."='".$val."'";
		}
		$query = $this->db->query(" UPDATE t_batal set ".implode(',', $field)." where idbatal='$id' ");
		return $query;
	}

	function delete($id=''){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_batal WHERE idbatal='$id'");
		return $query;

	}

	function getmaxkodejob(){
		$query = $this->db->query(" select max(kodebatal) as kode FROM t_batal where date(tgl_dibuat) = '".date('Y-m-d')."' ");
		$result = $query->row();
		if($result->kode == 0){
			return date('Ymd').'001';
		}else{
			$cok = date('Ymd');
			$fuck = explode($cok, $result->kode);
			$max = intval($fuck[1])+1;
			$h =  date('Ymd').str_pad($max,3,"0",STR_PAD_LEFT);
			return $h;
		}
	}

	function getdetailbatal($id=''){
		$query = $this->db->query("SELECT * FROM t_bataldetail WHERE idbatal='$id'");
		return $query->result();
	}

	function addsavedetail($data){
		//$this->db->insert('master_user', $data);
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO t_bataldetail (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}


	function deletedetail($id=''){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_bataldetail WHERE idbataldetail='$id'");
		return $query;

	}


}
?>