<?php
class Model_review extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}


	function addsave($data){
		//$this->db->insert('master_user', $data);
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO review (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}

	function updatesave($data,$id=''){
		$field = [];
		foreach ($data as $key => $val) {
			$field[] = $key."='".$val."'";
		}
		$query = $this->db->query(" UPDATE review set ".implode(',', $field)." where idreview='$id' ");
		return $query;
	}

	function reviewbypaket($filter=[]){

        $this->db->select("a.*,nama");
        $this->db->from('review as a');
        $this->db->join('t_reservasi as b ','a.idreservasi = b.idreservasi');
        $this->db->join('ms_paket as c ','b.idpaket=c.idpaket');
        $this->db->join('ms_customer as d ','b.idcustomer=d.idcustomer');
        if(!empty($filter)){
        	foreach ($filter as $key => $value) {
        		$this->db->where($value);
        	}
        }
		$query = $this->db->get();
		return $query;

	}

	function reviewstarpaket($filter=[]){

        $this->db->select("count(idreview) creviewstar,reviewstar");
        $this->db->from('review as a');
        $this->db->join('t_reservasi as b ','a.idreservasi = b.idreservasi');
        $this->db->join('ms_paket as c ','b.idpaket=c.idpaket');
        $this->db->join('ms_customer as d ','b.idcustomer=d.idcustomer');
        if(!empty($filter)){
        	foreach ($filter as $key => $value) {
        		$this->db->where($value);
        	}
        }
        $this->db->group_by('reviewstar');
		$query = $this->db->get()->result();
		$data = [];
		$total = 0;
		$count = 0;
		foreach ($query as $key => $value) {
			$data[$value->reviewstar] = $value->creviewstar;
			$total += $value->creviewstar*$value->reviewstar;
			$count += $value->creviewstar;
		}
		$avg = $total/$count;
		$data['avg'] = $avg;
		return $data;

	}

	
}
