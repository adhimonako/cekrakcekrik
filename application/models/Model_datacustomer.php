<?php
class Model_datacustomer extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	

	// function adduser($data){
	// 	//$this->db->insert('master_user', $data);
	// 	$query = $this->db->query("INSERT INTO master_user (username,password,level,status,dibuat,tgl_dibuat) VALUE('$data[username]','$data[password]','$data[level]','non aktif','admin',NOW()) ");
	// 	return $query;

	// }

	// function deleteuser($id_user){
	// 	//$this->db->insert('master_user', $data);
	// 	$query = $this->db->query("DELETE FROM master_user WHERE id_user='$id_user'");
	// 	return $query;

	// }

	function tampilcustomer(){
		$query = $this->db->query("SELECT * FROM ms_customer" );
		return $query;

	}

	function detail($id=''){

        $this->db->select("a.*");
        $this->db->from('ms_customer as a');
        $this->db->where("a.idcustomer = '$id'");
		$query = $this->db->get();
		return $query->row_array();

	}
	
	function deletecustomer($idcustomer){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM ms_customer WHERE idcustomer='$idcustomer'");
		return $query;

	}
	function addcustomer($data){
		//$this->db->insert('master_user', $data);
		$dibuat = $this->session->userdata('username');
		$query = $this->db->query("INSERT INTO ms_customer (nama,email,alamat,telp,tgllahir,jk,dibuat,tgl_dibuat) VALUE('$data[nama]','$data[email]','$data[alamat]','$data[telp]','$data[tgllahir]','$data[jk]','$dibuat',NOW())");
		return $query;

	}

	function addsave($data){
		//$this->db->insert('master_user', $data);
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO ms_customer (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}

	function updatesave($data,$id=''){
		$field = [];
		foreach ($data as $key => $val) {
			$field[] = $key."='".$val."'";
		}
		$query = $this->db->query(" UPDATE ms_customer set ".implode(',', $field)." where idcustomer='$id' ");
		return $query;
	}

	function delete($id=''){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM ms_customer WHERE idcustomer='$id'");
		return $query;

	}

	function updatecustomer($data){
		$diubah = $this->session->userdata('username');
		$query = $this->db->query("UPDATE ms_customer set nama='$data[nama]',email='$data[email]',alamat='$data[alamat]',telp='$data[telp]',tgllahir='$data[tgllahir]',jk='$data[jk]',terakhir_login='$data[terakhir_login]',diubah='$data[diubah]', tgl_diubah=NOW() where idcustomer='$data[idcustomer]'");
		return $query;
	}

	function autocomplete($search=''){
        $this->db->select("idcustomer as key, nama as value ");
        $this->db->from('ms_customer');
        $this->db->like('nama',$search);
        $this->db->limit(15);
		$query = $this->db->get()->result();
		$data = [];
		foreach($query as $key => $val){
			$data[] = ['key' => $val->key,'value' => $val->value]; 
		}

		return $data;
	}

}
?>