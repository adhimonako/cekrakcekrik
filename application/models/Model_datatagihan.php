<?php
class Model_datatagihan extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	
function getSesiUser()
	{
		return $this->session->userdata();
	}

	function tampiltagihan($filter=[]){

        $this->db->select("x.*,kodejobsheet,c.namapaket,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan,b.idreservasi,d.idcustomer");
        $this->db->from('t_tagihan x');
        $this->db->join('t_jobsheet as a ','x.idjobsheet = a.idjobsheet');
        $this->db->join('t_reservasi as b ','a.idreservasi = b.idreservasi');
        $this->db->join('ms_paket as c ','b.idpaket=c.idpaket ');
        $this->db->join('ms_customer as d ','b.idcustomer=d.idcustomer  ');
        if(!empty($filter)){
        	foreach ($filter as $key => $value) {
        		$this->db->where($value);
        	}
        }
		$query = $this->db->get();
		return $query;

	}

	function tampiltagihanbyuser($user='',$order_id=''){
		$sql = "						
				SELECT x.*,kodejobsheet,c.namapaket,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan,b.idreservasi,d.idcustomer
				FROM t_tagihan x
				join t_jobsheet a on x.idjobsheet = a.idjobsheet
				JOIN t_reservasi b on a.idreservasi = b.idreservasi
				join ms_paket c on b.idpaket=c.idpaket 
				join ms_customer d on b.idcustomer=d.idcustomer 
				left join t_pembayaran p on x.idtagihan = p.idtagihan and p.isvoid = '0' 
				where b.idcustomer = '$user' ";

		if(!empty($order_id)){
			$sql .= " and x.idtagihan = '$order_id' ";
		}
		$query = $this
				->db
				->query($sql);
		return $query->result();

	}

	function detail($id=''){

        $this->db->select("x.*,kodejobsheet,c.namapaket,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan,b.idreservasi,d.idcustomer");
        $this->db->from('t_tagihan as x');
        $this->db->join('t_jobsheet as a ','x.idjobsheet = a.idjobsheet');
        $this->db->join('t_reservasi as b ','a.idreservasi = b.idreservasi');
        $this->db->join('ms_paket as c ','b.idpaket=c.idpaket ');
        $this->db->join('ms_customer as d ','b.idcustomer=d.idcustomer  ');
        $this->db->where("idtagihan = '$id'");

		$query = $this->db->get();

		return $query->row_array();

	}

	function delete($id=''){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_tagihan WHERE idtagihan='$id'");
		return $query;

	}

	function deletetagihan($idtagihan){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_tagihan WHERE idtagihan='$idtagihan'");
		return $query;

	}
	
	function addsave($data){
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO t_tagihan (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}
	
	function updatesave($data,$id=''){
		$field = [];
		foreach ($data as $key => $val) {
			$field[] = $key."='".$val."'";
		}
		$query = $this->db->query(" UPDATE t_tagihan set ".implode(',', $field)." where idtagihan='$id' ");
		return $query;
	}

	function updatetagihan($data){
		$diubah = $this->session->userdata('username');
		$query = $this->db->query(" UPDATE t_tagihan set 
			namatagihan='$data[namatagihan]', 
			idtagihan='$data[idtagihan]',
			detailtagihan='$data[detailtagihan]',
			hargaperpax='$data[hargaperpax]',
			minimalpeserta='$data[hargaperpax]', 
			diubah='$data[diubah]', 
			tgl_diubah=NOW() 
			where idtagihan='$data[idtagihan]'");
		return $query;
	}

}
?>