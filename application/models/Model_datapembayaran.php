<?php
class Model_datapembayaran extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	
function getSesiUser()
	{
		return $this->session->userdata();
	}

	function tampilpembayaran($filter=[]){
        $this->db->select(" y.*,x.nominal as nominaltagihan, kodejobsheet,c.namapaket,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan,
						x.idjobsheet,a.idreservasi,b.idpaket,b.idcustomer,jenis_tagihan");
        // $this->db->select("*");
        $this->db->from('t_pembayaran as y');
        $this->db->join('t_tagihan as x','y.idtagihan = x.idtagihan');
        $this->db->join('t_jobsheet as a','x.idjobsheet = a.idjobsheet');
        $this->db->join('t_reservasi as b','a.idreservasi = b.idreservasi');
        $this->db->join('ms_paket as c','b.idpaket=c.idpaket');
        $this->db->join('ms_customer as d','b.idcustomer=d.idcustomer');
        if(!empty($filter)){
        	foreach ($filter as $key => $value) {
        		$this->db->where($value);
        	}
        }
		$query = $this->db->get();
		return $query;

	}
	
	function detail($id=''){
		$query = $this
				->db
				->query("
						SELECT y.*,x.nominal as nominaltagihan, kodejobsheet,c.namapaket,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan,x.idjobsheet,a.idreservasi,b.idpaket,b.idcustomer,
							jenis_tagihan
						FROM t_pembayaran y
						join t_tagihan x on y.idtagihan = x.idtagihan
						join t_jobsheet a on x.idjobsheet = a.idjobsheet
						JOIN t_reservasi b on a.idreservasi = b.idreservasi
						join ms_paket c on b.idpaket=c.idpaket 
						join ms_customer d on b.idcustomer=d.idcustomer 
						where idpembayaran = '$id'
						" );
		return $query->row_array();

	}


	function addsave($data){
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO t_pembayaran (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}
	
	function updatesave($data,$id=''){
		$field = [];
		foreach ($data as $key => $val) {
			$field[] = $key."='".$val."'";
		}
		$query = $this->db->query(" UPDATE t_pembayaran set ".implode(',', $field)." where idpembayaran='$id' ");
		return $query;
	}


	function delete($idpembayaran){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_pembayaran WHERE idpembayaran='$idpembayaran'");
		return $query;

	}

	function addpembayaran($data){
		//$this->db->insert('master_user', $data);
		$dibuat = $this->session->userdata('username');
		$query = $this->db->query("INSERT INTO t_pembayaran (namapembayaran,idpembayaran,detailpembayaran,hargaperpax,minimalpeserta,dibuat,tgl_dibuat) VALUE('$data[namapembayaran]','$data[idpembayaran]','$data[detailpembayaran]','$data[hargaperpax]','$data[minimalpeserta]','$dibuat',NOW() )");
		return $query;

	}

	function updatepembayaran($data){
		$diubah = $this->session->userdata('username');
		$query = $this->db->query(" UPDATE t_pembayaran set 
			namapembayaran='$data[namapembayaran]', 
			idpembayaran='$data[idpembayaran]',
			detailpembayaran='$data[detailpembayaran]',
			hargaperpax='$data[hargaperpax]',
			minimalpeserta='$data[hargaperpax]', 
			diubah='$data[diubah]', 
			tgl_diubah=NOW() 
			where idpembayaran='$data[idpembayaran]'");
		return $query;
	}

}
?>