<?php
class Model_dataitinerary extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	
function getSesiUser()
	{
		return $this->session->userdata();
	}

	function tampilitinerary(){
		$query = $this
				->db
				->query("
						SELECT x.*,kodejobsheet,c.namapaket,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan
						FROM t_itinerary x
						join t_jobsheet a on x.idjobsheet = a.idjobsheet
						JOIN t_reservasi b on a.idreservasi = b.idreservasi
						join ms_paket c on b.idpaket=c.idpaket 
						join ms_customer d on b.idcustomer=d.idcustomer 
						" );
		return $query;

	}

	function detail($id=''){
		$query = $this
				->db
				->query("
						SELECT x.*,kodejobsheet,c.namapaket,d.nama as namacustomer,jumlahpeserta,tglkeberangkatan,(coalesce(jumlahpeserta,1) * hargaperpax) as totaltagihan
						FROM t_itinerary x
						join t_jobsheet a on x.idjobsheet = a.idjobsheet
						JOIN t_reservasi b on a.idreservasi = b.idreservasi
						join ms_paket c on b.idpaket=c.idpaket 
						join ms_customer d on b.idcustomer=d.idcustomer 
						where iditinerary = '$id'
						" );
		return $query->row_array();

	}

	function addsave($data){
		//$this->db->insert('master_user', $data);
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO t_itinerary (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}

	function updatesave($data,$id=''){
		$field = [];
		foreach ($data as $key => $val) {
			$field[] = $key."='".$val."'";
		}
		$query = $this->db->query(" UPDATE t_itinerary set ".implode(',', $field)." where iditinerary='$id' ");
		return $query;
	}

	function delete($id=''){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_itinerary WHERE iditinerary='$id'");
		return $query;

	}

	function getmaxkodejob(){
		$query = $this->db->query(" select max(kodejobsheet) as kode FROM t_jobsheet where date(tgl_dibuat) = '".date('Y-m-d')."' ");
		$result = $query->row();
		if($result->kode == 0){
			return date('Ymd').'001';
		}else{
			$max = intval($result->kode)+1;
			return $max;
		}
	}

	function getdetailitinerary($id=''){
		$query = $this->db->query("SELECT * FROM t_itinerarydetail WHERE iditinerary='$id'");
		return $query->result();
	}

	function addsavedetail($data){
		//$this->db->insert('master_user', $data);
		$field = [];
		$value = [];
		foreach ($data as $key => $val) {
			$field[] = $key;
			$value[] = "'".$val."'";
		}

		$query = $this->db->query("INSERT INTO t_itinerarydetail (".implode(',', $field).") VALUE(".implode(',', $value).")");
		return $query;

	}


	function deletedetail($id=''){
		//$this->db->insert('master_user', $data);
		$query = $this->db->query("DELETE FROM t_itinerarydetail WHERE iditinerarydetail='$id'");
		return $query;

	}

}
?>