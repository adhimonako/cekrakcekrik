
<!DOCTYPE html>
<html class="no-js">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Message</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="CekrakCekrik.SITE" />
	<link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/animate.css'?>">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/icomoon.css'?>">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.css'?>">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/flexslider.css'?>">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/style.css'?>">
	<link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datepicker/datepicker3.css">
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>admin/assets/css/font-awesome.min.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url().'theme/js/modernizr-2.6.2.min.js'?>"></script>
	<!-- jQuery -->
	<script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url().'theme/js/jquery.easing.1.3.js'?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url().'theme/js/bootstrap.min.js'?>"></script>
	<script src="<?php echo base_url(); ?>admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

	</head>
	<body>


	<div id="fh5co-page">
	<?php
	$this->load->view('header_front2',$this->data);

	?>


	<div class="fh5co-contact animate-box">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-push-0 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Pesan</h3>
					<button type="button" class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#tambah">
						<i class="fa fa-edit"></i> Tulis Pesan
					</button>
				</div>				
				<div class="col-md-12 col-md-push-0 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">

				<?php 
				// var_dump($user);
				echo $this->session->flashdata('msg');
				?>
					<div class="row">
			            <!-- /.box-header -->
			            <div class="box-body table-responsive">
			              <table id="tb_datapaket" class="table table-bordered table-striped">
			                <thead>
			                <tr>
			                  <th width="20%">Dari</th>
			                  <th width="20%">Kepada</th>
			                  <th width="20%">Tanggal</th>
			                  <th>Pesan</th>
			                  <!-- <th>Action</th> -->
			                </tr>
			                </thead>
			                <tbody>

			                <?php
			                $no = 0;
			                if(!empty($tampilkan)){
			                foreach ($tampilkan as $isipaket) {
			                  ?>
			                <tr>
			                  <td><?php echo $isipaket->dariinbox; ?></td>
			                  <td><?php echo $isipaket->keinbox; ?></td>
			                  <td><?php echo $isipaket->tanggal; ?></td>
			                  <td><?php echo $isipaket->inbox_pesan; ?></td>
			                </tr>
			                <?php
			                }
			            	}else{
			            	?>
			            	<tr>
			            		<td colspan="4" align="center"><strong>Tidak Ada Pesan</strong></td>
			            	</tr>
			            	<?php
			            	}
			                ?>
			                
			                </tbody>
			              </table>
			            </div>
			            <!-- /.box-body -->

					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('v_footer');?>
	</div>


<!-- Modal tambah paket-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tulis Pesan</h4>
      </div>
      <form method="post" action="<?php echo base_url().'pesan/kirim_pesan'?>">
	      <div class="modal-body">
	        <div class="box-body">
	        	<br>
				<div class="col-md-12">
					<div class="form-group">
						<textarea name="pesan" class="form-control" id="" cols="30" rows="7" placeholder="Message" required></textarea>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						
					</div>
				</div>
	        </div>
	      </div>
	      <input class="form-control" name="nama" placeholder="Nama" type="hidden" value="<?=$user['nama']?>" required>
	      <input class="form-control" name="email" placeholder="Email" type="hidden"  value="<?=$user['username']?>" required>
	      <div class="modal-footer">
	      	<input value="Kirim Pesan" class="btn btn-primary" type="submit">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	  </form>
    </div>
  </div>
</div>

	<!-- Waypoints -->
	<script src="<?php echo base_url().'theme/js/jquery.waypoints.min.js'?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url().'theme/js/jquery.flexslider-min.js'?>"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="<?php echo base_url().'theme/js/google_map.js'?>"></script>

	<!-- MAIN JS -->
	<script src="<?php echo base_url().'theme/js/main.js'?>"></script>
	<script type="text/javascript">
	  $(function(){
	    $('.datepicker').datepicker({
	        autoclose: true,
	        format: "yyyy-mm-dd"
	    });
	  });
	</script>
	</body>
</html>
