
<!DOCTYPE html>
<html class="no-js">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Paket</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="CekrakCekrik.SITE" />
	<link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/animate.css'?>">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/icomoon.css'?>">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.css'?>">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/flexslider.css'?>">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/style.css'?>">
	<link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datepicker/datepicker3.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url().'theme/js/modernizr-2.6.2.min.js'?>"></script>
	<!-- jQuery -->
	<script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url().'theme/js/jquery.easing.1.3.js'?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url().'theme/js/bootstrap.min.js'?>"></script>
	<script src="<?php echo base_url(); ?>admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

	</head>
	<body>


	<div id="fh5co-page">
	<?php
	$this->load->view('header_front2',$this->data);

	function limit_words($string, $word_limit){
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit));
    }
	?>


	<div id="fh5co-grid-products" class="animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
					<h2>See our paket</h2>
					<p>Kami mempunyai beberapa paket.</p>
				</div>
			</div>
		</div>
		<?php
			foreach ($tampilkan as $isipaket) :
				$filefoto = $isipaket->filefoto;
				if(empty($filefoto)){
					$filefoto = '1c83ddb2b40bcbfda8499051ef4b8341.jpg';
				}
		?>
		<div class="col-md-4">
			<a href="#" ><img src="<?php echo base_url().'assets/images/'.$filefoto;?>" class="img-responsive"></a>
				<div class="v-align">
					<div class="v-align-middle"><br/>
						<h3 class="title"><?php echo $isipaket->namapaket;?></h3>
						<?php
						if(!empty($isipaket->namajenispaket)){
						?>
						<strong class="category"><?php echo $isipaket->namajenispaket; ?></strong>
						<?php
						}else{
							echo "<h5>&nbsp</h5>";
						}
						?>
<!-- 						<p>
							<?=limit_words($isipaket->detailpaket,3).'...'?>
						</p> -->
						<br>
						<a href="<?php echo base_url(); ?>index.php/paket/detail?idpaket=<?=$isipaket->idpaket ?>" ><button type="button" class="btn btn-sm btn-info">Detail</button>
							<br>
						<a href="<?php echo base_url(); ?>index.php/reservasi?idpaket=<?=$isipaket->idpaket ?>" ><button type="button" class="btn btn-sm btn-success">Reservasi</button>
							<br>
					</div>
				</div>
				<br>
		</div>
		<?php endforeach;?>


	</div>
	<?php
	/*
	<div class="fh5co-contact animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Paket</h3>
				<?php echo $this->session->flashdata('msg');?>
					<div class="row">
			            <!-- /.box-header -->
			            <div class="box-body table-responsive">
			              <table id="tb_datapaket" class="table table-bordered table-striped">
			                <thead>
			                <tr>
			                  <th>No.</th>
			                  <th>Nama Paket</th>
			                  <th>Jenis Paket</th>
			                  <th>Detail Paket</th>
			                  <th>Harga / Pax</th>
			                  <th>Minimal Peserta</th>
			                  <th>Reservasi</th>
			                </tr>
			                </thead>
			                <tbody>

			                <?php
			                foreach ($tampilkan as $isipaket) {
			                  $no++;
			                  ?>
			                <tr>
			                  <td><?php echo $no ; ?></td>
			                  <td><?php echo $isipaket->namapaket; ?></td>
			                  <td><?php echo $isipaket->namajenispaket; ?></td>
			                  <td><?php echo $isipaket->detailpaket; ?></td>
			                  <td><?php echo $isipaket->hargaperpax; ?></td>
			                  <td><?php echo $isipaket->minimalpeserta; ?></td>
			                  <td>
			                    <a href="<?php echo base_url(); ?>index.php/reservasi?idpaket=<?=$isipaket->idpaket ?>" ><button type="button" class="btn btn-sm btn-success">Reservasi</button>
			                  </td>
			                </tr>
			                <?php
			                }
			                ?>
			                </tbody>
			              </table>
			            </div>
			            <!-- /.box-body -->

					</div>
				</div>
			</div>
		</div>
	</div>
	*/
	?>
	<?php $this->load->view('v_footer');?>
	</div>



	<!-- Waypoints -->
	<script src="<?php echo base_url().'theme/js/jquery.waypoints.min.js'?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url().'theme/js/jquery.flexslider-min.js'?>"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="<?php echo base_url().'theme/js/google_map.js'?>"></script>

	<!-- MAIN JS -->
	<script src="<?php echo base_url().'theme/js/main.js'?>"></script>
	<script type="text/javascript">
	  $(function(){
	    $('.datepicker').datepicker({
	        autoclose: true,
	        format: "yyyy-mm-dd"
	    });
	  });
	</script>
	</body>
</html>
