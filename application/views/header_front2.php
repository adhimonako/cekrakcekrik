	<style type="text/css">
		
		#fh5co-header2 {
		  position: absolute;
		  z-index: 1001;
		  width: 100%;
		  margin: 40px 0 40px 0;
		}
		@media screen and (max-width: 768px) {
		  #fh5co-header2 {
		    margin: 0px 0 0 0;
		  }
		}
		#fh5co-header2 .header-inner {
		  height: 80px;
		  padding-left: 20px;
		  padding-right: 20px;
		  float: left;
		  width: 100%;
		  -webkit-border-radius: 7px;
		  -moz-border-radius: 7px;
		  -ms-border-radius: 7px;
		  border-radius: 7px;
		}
		#fh5co-header2 h1 {
		  float: left;
		  padding: 0;
		  font-weight: 700;
		  line-height: 0;
		  font-size: 30px;
		}
		#fh5co-header2 h1 a {
		  color: white;
		}
		#fh5co-header2 h1 a > span {
		  color: #00B906;
		}
		#fh5co-header2 h1 a:hover, #fh5co-header2 h1 a:active, #fh5co-header2 h1 a:focus {
		  text-decoration: none;
		  outline: none;
		}
		#fh5co-header2 h1, #fh5co-header2 nav {
		  margin: 38px 0 0 0;
		}
		#fh5co-header2 nav {
		  float: right;
		  padding: 0;
		}
		@media screen and (max-width: 768px) {
		  #fh5co-header2 nav {
		    display: none;
		  }
		}
		#fh5co-header2 nav ul {
		  padding: 0;
		  margin: 0 -0px 0 0;
		  line-height: 0;
		}
		#fh5co-header2 nav ul li {
		  padding: 0;
		  margin: 0;
		  list-style: none;
		  display: -moz-inline-stack;
		  display: inline-block;
		  zoom: 1;
		  *display: inline;
		}
		#fh5co-header2 nav ul li a {
		  color: rgba(255, 255, 255, 0.7);
		  font-size: 18px;
		  padding: 10px;
		  position: relative;
		  -webkit-transition: 0.2s;
		  -o-transition: 0.2s;
		  transition: 0.2s;
		}
		#fh5co-header2 nav ul li a i {
		  line-height: 0;
		  font-size: 20px;
		  position: relative;
		  top: 3px;
		}
		#fh5co-header2 nav ul li a:after {
		  content: "";
		  position: absolute;
		  height: 2px;
		  bottom: 7px;
		  left: 10px;
		  right: 10px;
		  background-color: #fff;
		  visibility: hidden;
		  -webkit-transform: scaleX(0);
		  -moz-transform: scaleX(0);
		  -ms-transform: scaleX(0);
		  -o-transform: scaleX(0);
		  transform: scaleX(0);
		  -webkit-transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
		  -moz-transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
		  -ms-transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
		  -o-transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
		  transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
		}
		#fh5co-header2 nav ul li a:hover {
		  text-decoration: none;
		  color: white;
		}
		#fh5co-header2 nav ul li a:hover:after {
		  visibility: visible;
		  -webkit-transform: scaleX(1);
		  -moz-transform: scaleX(1);
		  -ms-transform: scaleX(1);
		  -o-transform: scaleX(1);
		  transform: scaleX(1);
		}
		#fh5co-header2 nav ul li a:active, #fh5co-header2 nav ul li a:focus {
		  outline: none;
		  text-decoration: none;
		}
		#fh5co-header2 nav ul li.cta {
		  margin-left: 20px;
		}
		#fh5co-header2 nav ul li.cta a {
		  padding-left: 16px !important;
		  padding-right: 16px !important;
		  padding-top: 7px !important;
		  padding-bottom: 7px !important;
		  border: 2px solid rgba(255, 255, 255, 0.7);
		  -webkit-border-radius: 30px;
		  -moz-border-radius: 30px;
		  -ms-border-radius: 30px;
		  border-radius: 30px;
		}
		#fh5co-header2 nav ul li.cta a:hover {
		  background: #fff;
		  color: #00B906;
		}
		#fh5co-header2 nav ul li.cta a:hover:after {
		  display: none;
		}
		#fh5co-header2 nav ul li.active a {
		  text-decoration: none;
		  color: white;
		}
		#fh5co-header2 nav ul li.active a:after {
		  visibility: visible;
		  -webkit-transform: scaleX(1);
		  -moz-transform: scaleX(1);
		  -ms-transform: scaleX(1);
		  -o-transform: scaleX(1);
		  transform: scaleX(1);
		}
		.dropdown-menu li a{
		  text-decoration: none !important;
		}
		.dropdown-menu li a .active{
		  text-decoration: none  !important;
		}
		.dropdown-menu li a :hover{
		  text-decoration: none !important;
		}

		.usermenu {
			display: block !important;
			padding: 3px 20px !important;
			clear: both !important;
			font-weight: normal !important;
			line-height: 1.42857 !important;
			color: #333333 !important;
			white-space: nowrap !important;
			text-decoration : none !important;
		}

		.usermenu a:hover {
			text-decoration : none !important;
		}
		.usermenu a:focus {
			text-decoration : none !important;
		}
		

	</style>
	<header id="fh5co-header2" style='background-image: url("<?=base_url()?>theme/images/slide_3.jpg"); width: 100%; padding-bottom: 24px; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;'>
		<div class="container">
			<div class="header-inner">
		     	
				<h1><a href="<?php echo base_url().''?>">CekrakCekrik<span>.</span>NET</a></h1>
				<nav role="navigation" class="navbar">
					<ul >
						<li <?=((($this->uri->segment(1) == 'home') or ($this->uri->segment(1) == ''))? 'class="active"' : '')?>><a href="<?php echo base_url().''?>">Home</a></li>
						<li <?=(($this->uri->segment(1) == 'about')? 'class="active"' : '')?>><a href="<?php echo base_url().'about'?>">About</a></li>
						<li <?=(($this->uri->segment(1) == 'paket')? 'class="active"' : '')?>><a href="<?php echo base_url().'paket'?>">Paket</a></li>
						<li <?=(($this->uri->segment(1) == 'portfolio')? 'class="active"' : '')?>><a href="<?php echo base_url().'portfolio'?>">Portfolio</a></li>
						<li <?=(($this->uri->segment(1) == 'artikel')? 'class="active"' : '')?>><a href="<?php echo base_url().'artikel'?>">Blog</a></li>
						<li <?=(($this->uri->segment(1) == 'gallery')? 'class="active"' : '')?>><a href="<?php echo base_url().'gallery'?>">Gallery</a></li>
						<li <?=(($this->uri->segment(1) == 'kontak')? 'class="active"' : '')?>><a href="<?php echo base_url().'kontak'?>">Contact</a></li>
						<?php
						if(!empty($userdata)){
						?>
						<li <?=(($this->uri->segment(1) == 'reservasi')? 'class="active"' : '')?>><a href="<?php echo base_url().'reservasi'?>">Reservasi</a></li>

						<li class="dropdown ">
							<a href="javascript:void(0)" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
								<?php
								echo $userdata['nama'];
								?>
							</a>
							<ul class="dropdown-menu" role="menu">
<!-- 								<li <?=(($this->uri->segment(1) == 'history')? 'class="active"' : '')?> style="text-decoration : none !important;" >
									<a style="text-decoration : none !important;" class="usermenu" role="menuitem" href="<?php echo base_url().'history'?>">Riwayat Transaksi</a>
								</li>
								<li style="text-decoration : none !important;"  <?=(($this->uri->segment(1) == 'tagihan')? 'class="active"' : '')?>>
									<a  style="text-decoration : none !important;"   class="usermenu" role="menuitem" href="<?php echo base_url().'tagihan'?>">Tagihan</a>
								</li>
								<li <?=(($this->uri->segment(1) == 'logout')? 'class="active"' : '')?>>
									<a class="usermenu" role="menuitem" href="<?php echo base_url().'login/logout'?>">Logout</a>
								</li>
 -->

 								<li style="text-decoration : none !important;" >
									<a style="text-decoration : none !important;" class="usermenu" role="menuitem" href="<?php echo base_url().'history'?>">Riwayat Transaksi</a>
								</li>
								<li style="text-decoration : none !important;"  >
									<a  style="text-decoration : none !important;"   class="usermenu" role="menuitem" href="<?php echo base_url().'tagihan'?>">Tagihan</a>
								</li>
								<li style="text-decoration : none !important;"  >
									<a  style="text-decoration : none !important;"   class="usermenu" role="menuitem" href="<?php echo base_url().'pesan'?>">Message</a>
								</li>
								<li >
									<a class="usermenu" role="menuitem" href="<?php echo base_url().'login/logout'?>">Logout</a>
								</li>								
						    </ul>
						</li>
						<?php
						}else{
						?>
						<li class="cta">
							<a href="<?php echo base_url().'register'?>">Register</a>
							<a href="<?php echo base_url().'login'?>">Login</a>
						</li>
						<?php
						}
						?>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<br>
	<br>
	<br>