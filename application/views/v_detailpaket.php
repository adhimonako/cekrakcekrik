<!DOCTYPE html>
<html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Detail Paket</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="CekrakCekrik.SITE" />
	<link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/animate.css'?>">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/icomoon.css'?>">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.css'?>">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/flexslider.css'?>">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/style.css'?>">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/votestar/dist/voteStar.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url().'theme/js/modernizr-2.6.2.min.js'?>"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/votestar/dist/jquery.voteStar.js" type="text/javascript"></script>

	</head>
	<body>


	<div id="fh5co-page">
	<?php
	$this->load->view('header_front',$this->data);
	$filefoto = $detail['filefoto'];
	if(empty($filefoto)){
		$filefoto = '1c83ddb2b40bcbfda8499051ef4b8341.jpg';
	}
	?>

	<aside id="fh5co-hero" clsas="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
		   	<li style="background-image: url(<?php echo base_url().'theme/images/slide_3.jpg'?>);">
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
		   				<div class="slider-text-inner">
		   					<h2><?=$detail['namapaket']?></h2>
		   				</div>
		   			</div>
		   		</div>
		   	</li>
		  	</ul>
	  	</div>
	</aside>

	<div class="fh5co-about animate-box">
		<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
			<h2>Detail Paket</h2>
		</div>
		<div class="container">
			<div class="col-md-6">
				<figure>
					<img src="<?php echo base_url().'assets/images/'.$filefoto;?>" class="img-responsive">
				</figure>
			</div>
			<div class="col-md-6">
				<table class="table no-border">
					<tr>
						<td>Nama Paket</td>
						<td>:</td>
						<td><strong><?=$detail['namapaket']?></strong></td>
					</tr>
					<tr>
						<td>Minimal Peserta</td>
						<td>:</td>
						<td><strong><?=$detail['minimalpeserta']?></strong></td>
					</tr>
					<tr>
						<td>Harga Per pax</td>
						<td>:</td>
						<td><strong><?=$detail['hargaperpax']?></strong></td>
					</tr>
					<tr>
						<td>Deskripsi</td>
						<td>:</td>
						<td><strong><?=$detail['detailpaket']?></strong></td>
					</tr>
					<tr>
						<td colspan="3" align="center">
							<a href="<?php echo base_url(); ?>index.php/reservasi?idpaket=<?=$detail['idpaket']?>" ><button type="button" class="btn btn-sm btn-success">Reservasi Sekarang</button>
						</td>
					</tr>
				</table>
			</div>
			<div class="col-md-12">
				<h3>Review</h3>
				<div class="col-md-4">
					<table class="table">
						<tr>
							<td>Average</td>
							<td>
								<span class="event_star star_big" data-starnum="<?=$reviewstar['avg']?>"><i></i></span>
							</td>
						</tr>
						<tr>
							<td>
								<span class="event_star star_small" data-starnum="5"><i></i></span>
							</td>
							<td>
								<?=intval($reviewstar['5'])?>
							</td>
						</tr>
						<tr>
							<td>
								<span class="event_star star_small" data-starnum="4"><i></i></span>
							</td>
							<td>
								<?=intval($reviewstar['4'])?>
							</td>
						</tr>
						<tr>
							<td>
								<span class="event_star star_small" data-starnum="3"><i></i></span>
							</td>
							<td>
								<?=intval($reviewstar['3'])?>
							</td>
						</tr>
						<tr>
							<td>
								<span class="event_star star_small" data-starnum="2"><i></i></span>
							</td>
							<td>
								<?=intval($reviewstar['2'])?>
							</td>
						</tr>
						<tr>
							<td>
								<span class="event_star star_small" data-starnum="1"><i></i></span>
							</td>
							<td>
								<?=intval($reviewstar['1'])?>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-md-8">
					<h3>Customer Review</h3>
					<table class="" width="100%">
						<?php
						if(!empty($review)) {
							// var_dump($review);
							foreach($review as $r){
						?>
							<tr>
								<td width="200px" align="center">
									<img src="<?=base_url().'admin/dist/img/default.png'?>" width="64px">
									<br>
									<strong>
										<?=$r->nama?>
									</strong>
									<br>
								</td>
								<td></td>
								<td>
									<p><?=$r->reviewtext?></p>
								</td>
							</tr>
							<tr>
								<td align="right">Rating</td>
								<td>&nbsp</td>
								<td>
									<span class="event_star star_small" data-starnum="<?=$r->reviewstar?>"><i></i></span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<hr>
								</td>
							</tr>
						<?php
							}
						}else{
						?>
							<tr>
								Tidak Ada Review.
							</tr>
						<?php
						}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('v_footer');?>
	</div>


	<!-- jQuery -->
	<script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url().'theme/js/jquery.easing.1.3.js'?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url().'theme/js/bootstrap.min.js'?>"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url().'theme/js/jquery.waypoints.min.js'?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url().'theme/js/jquery.flexslider-min.js'?>"></script>

	<!-- MAIN JS -->
	<script src="<?php echo base_url().'theme/js/main.js'?>"></script>

	</body>
</html>
