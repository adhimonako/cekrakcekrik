
<!DOCTYPE html>
<html class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Tagihan</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="CekrakCekrik.SITE" />
		<link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
		<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
		<link rel="shortcut icon" href="favicon.ico">
		<!-- Animate.css -->
		<link rel="stylesheet" href="<?php echo base_url().'theme/css/animate.css'?>">
		<!-- Icomoon Icon Fonts-->
		<link rel="stylesheet" href="<?php echo base_url().'theme/css/icomoon.css'?>">
		<!-- Bootstrap  -->
		<link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.css'?>">
		<!-- Flexslider  -->
		<link rel="stylesheet" href="<?php echo base_url().'theme/css/flexslider.css'?>">
		<!-- Theme style  -->
		<link rel="stylesheet" href="<?php echo base_url().'theme/css/style.css'?>">
		<link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datepicker/datepicker3.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>admin/assets/css/font-awesome.min.css">

		<!-- Modernizr JS -->
		<script src="<?php echo base_url().'theme/js/modernizr-2.6.2.min.js'?>"></script>
		<!-- jQuery -->
		<script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
		<!-- jQuery Easing -->
		<script src="<?php echo base_url().'theme/js/jquery.easing.1.3.js'?>"></script>
		<!-- Bootstrap -->
		<script src="<?php echo base_url().'theme/js/bootstrap.min.js'?>"></script>
		<script src="<?php echo base_url(); ?>admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

	</head>
	<body>


	<div id="fh5co-page">
	<?php
	$this->load->view('header_front2',$this->data);
	// var_dump($tampilkan);
	?>


	<div class="fh5co-contact animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-push-0 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Tagihan</h3>
				<?php echo $this->session->flashdata('msg');?>
					<div class="row">
			            <!-- /.box-header -->
			            <div class="box-body table-responsive">
			              <table id="tb_datapaket" class="table table-bordered table-striped">
			                <thead>
			                <tr>
			                  <th>No.</th>
			                  <th>Nama Paket</th>
			                  <th>Tgl. Berangkat</th>
			                  <th>Jenis Tagihan</th>
			                  <th>Jumlah Peserta</th>
			                  <th>Nominal</th>
			                  <th>Status</th>
			                  <th>Action</th>
			                </tr>
			                </thead>
			                <tbody>

			                <?php
			                $no = 0;
			                foreach ($tampilkan as $isipaket) {
			                $status = "<span class='btn btn-danger btn-sm'>Belum Bayar</span>";
			                $href = "";
			                if(!empty($isipaket->statustagihan)){
			                	$statustagihan = $isipaket->statustagihan;
			                	if($statustagihan == '1'){
					                $status = "<span class='btn btn-success btn-sm'>Lunas</span>";
			                		//idpembayaran
			                		if(!empty($isipaket->idpembayaran)){
				            		$href = " <a target='_blank' href='".base_url()."Tagihan/cetaknota/".$isipaket->idpembayaran."'><i class='fa fa-print'></i> Cetak Kwitansi</a>";
				            		}

			                	}else if($statustagihan == "10"){
			                		$status = "<span class='btn btn-danger btn-sm'>PENDING</span>";
			                	}
				            }
			                  $no++;
			                  ?>
			                <tr>
			                  <td><?php echo $no ; ?></td>
			                  <td><?php echo $isipaket->namapaket; ?></td>
			                  <td><?php echo $isipaket->tglkeberangkatan; ?></td>
			                  <td><?php echo (empty($isipaket->jenistagihan)?'DP' : 'Pelunasan'); ?></td>
			                  <td><?php echo $isipaket->jumlahpeserta; ?></td>
			                  <td><?php echo $isipaket->nominal; ?></td>
			                  <td><?php echo $status; ?></td>
			                  <td>
			                  	<?php
			                  	if(empty($isipaket->statustagihan)) {
			                  	?>
			                    <button type="button" class="btn btn-success btn-sm pay-button" data-toggle="modal" data-gross_amount="<?php echo intval($isipaket->nominal); ?>" data-order_id="<?php echo $isipaket->idtagihan; ?>" id="getpaket" data-target="#view-modal">Bayar</button>
			                  	<?php
			                  	}else if($statustagihan == "10"){
			                  	?>
			                  	<button type="button" class="btn btn-success btn-sm check-tagihan" data-order_id="<?php echo $isipaket->idtagihan; ?>" >Check Tagihan</button>
			                  	<?php
			                  	}
			                  	
			                  	echo $href;
			                  	?>
			                  </td>
			                </tr>
			                <?php
			                }
			                ?>
			                
			                </tbody>
			              </table>
			            </div>
			            <!-- /.box-body -->

					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('v_footer');?>
	</div>



	<!-- Waypoints -->
	<script src="<?php echo base_url().'theme/js/jquery.waypoints.min.js'?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url().'theme/js/jquery.flexslider-min.js'?>"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="<?php echo base_url().'theme/js/google_map.js'?>"></script>

	<!-- MAIN JS -->
	<script src="<?php echo base_url().'theme/js/main.js'?>"></script>
	<script type="text/javascript">
	  $(function(){
	    $('.datepicker').datepicker({
	        autoclose: true,
	        format: "yyyy-mm-dd"
	    });

	    $('.check-tagihan').click(function(){
	    	var idtag = $(this).attr('data-order_id');
	    	window.location = "<?=base_url().'tagihan?order_id='?>"+idtag;
	    });

	  });
	</script>

	<!-- TODO: Remove ".sandbox" from script src URL for production environment. Also input your client key in "data-client-key" -->
	<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-jJSCQHDJL8SBDYOi"></script>
	<script type="text/javascript">
	var  xfirst_name,xlast_name,xemail,xphone,xalamat;
	xfirst_name = "<?=$this->userdata['nama']?>";
	xlast_name = "<?=$this->userdata['nama']?>";
	xemail = "<?=$this->userdata['email']?>";
	xphone = "<?=$this->userdata['telp']?>";
	xalamat = "<?=$this->userdata['alamat']?>";
	$(function(){
	  $('.pay-button').click(function(){
	  	var order_id = $(this).attr('data-order_id');
	  	var gross_amount = $(this).attr('data-gross_amount');
	    var requestBody = 
	    {
	      transaction_details: {
	        gross_amount: gross_amount,
	        order_id: order_id+'T-'+Math.round((new Date()).getTime() / 1000)
	        // order_id: order_id
	      },
		  customer_details: {
		    first_name: xfirst_name,
		    email: xemail,
		    phone: xphone,
		    billing_address: {
		      first_name: xfirst_name,
		      email: xemail,
		      phone: xphone,
		      address: xalamat,
		      city: "Surabaya",
		      postal_code: 60231,
		      country_code: "IDN"
		    },
		    shipping_address: {
		      first_name: xfirst_name,
		      email: xemail,
		      phone: xphone,
		      address: xalamat,
		      city: "Surabaya",
		      postal_code: 60213,
		      country_code: "IDN"
		    }
		  }
	    }
	    // console.log(requestBody);
	    getSnapToken(requestBody, function(response){
	      var response = JSON.parse(response);
	      console.log("new token response", response);
	      // Open SNAP payment popup, please refer to docs for all available options: https://snap-docs.midtrans.com/#snap-js
	      snap.pay(response.token);
	    })
	  });
	});

	  /**
	  * Send AJAX POST request to checkout.php, then call callback with the API response
	  * @param {object} requestBody: request body to be sent to SNAP API
	  * @param {function} callback: callback function to pass the response
	  */
	  function getSnapToken(requestBody, callback) {
	    var xmlHttp = new XMLHttpRequest();
	    xmlHttp.onreadystatechange = function() {
	      if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
	        callback(xmlHttp.responseText);
	      }
	    }
	    xmlHttp.open("post", "<?=base_url()?>checkout.php");
	    xmlHttp.send(JSON.stringify(requestBody));
	  }
	</script>
	</body>
</html>
