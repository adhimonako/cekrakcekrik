
<!DOCTYPE html>
<html class="no-js">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Riwayat Transaksi</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="CekrakCekrik.SITE" />
	<link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/animate.css'?>">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/icomoon.css'?>">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.css'?>">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/flexslider.css'?>">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/style.css'?>">
	<link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datepicker/datepicker3.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/votestar/dist/voteStar.css">
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>admin/assets/css/font-awesome.min.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url().'theme/js/modernizr-2.6.2.min.js'?>"></script>
	<!-- jQuery -->
	<script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url().'theme/js/jquery.easing.1.3.js'?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url().'theme/js/bootstrap.min.js'?>"></script>
	<script src="<?php echo base_url(); ?>admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.emojiRatings.js" type="text/javascript"></script>

	</head>
	<body>


	<div id="fh5co-page">
	<?php
	$this->load->view('header_front2',$this->data);
	?>


	<div class="fh5co-contact animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-push-0 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Transaksi</h3>
				<?php echo $this->session->flashdata('msg');?>
					<div class="row">
			            <!-- /.box-header -->
			            <div class="box-body table-responsive">
			              <table id="tb_datapaket" class="table table-bordered table-striped">
			                <thead>
			                <tr>
			                  <th>No.</th>
			                  <th>Nama Paket</th>
			                  <th>Tgl. Keberangkatan</th>
			                  <th>Jumlah Peserta</th>
			                  <th>Status</th>
			                  <!-- <th>Action</th> -->
			                </tr>
			                </thead>
			                <tbody>

			                <?php
			                $no = 0;
			                foreach ($tampilkan as $isipaket) {
			                $status = "<span class='btn btn-danger btn-sm'>Menunggu Konfirmasi</span>";
			                $href = "";
			                $idreview = "";
			                if(!empty($isipaket->statusreservasi)){
			                	if($isipaket->statusreservasi == 2){
				                	$status = "<a href='".base_url().'tagihan'."' class='btn btn-info btn-sm'>Menunggu Pembayaran</a>";
				            	}else if($isipaket->statusreservasi == 1){
				            		$status = "<span class='btn btn-success btn-sm'>Selesai</span>";
				            	}else if($isipaket->statusreservasi == 60){
				            		$status = "<span class='btn btn-warning btn-sm'>Pengajuan Pembatalan</span>";
				            	}else if($isipaket->statusreservasi == 61){
				            		$status = "<span class='btn btn-danger btn-sm'>Batal</span>";
				            	}else if($isipaket->statusreservasi == 62){
				            		$status = "<span class='btn btn-warning btn-sm'>Proses Pembatalan</span>";
				            	}

				            	if(!empty($isipaket->iditinerary) and ($isipaket->statusreservasi < 20)){
				            		$href = " <a target='_blank' href='".base_url()."History/cetakitil/".$isipaket->iditinerary."'><i class='fa fa-print'></i> Cetak Itinerary</a>";
				            	}
				            }
			                  $no++;
			                  ?>
			                <tr>
			                  <td><?php echo $no ; ?></td>
			                  <td><?php echo $isipaket->namapaket; ?></td>
			                  <td><?php echo $isipaket->tglkeberangkatan; ?></td>
			                  <td><?php echo $isipaket->jumlahpeserta; ?></td>
			                  <td>
			                  	<?=$status?>
			                  	<?=$href?>
			                  	<?php
			                  	if(empty($isipaket->idreview) and !empty($isipaket->iditinerary) and ($isipaket->statusreservasi == 1)){
			                  	?>
			                  	<br>
			                  	<a href="javascript:void(0)" class="btn-review" data-id="<?=$isipaket->idreservasi?>">
			                  	<i class="fa fa-edit"></i>
			                  	Tulis Review
			                  	</a>
			                  	<?php
			                  	}else if(!empty($isipaket->iditinerary) and ($isipaket->statusreservasi == 1)){
			                  	?>
			                  	<br>
			                  	<span class="event_star star_big" data-starnum="<?=$isipaket->reviewstar?>"><i></i></span><br>	
			                  	<?php
			                  	}
			                  	?>
			                  </td>
			                </tr>
			                <?php
			                }
			                ?>
			                
			                </tbody>
			              </table>
			            </div>
			            <!-- /.box-body -->

					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('v_footer');?>
	</div>


	<div id="tambah" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Form Review</h4>
				</div>
				<form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/History/reviewsave">
					<div class="modal-body">
						<div class="box-body">
							<div class="form-group">
								<label for="reviewtext">Review :</label>
								<textarea name="reviewtext" id="reviewtext" required="harus diisi" class="form-control"></textarea>
							</div>
							<div class="form-group" id="rating">
								<label for="reviewstar">Rating :</label>
								<input type="hidden" name="reviewstar" id="reviewstar">
								<input type="hidden" name="idreservasi" id="idreservasi">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-default">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Waypoints -->
	<script src="<?php echo base_url().'theme/js/jquery.waypoints.min.js'?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url().'theme/js/jquery.flexslider-min.js'?>"></script>
	<!-- Google Map -->

	<!-- MAIN JS -->
	<script src="<?php echo base_url().'theme/js/main.js'?>"></script>
	<script type="text/javascript">
	  $(function(){
	    $('.datepicker').datepicker({
	        autoclose: true,
	        format: "yyyy-mm-dd"
	    });

	    $('.btn-review').click(function(){
	    	var id = $(this).attr('data-id');
	    	$('#idreservasi').val(id);
	    	$('#tambah').modal('show');
	    })
	  
		$("#rating").emojiRating({
			fontSize: 32,
			onUpdate: function(count) {
				$(".review-text").show();
				// alert(count);
				$('#reviewstar').val(count);
			}
		});

	  });

	</script>
	<script src="<?php echo base_url(); ?>assets/plugins/votestar/dist/jquery.voteStar.js" type="text/javascript"></script>

	</body>
</html>
