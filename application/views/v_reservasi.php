
<!DOCTYPE html>
<html class="no-js">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Resevasi</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="CekrakCekrik.SITE" />
	<link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/animate.css'?>">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/icomoon.css'?>">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.css'?>">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/flexslider.css'?>">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url().'theme/css/style.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datepicker/datepicker3.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url().'theme/js/modernizr-2.6.2.min.js'?>"></script>
	<!-- jQuery -->
	<script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url().'theme/js/jquery.easing.1.3.js'?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url().'theme/js/bootstrap.min.js'?>"></script>
	<script src="<?php echo base_url(); ?>admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

	</head>
	<body>


	<div id="fh5co-page">
	<?php
	$this->load->view('header_front2',$this->data);
	?>


	<div class="fh5co-contact animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-md-push-3 col-sm-7 col-sm-push-0 col-xs-7 col-xs-push-0">
					<h3>Reservasi</h3>
				<?php echo $this->session->flashdata('msg');?>
					<div class="row">
					<form method="post" action="<?php echo base_url().'Reservasi/save'?>">
						<div class="col-md-12">
				            <div class="form-group">
				              <label for="idpaket">Pilih Paket :</label>
				              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
				              <select name="idpaket" name="idpaket" class="form-control">
				                <?php
				                  foreach ($tampilpaket as $paket) {
				                  $select = "";
				                  	if($idpaket == $paket->idpaket){
				                  		$select = "selected='selected'";
				                  	}
				                    ?>
				                  <option value="<?php echo $paket->idpaket ?>" <?=$select?> > <?php echo $paket->namapaket ?> </option>
				                  <?php
				                  }
				                ?>
				              </select>
				            </div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="jumlahpeserta">Jumlah Peserta :</label>
								<input required="harus diisi" type="text" name="jumlahpeserta" class="form-control" id="jumlahpeserta" placeholder="Masukkan Jumlah Peserta">
							</div>
						</div>
						<div class="col-md-12">
				            <div class="form-group">
								<label for="tglkeberangkatan">Tanggal Keberangkatan :</label>
								<input required="harus diisi" type="text" name="tglkeberangkatan" class="form-control datepicker" id="tglkeberangkatan" placeholder="Masukkan Tanggal Keberangkatan">
				            </div>
			            </div>
						<div class="col-md-12">
							<div class="form-group text-right">
								<button type="reset" value="Reset" class="btn btn-warning btn-lg" type="reset">Reset</button>
								<input value="Submit" class="btn btn-primary btn-lg" type="submit">
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('v_footer');?>
	</div>



	<!-- Waypoints -->
	<script src="<?php echo base_url().'theme/js/jquery.waypoints.min.js'?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url().'theme/js/jquery.flexslider-min.js'?>"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="<?php echo base_url().'theme/js/google_map.js'?>"></script>

	<!-- MAIN JS -->
	<script src="<?php echo base_url().'theme/js/main.js'?>"></script>
	<script type="text/javascript">
	  $(function(){
	    $('.datepicker').datepicker({
	        autoclose: true,
	        format: "yyyy-mm-dd",
	        dateFormat: "yy-mm-dd",
	        minDate: 0
	    });
	  });
	</script>
	</body>
</html>
