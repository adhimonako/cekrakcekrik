
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        W E L C O M E
        <small>Sistem Informasi Manajemen Reservasi CV. Cekrakcekrik</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>index.php/welcome/kosong"><i class="fa fa-dashboard"></i> Halaman Utama</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/cont_datacustomer/datacustomer">Data customer</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">Master Data Tabel Customer Manager</h3>
              
            </div>
            <div class="col-xs-12">
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah">Tambah Customer</button>
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="tb_datacustomer" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Customer</th>
                  <th>Jenis Kelamin</th>
                  <th>Tanggal Lahir</th>
                  <th>Alamat</th>
                  <th>Telepon</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($tampilkan as $isicustomer) {
                  $no++;
                  ?>
                <tr>
                  <td><?php echo $no ; ?></td>
                  <td><?php echo $isicustomer->nama; ?></td>
                  <td><?php echo $isicustomer->jk; ?></td>
                  <td><?php echo $isicustomer->tgllahir; ?></td>
                  <td><?php echo $isicustomer->alamat; ?></td>
                  <td><?php echo $isicustomer->telp; ?></td>
                  <td><?php echo $isicustomer->email; ?></td>
                  <td>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isicustomer->idcustomer; ?>" id="getcustomer" data-target="#view-modal">Ubah</button>
                    <a href="<?php echo base_url(); ?>index.php/admin/cont_datacustomer/deletecustomer/<?=$isicustomer->idcustomer ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button>
                  </td>
                </tr>
                <?php
                }
                ?>
                
                </tbody>
              </table>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- Modal tambah customer-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Tambah Customer</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
          <!-- <form name="uploadfoto" method="post" action="<?= base_url() ?>index.php/admin/cont_datacustomer/upload" enctype="multipart/form-data" >
            <div class="form-group">
              <label>Foto</label><p></p>
              <input class="form-control"  type="file" name="foto_customer" onchange="submit();" />
              <img src="<?php base_url(); ?>admin/dist/img/customer2-160x160.jpg">

            </div>
            </form> -->
            <form name="tambahcustomer" method="post" action="<?= base_url() ?>index.php/admin/cont_datacustomer/addcustomer">
            <div class="form-group">
              <label for="nama">Nama Customer :</label>
              <input required="harus diisi" type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama Customer">
            </div>
            <div class="form-group">
              <label for="jk">Jenis Kelamin :</label>
              <input required="harus diisi" type="text" name="jk" class="form-control" id="jk" placeholder="Masukkan Jenis Kelamin">
            </div>
            <div class="form-group">
              <label for="tgllahir">Tanggal Lahir :</label>
              <input required="harus diisi" type="text" name="tgllahir" class="form-control" id="tgllahir" placeholder="Masukkan Tanggal Lahir">
            </div>
            <div class="form-group">
              <label for="alamat">Alamat :</label>
              <input required="harus diisi" type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat">
            </div>
            <div class="form-group">
              <label for="telp">Telepon :</label>
              <input required="harus diisi" type="text" name="telp" class="form-control" id="telp" placeholder="Masukkan Telepon">
            </div>
            <div class="form-group">
              <label for="telp">Email :</label>
              <input required="harus diisi" type="text" name="email" class="form-control" id="email" placeholder="Masukkan Email">
            </div>

            <button type="submit" class="btn btn-default">Simpan</button>
          </form>
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


    <!-- Modal pop up ubah-->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> Data Profile Customer
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->
  <!--Upload Modal-->
  <div id="uploadfoto" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Foto</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
          <form name="uploadfoto" method="post" action="<?= base_url() ?>index.php/admin/cont_datacustomer/upload" enctype="multipart/form-data" >
            <div class="form-group">
              <label>Foto</label><p></p>
              <input class="form-control"  type="file" name="foto_customer" onchange="submit();" />
            </div>
            </form>
            
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <!--End of Upload modal-->
  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script>
  $(document).ready(function(){
    
    $(document).on('click', '#getcustomer', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row
      
      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/admin/cont_datacustomer/getcustomerid/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });


$('#tb_datacustomer').dataTable( {
    paging: true,
    searching: false
} );
</script>