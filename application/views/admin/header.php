
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CV. Cekrakcekrik | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/assets/css/font-awesome.min.css">
  <!-- Ionicons -->

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/dist/css/skins/_all-skins.min.css"> 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.1.custom.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.ui.combogrid.css">

  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/select2/select2.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/easy-autocomplete.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/easy-autocomplete.themes.min.css">

  <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
  <!-- <link href='<?php echo base_url();?>admin/autocomplete/js/jquery.autocomplete.css' rel='stylesheet' /> -->

  <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
  <!-- <link href='<?php echo base_url();?>admin/autocomplete/css/default.css' rel='stylesheet' /> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script>

function showHint(str) {
     if (str.length == 0) { 
         document.getElementById("txtHint").innerHTML = "";
         return;
     } else {
         var xmlhttp = new XMLHttpRequest();
         xmlhttp.onreadystatechange = function() {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
             }
         }
         xmlhttp.open("GET", "gethint.php?q="+str, true);
         xmlhttp.send();
     }
}
</script>

<!-- jQuery 2.2.3 -->

<script src="<?php echo base_url(); ?>admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>


<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>admin/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.easy-autocomplete.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/nominatim.autocomplete.js"></script>

<!-- coba css 2 kolom -->
  <style type="text/css">
#kiri
{
width:50%;
height:30px;
/*background-color:#FF0;*/
float:left;
}
#kanan
{
width:50%;
height:30px;
/*background-color:#0C0;*/
float:right;
}
/*
  .ui-autocomplete-loading { 
    background:url('../img/loading81.gif') no-repeat right center;
    background-size: 32px 32px;
  }*/
 .ui-autocomplete { z-index:2147483647; }
</style>
<!-- end coba -->

  
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>index.php/welcome/kosong" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>CV. Cekrakcekrik</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php
                if($sesi->foto == ""){
                ?>
              <img src="<?php echo base_url(); ?>admin/dist/img/default.png" class="user-image" alt="User Image">
              <?php
              }else{
              ?>
              <img src="<?php echo base_url(); ?>admin/dist/img/<?php echo $sesi->foto; ?>" class="user-image" alt="User Image">
              <?php
              }
              ?>
              <span class="hidden-xs">
              <?php
              if ($this->session->userdata('logged_in')) {
                  echo $sesi->username;
                  # code...
                }
                else{
                  redirect('welcome/index');
                  //redirect('welcome/index');
                }
              ?>
              </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                
                <?php
                if($sesi->foto == ""){
                ?>
                <img src="<?php echo base_url(); ?>admin/dist/img/default.png" class="img-circle" alt="User Image">
                <?php
                }
                else{
                ?>
                <img src="<?php echo base_url(); ?>admin/dist/img/<?php echo $sesi->foto; ?>" class="img-circle" alt="User Image">
                <?php
                }
                ?>

                <p>
                  <?php echo $this->session->userdata('username');?> - <?php echo $this->session->userdata('level');?>
                  <small>Terakhir Login <?php echo $this->session->userdata('terakhir_login');?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#"></a>
                  </div>
                  
                  <div class="col-xs-4 text-center">
                    <a href="#"></a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a onclick="alert('Anda Telah keluar');" href="<?php echo base_url(); ?>index.php/admin/cont_datauser/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>


  
<!-- FastClick -->
<script src="<?php echo base_url(); ?>admin/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>admin/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>admin/dist/js/demo.js"></script>
<!-- page script -->
</body>
</html>
