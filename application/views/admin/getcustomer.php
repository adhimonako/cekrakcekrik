
		<div class="table-responsive">
		<form method="post" action="<?= base_url() ?>index.php/admin/cont_datacustomer/ubahcustomer">
    <input type="hidden" name="idcustomer" value="<?php echo $detail['idcustomer']; ?>">
    <input type="hidden" name="diubah" value="<?php echo $this->session->userdata('username'); ?>">


          <!-- role admin -->
            <div class="form-group">
              <label for="nama">Nama :</label>
              <input required type="text" name="nama"  value="<?php echo $detail['nama']; ?>" class="form-control" id="nama" placeholder="Masukkan Nama Customer">
            </div>

            <div class="form-group">
              <label for="jk">Jenis Kelamin :</label>
              <input required type="text" name="jk"  value="<?php echo $detail['jk']; ?>" class="form-control" id="jk" placeholder="Masukkan Jenis Kelamin">
            </div>
            
            <div class="form-group">
              <label for="tgllahir">Tanggal Lahir :</label>
              <input required type="text" name="tgllahir"  value="<?php echo $detail['tgllahir']; ?>" class="form-control" id="tgllahir" placeholder="Masukkan Tanggal Lahir">
            </div>

            <div class="form-group">
              <label for="alamat">Alamat :</label>
              <input required type="text" name="alamat"  value="<?php echo $detail['alamat']; ?>" class="form-control" id="alamat" placeholder="Masukkan Alamat">
            </div>

            <div class="form-group">
              <label for="telp">Telepon :</label>
              <input required type="text" name="telp"  value="<?php echo $detail['telp']; ?>"  class="form-control" id="telp" placeholder="Masukkan Telepon">
            </div>

            <div class="form-group">
              <label for="email">Email :</label>
              <input required type="text" name="email"  value="<?php echo $detail['email']; ?>" class="form-control" id="email" placeholder="Masukkan Email">
            </div>

            <!-- end role admin -->


            <input type="submit" value="Simpan" class="btn btn-default" />
          </form>
		</div>
			