
		<div class="table-responsive">
		<form method="post" action="<?= base_url() ?>index.php/admin/cont_datapaket/ubahpaket" enctype="multipart/form-data">
    <input type="hidden" name="idpaket" value="<?php echo $detail['idpaket']; ?>">
    <input type="hidden" name="diubah" value="<?php echo $this->session->userdata('username'); ?>">


          <!-- role admin -->
            <div class="form-group">
              <label for="namapaket">Nama Paket :</label>
              <input required type="text" name="namapaket"  value="<?php echo $detail['namapaket']; ?>" class="form-control" id="namapaket" placeholder="Masukkan Nama Paket">
            </div>

            <div class="form-group">
              <label for="idjenispaket">ID Jenis Paket:</label>
              <input required  name="idjenispaket"  value="<?php echo $detail['idjenispaket']; ?>" type="text" class="form-control" id="idjenispaket" placeholder="Masukkan ID Jenis Paket">
            </div>
            <div class="form-group">
              <label>Gambar</label>
              <input type="file" name="filefoto" style="width: 100%;" >
            </div>
            <div class="form-group">
              <label for="detailpaket">Detail Paket:</label>
              <textarea name="detailpaket" id="detailpaket1" required="harus diisi" class="form-control"><?php echo $detail['detailpaket']; ?></textarea>

              <!-- <input required name="detailpaket"  value="<?php echo $detail['detailpaket']; ?>" type="text" class="form-control" id="detailpaket" placeholder="Masukkan Detail Paket"> -->
            </div>

            <div class="form-group">
              <label for="hargaperpax">Harga / Pax:</label>
              <input required name="hargaperpax"  value="<?php echo $detail['hargaperpax']; ?>" type="text" class="form-control" id="hargaperpax" placeholder="Masukkan Harga / Pax">
            </div>

            <div class="form-group">
              <label for="minimalpeserta">Minimal Peserta:</label>
              <input required  name="minimalpeserta"  value="<?php echo $detail['minimalpeserta']; ?>" type="text" class="form-control" id="minimalpeserta" placeholder="Masukkan Minimal Peserta">
            </div>

            <!-- end role admin -->


            <input type="submit" value="Simpan" class="btn btn-default" />
          </form>
		</div>
		
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<!-- Page script -->

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
  
    CKEDITOR.replace('detailpaket1');
   
  
  });
</script>