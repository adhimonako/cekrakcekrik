
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        W E L C O M E
        <small>Sistem Informasi Manajemen Reservasi CV. Cekrakcekrik</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>index.php/welcome/kosong"><i class="fa fa-dashboard"></i> Halaman Utama</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/cont_datauser/datauser">Data User</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">Master Data Tabel User Manager</h3>
              
            </div>
            <div class="col-xs-12">
            <?php
              if(($this->session->userdata('level') == 'Admin') || ($this->session->userdata('level') == 'Supervisor')){
            ?>
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah">Tambah User</button>
            <?php
            }
            ?>
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="tb_datauser" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama User</th>
                  <th>Level(s)</th>
                  <th>Foto</th>
                  <th>Dibuat Oleh</th>
                  <th>Tanggal Dibuat</th>
                  <th >Last Login</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($tampilkan as $isiuser) {
                  $no++;
                  ?>
                <tr>
                  <td><?php echo $no ; ?></td>
                  <td><?php echo $isiuser->username; ?></td>
                  <td><?php echo $isiuser->level; ?></td>
                  <td>
                      <?php 
                        if ($isiuser->foto == '') {
                      ?>

                        <img src="<?php echo base_url(); ?>admin/dist/img/avatar.png"  width="50" height="50" class="user-image" alt="User Image">
                      <?php
                        }
                        else{
                      ?>
                        <a href="<?php echo base_url(); ?>admin/dist/img/<?php echo $isiuser->foto; ?>"><img src="<?php echo base_url(); ?>admin/dist/img/<?php echo $isiuser->foto; ?>"  width="50" height="50" class="" alt="User Image"></a>
                        
                        <?php
                        }
                      ?>

                      <?php 
                          if($this->session->userdata('username') == $isiuser->username){
                        ?>
                          <a href="#" data-toggle="modal" data-target="#uploadfoto" >Upload</a>
                        <?php
                        }
                        else{
                          echo "";
                        }
                        ?>
                    
                  </td>
                  <td><?php echo $isiuser->dibuat; ?></td>
                  <td><?php echo $isiuser->tgl_dibuat; ?></td>
                  <td><?php echo $isiuser->terakhir_login; ?></td>
                  <td>
                    


                    
                   <!-- role admin -->
                    <?php
                    if( ($this->session->userdata('level') == 'Admin') && ( $isiuser->level== 'Admin') ){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isiuser->id_user; ?>" id="getUser" data-target="#view-modal">Ubah</button>

                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>
                   <?php
                    if( ($this->session->userdata('level') == 'Admin') && ( $isiuser->level <> 'Admin') ){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isiuser->id_user; ?>" id="getUser" data-target="#view-modal">Ubah</button>

                    <a href="<?php echo base_url(); ?>index.php/admin/cont_datauser/resetPW/<?=$isiuser->id_user ?>" onclick="return confirm('Yakin Direset ?');"><button type="button" class="btn btn-sm btn-primary">Reset PW</button></a>

                    <a href="<?php echo base_url(); ?>index.php/admin/cont_datauser/deleteuser/<?=$isiuser->id_user ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a>
                   <?php
                    }
                   ?>
                    <!-- end role admin -->

                   <!-- role Supervisor -->
                   <?php
                    if( ($this->session->userdata('level') == 'Supervisor') && ( $isiuser->level == 'Supervisor') ){
                    ?>
                    <?php
                    if( $this->session->userdata('username') == $isiuser->username){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isiuser->id_user; ?>" id="getUser" data-target="#view-modal">Ubah</button>
                    <?php
                    }
                    else{
                    ?>
                    <a href="#"><button type="button" class="btn btn-sm btn-warning disabled">Ubah</button></a>
                    <?php
                    }
                    ?>

                    
                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>

                   <?php
                    if( ($this->session->userdata('level') == 'Supervisor') && ( $isiuser->level <> 'Supervisor') && ( $isiuser->level <> 'Admin') ){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isiuser->id_user; ?>" id="getUser" data-target="#view-modal">Ubah</button>

                    <a href="<?php echo base_url(); ?>index.php/admin/cont_datauser/resetPW/<?=$isiuser->id_user ?>" onclick="return confirm('Yakin Direset ?');"><button type="button" class="btn btn-sm btn-primary">Reset PW</button></a>

                    <a href="<?php echo base_url(); ?>index.php/admin/cont_datauser/deleteuser/<?=$isiuser->id_user ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a>
                   <?php
                    }
                   ?>

                   <?php
                    if( ($this->session->userdata('level') == 'Supervisor') && ( $isiuser->level == 'Admin') ){
                    ?>
                    <a href="#"><button type="button" class="btn btn-sm btn-warning disabled">Ubah</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>
                    <!-- end role Supervisor -->

                    <!-- role Operator -->
                    <?php
                    if( ($this->session->userdata('level') == 'Operator') && ( $isiuser->level== 'Operator') ){
                    ?>
                    <?php
                    if( $this->session->userdata('username') == $isiuser->username){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isiuser->id_user; ?>" id="getUser" data-target="#view-modal">Ubah</button>
                    <?php
                    }
                    else{
                    ?>
                    <a href="#"><button type="button" class="btn btn-sm btn-warning disabled">Ubah</button></a>
                    <?php
                    }
                    ?>

                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>
                   <?php
                    if( ($this->session->userdata('level') == 'Operator') && ( $isiuser->level <> 'Operator') ){
                    ?>
                    
                    <a href="#"><button type="button" class="btn btn-sm btn-warning disabled">Ubah</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>
                    <!-- end role Operator -->

                    <!-- role Logistik -->
                    <?php
                    if( ($this->session->userdata('level') == 'Logistik') && ( $isiuser->level== 'Logistik') ){
                    ?>
                    <?php
                    if( $this->session->userdata('username') == $isiuser->username){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isiuser->id_user; ?>" id="getUser" data-target="#view-modal">Ubah</button>
                    <?php
                    }
                    else{
                    ?>
                    <a href="#"><button type="button" class="btn btn-sm btn-warning disabled">Ubah</button></a>
                    <?php
                    }
                    ?>

                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>
                   <?php
                    if( ($this->session->userdata('level') == 'Logistik') && ( $isiuser->level <> 'Logistik') ){
                    ?>
                    
                    <a href="#"><button type="button" class="btn btn-sm btn-warning disabled">Ubah</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>
                    <!-- end role Operator -->

                    <!-- role Admin Gudang -->
                    <?php
                    if( ($this->session->userdata('level') == 'Admin Gudang') && ( $isiuser->level== 'Admin Gudang') ){
                    ?>
                    <?php
                    if( $this->session->userdata('username') == $isiuser->username){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isiuser->id_user; ?>" id="getUser" data-target="#view-modal">Ubah</button>
                    <?php
                    }
                    else{
                    ?>
                    <a href="#"><button type="button" class="btn btn-sm btn-warning disabled">Ubah</button></a>
                    <?php
                    }
                    ?>

                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>
                   <?php
                    if( ($this->session->userdata('level') == 'Admin Gudang') && ( $isiuser->level <> 'Admin Gudang') ){
                    ?>
                    
                    <a href="#"><button type="button" class="btn btn-sm btn-warning disabled">Ubah</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-primary disabled">Reset PW</button></a>

                    <a href="#"><button type="button" class="btn btn-sm btn-danger disabled">Hapus</button></a>
                   <?php
                    }
                   ?>
                    <!-- end role Admin Gudang -->
                  </td>
                </tr>
                <?php
                }
                ?>
                
                </tbody>
              </table>

              

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- Modal tambah user-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Tambah User</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
          <!-- <form name="uploadfoto" method="post" action="<?= base_url() ?>index.php/admin/cont_datauser/upload" enctype="multipart/form-data" >
            <div class="form-group">
              <label>Foto</label><p></p>
              <input class="form-control"  type="file" name="foto_user" onchange="submit();" />
              <img src="<?php base_url(); ?>admin/dist/img/user2-160x160.jpg">

            </div>
            </form> -->
            <form name="tambahuser" method="post" action="<?= base_url() ?>index.php/admin/cont_datauser/adduser">
            <div class="form-group">
              <label for="Nama User">Nama User:</label>
              <input required="harus diisi" type="text" name="username" class="form-control" id="username" placeholder="Masukkan Nama User">
            </div>
            <div class="form-group">
              <label for="password">Password:</label>
              <input required type="text" name="password" class="form-control" id="password" value="123" readonly>
            </div>
            <div class="form-group">
              <label for="Level User">Level User:</label>
              <select class="form-control" name="level">
                <option value="Supervisor">Supervisor</option>
                <option value="Admin Gudang">Admin Gudang</option>
                <option value="Operator" selected>Operator</option>
                <option value="Logistik">Logistik</option>
              </select>
            </div>

            <button type="submit" class="btn btn-default">Simpan</button>
          </form>
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


    <!-- Modal pop up ubah-->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> User Profile
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->
  <!--Upload Modal-->
  <div id="uploadfoto" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Foto</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
          <form name="uploadfoto" method="post" action="<?= base_url() ?>index.php/admin/cont_datauser/upload" enctype="multipart/form-data" >
            <div class="form-group">
              <label>Foto</label><p></p>
              <input class="form-control"  type="file" name="foto_user" onchange="submit();" />
            </div>
            </form>
            
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <!--End of Upload modal-->
  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script>
  $(document).ready(function(){
    
    $(document).on('click', '#getUser', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row
      
      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/admin/cont_datauser/getuserid/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });


$('#tb_datauser').dataTable( {
    paging: true,
    searching: false
} );
</script>