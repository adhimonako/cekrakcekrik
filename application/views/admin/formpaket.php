
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    // var_dump($this->data['controller_title']);
    $this->load->view('layout/section_header',$this->data);
    ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                <?=$controller_title?>
              </h3>
              
            </div>
            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>
<form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/admin/cont_datapaket/addpaket" enctype="multipart/form-data">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-4">

                  <div class="form-group">
                    <label for="namapaket">Nama Paket :</label>
                    <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket">
                  </div>
                  <div class="form-group">
                    <label for="idjenispaket">Jenis Paket :</label>
                    <select name="idjenispaket" id="idjenispaket">
                      <?php
                        foreach ($tampilkanjenis as $jenispaket) {
                          ?>
                        <option value="<?php echo $jenispaket->idjenispaket ?>"> <?php echo $jenispaket->namajenispaket ?> </option>
                        <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                      <label>Gambar</label>
                      <input type="file" name="filefoto" style="width: 100%;" >
                  </div>
                  <div class="form-group">
                    <label for="hargaperpax">Harga / Pax :</label>
                    <input required="harus diisi" type="text" name="hargaperpax" class="form-control" id="hargaperpax" placeholder="Masukkan Harga / Pax">
                  </div>
                  <div class="form-group" id="rowMinimalPeserta">
                    <label for="minimalpeserta">Minimal Peserta :</label>
                    <input required="harus diisi" type="text" name="minimalpeserta" class="form-control" id="minimalpeserta" placeholder="Masukkan Minimal Peserta">
                  </div>
              </div>

              <div class="col-md-8">
                  <div class="form-group">
                    <label for="detailpaket">Detail Paket :</label>
                    <textarea name="detailpaket" id="detailpaket" required="harus diisi" class="form-control"></textarea>
                    <!-- <input required="harus diisi" type="text" name="detailpaket" class="form-control" id="detailpaket" placeholder="Masukkan Detail paket"> -->
                  </div>

              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-success btn-md"><i class="fa fa-save"> Simpan</i></button>
                <a href="<?=base_url().'index.php/'.$controller_class?>" class="btn btn-danger btn-md">
                  <i class="fa fa-arrow-left"> Kembali</i>
                </a>                
              </div>
            </div>
          </div>
          </form>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    </div>

  <!-- /.content-wrapper -->

  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script>
  var controller_class = "<?=$controller_class?>";
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row

      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetail/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });

$(function(){
  $('#idreservasi').change(function(){
    var idreservasi = $(this).val();
    $.ajax({
      url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdatareservasi/",
      type : 'POST',
      data: {id : idreservasi},
      dataType : 'JSON',
      success : function(data){
        $('#kodejobsheet').val(data.kodejobsheet);
        $('#totaltagihan').val(data.totaltagihan);
        $('#tglkeberangkatan').val(data.tglkeberangkatan);
      }
    });
  });

  $('#btnSave').click(function(){
    $('#form_data').submit();
  });
})

$('#data-detail').dataTable( {
    paging: true,
    searching: false
} );

</script>

<script type='text/javascript'>
    $(function(){
        $('#idcustomer').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            selectresbycust(ui.item.key);
          },
        });
    });
</script>
<script type="text/javascript">
  
<?php
if (!empty($idjobsheet)) {
?>
$(function(){
  getTableDetail();
  $('#btnAddDetail').click(function(){
    saveDetail();
  });
});

function getTableDetail(){
  var idjobsheet = "<?=$idjobsheet?>";
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetailjob/",
    type : 'POST',
    data: {id : idjobsheet},
    success : function(data){
      $('#bodyDetail').empty();
      $('#bodyDetail').html(data);
      $('.delDetail').click(function(){
        var idd = $(this).attr('data-id');
        if (confirm('Yakin Dihapus ?')) {
          delDetail(idd);
        }
      });
    }
  });
}

function saveDetail(){
  var form = $('#detailjob').serialize();
  console.log(form);
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/savedetail/",
    type : 'POST',
    data: form,
    dataType : 'JSON',
    success : function(data){
      $('#nominal').val('');
      $('#vendor').val('');
      $('#namatagihan').val('');
      if(data.status){
        getTableDetail();
      }else{
        alert('Gagal menambahkan data.');
      }
    }
  });
}
function delDetail(id=''){
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/deletedetail/"+id,
    type : 'POST',
    data: {id : id},
    dataType : 'JSON',
    success : function(data){
      if(data.status){
        getTableDetail();
      }else{
        alert('Gagal hapus data.');
      }
    }
  });
}

<?php
}
?>

  function selectresbycust(user=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectrsv1"+'/'+user,
      success: function( data ) {
        $('#idreservasi').empty();
        $('#idreservasi').html(data);
      },
    });
  }
</script>
<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<!-- Page script -->

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
  
    CKEDITOR.replace('detailpaket');
   
  
  });
</script>
