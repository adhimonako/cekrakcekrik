
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    // var_dump($this->data['controller_title']);
    $this->load->view('layout/section_header',$this->data);
    ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                <?=$controller_title?>
              </h3>
              
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <div class="col-xs-8">
                <form target="_blank" name="tambahpaket" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/jumlahpembayaran">
                <div class="form-group">
                  <label for="idpaket">Nama Paket :</label>
                  <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
                  <select name="idpaket" name="idpaket" class="form-control">
                    <option value="">Semua</option>
                    <?php
                      foreach ($tampilpaket as $paket) {
                        ?>
                      <option value="<?php echo $paket->idpaket ?>"> <?php echo $paket->namapaket ?> </option>
                      <?php
                      }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="idcustomer">Customer</label>
                  <input type="text" name="idcustomer_v" id="idcustomer_v" class="form-control">
                  <input type="hidden" name="idcustomer" id="idcustomer" class="form-control">
                </div>

                <button type="submit" class="btn btn-success pull-right">Cetak</button>
              </form>
            </div>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Modal pop up ubah-->


<script type='text/javascript'>
    $(function(){
        $('#idcustomer_v').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            $('#idcustomer').val(ui.item.key);
          },
        });

    });
</script>
<script>
  var controller_class = "<?=$controller_class?>";
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row

      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetail/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });

  $(function(){
    $('#filter_status').on('change',function(){
      var status = $(this).val();
      // alert(status);
      $('#form_list').submit();
    });
  })


$('#tb_datapaket').dataTable( {
    paging: true,
    // searching: false
} );
</script>