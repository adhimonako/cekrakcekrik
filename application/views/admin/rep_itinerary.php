
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CV. Cekrakcekrik | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/assets/css/font-awesome.min.css">
  <!-- Ionicons -->

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/dist/css/skins/_all-skins.min.css"> 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.1.custom.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.ui.combogrid.css">

  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/select2/select2.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/easy-autocomplete.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/easy-autocomplete.themes.min.css">

  <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
  <!-- <link href='<?php echo base_url();?>admin/autocomplete/js/jquery.autocomplete.css' rel='stylesheet' /> -->

  <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
  <!-- <link href='<?php echo base_url();?>admin/autocomplete/css/default.css' rel='stylesheet' /> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script>

function showHint(str) {
     if (str.length == 0) { 
         document.getElementById("txtHint").innerHTML = "";
         return;
     } else {
         var xmlhttp = new XMLHttpRequest();
         xmlhttp.onreadystatechange = function() {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
             }
         }
         xmlhttp.open("GET", "gethint.php?q="+str, true);
         xmlhttp.send();
     }
}
</script>

<!-- jQuery 2.2.3 -->

<script src="<?php echo base_url(); ?>admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>


<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>admin/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.easy-autocomplete.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/nominatim.autocomplete.js"></script>

<!-- coba css 2 kolom -->
  <style type="text/css">
#kiri
{
width:50%;
height:30px;
/*background-color:#FF0;*/
float:left;
}
#kanan
{
width:50%;
height:30px;
/*background-color:#0C0;*/
float:right;
}
/*
  .ui-autocomplete-loading { 
    background:url('../img/loading81.gif') no-repeat right center;
    background-size: 32px 32px;
  }*/
 .ui-autocomplete { z-index:2147483647; }
</style>
<!-- end coba -->

  
</head>
<body>
	<div class="row">
		<div class="text-center">
			<h3>Cetak Itinerary</h3>	
		</div>
	</div>
	<!-- /.box-header -->
  <div class="box-body">
      <form name="tambahpaket" id="form_data" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/addsave">
          <div class="col-md-6">
            <?php
            if(empty($iditinerary)){
            ?>
            <div class="form-group">
              <label>
                Customer
              </label>
              <input type="text" name="idcustomer" id="idcustomer" class="form-control">
            </div>
            <div class="form-group">
              <label for="idreservasi">Reservasi :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idreservasi" id="idreservasi" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Customer Dahulu -- </option>
              </select>
            </div>      
            <div class="form-group">
              <label for="idjobsheet">Jobsheet :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idjobsheet" id="idjobsheet" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Reservasi Dahulu -- </option>
              </select>
            </div>
            <?php
            }else{
            ?>
            <div class="form-group">
              <label>
                Customer
              </label>
              <input type="text" disabled="" value="<?=$detail['namacustomer']?>" class="form-control">
            </div>
            <div class="form-group">
              <label>
                Reservasi
              </label>
              <input type="text" disabled="" value="<?=$detail['namapaket']?>" class="form-control">
            </div>
            <?php
            }
            ?>                   
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>
                Jobsheet
              </label>
              <input type="text" disabled="" value="<?=$detail['kodejobsheet']?>" class="form-control">
            </div>
            <div class="form-group">
              <label>
                Tgl Berangkat
              </label>
              <input type="text" disabled="" value="<?=$detail['tglkeberangkatan']?>" class="form-control">
            </div>
          </div>
          <input type="hidden" name="iditinerary" id="iditinerary" value="<?=$iditinerary?>">
      </form>
  </div>

  <div class="box">
            <div class="box-header">
              <h4>Detail Itinerary</h4>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table id="data-detail" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>waktumulai</th>
                        <th>waktuselesai</th>
                        <th>Deskripsi</th>
                        <th>Keterangan</th>
                      </tr>
                      </thead>
                      <tbody id="bodyDetail">
                        <?php
                        $return = "";
                        if(!empty($kegiatan)){
                          foreach ($kegiatan as $key => $value) {
                            $return .= "<tr>";
                            $return .= "<td>";
                            $return .= $value->waktumulai;
                            $return .= "</td>";
                            $return .= "<td>";
                            $return .= $value->waktuselesai;
                            $return .= "</td>";
                            $return .= "<td>";
                            $return .= $value->deskripsi;
                            $return .= "</td>";
                            $return .= "<td>";
                            $return .= $value->keterangan;
                            $return .= "</td>";
                            $return .= "</tr>";
                          }
                        }else{
                          $return .= '<tr><td colspan="4" align="center"><strong>Tidak Ada Data</strong></td></tr>';
                        }

                        echo $return;
                        ?>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
</body>