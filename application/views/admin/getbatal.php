<?php
// var_dump($detail);
?>
		<div class="table-responsive">
    <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/updatesave">
        <input type="hidden" name="idbatal" value="<?php echo $detail['idbatal']; ?>">
            <div class="form-group">
              <label for="idcustomer">Customer</label>
              <input type="text" name="idcustomer_ve" id="idcustomer_ve" class="form-control" value="<?=$detail['namacustomer']?>">
              <input type="hidden" name="idcustomer" id="idcustomer_e" class="form-control"  value="<?=$detail['idcustomer']?>">
            </div>
            <div class="form-group">
              <label for="idreservasi">Reservasi :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idreservasi" id="idreservasie" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Customer Dahulu -- </option>
              </select>
            </div>       
            <div class="form-group">
              <label for="nominal">Nominal Kembali :</label>
              <input required="harus diisi" type="text" name="nominalkembali" value="<?=$detail['nominalkembali']?>" class="form-control" id="nominalkembali" placeholder="Masukkan Nominal">
            </div>            
            <div class="form-group">
              <label for="nominal">Alasan batal :</label>
              <textarea id="alasanbatal" name="alasanbatal" class="form-control"><?=$detail['alasanbatal']?></textarea>
            </div>            
            <div class="form-group">
              <label for="statusbatal">Status Batal :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="statusbatal" id="statusbatal" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value="0" <?=(($detail['statusbatal'] == '0')? 'selected=""' : '')?> > -- Pilih Status Pembatalan -- </option>
                <option value="2" <?=(($detail['statusbatal'] == '2')? 'selected=""' : '')?> > Proses </option>
                <option value="1" <?=(($detail['statusbatal'] == '1')? 'selected=""' : '')?> > Selesai </option>
              </select>
            </div>      

            <input type="submit" value="Simpan" class="btn btn-default" />
          </form>
		</div>

<script type='text/javascript'>
    $(function(){
        $('#idcustomer_ve').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            $('#idcustomer_e').val(ui.item.key);
            selectresbycuste(ui.item.key);

          },
        });

        $('#idreservasie').change(function(){
          selectresjbste($(this).val());
        });
    });


  function selectresbycuste(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectrsv"+'/'+user+'/'+val,
      success: function( data ) {
        $('#idreservasie').empty();
        $('#idreservasie').html(data);
      },
    });
  }

  function selectresjbste(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectjbst"+'/'+user+'/'+val,
      success: function( data ) {
        $('#idjobsheete').empty();
        $('#idjobsheete').html(data);
      },
    });
  }

</script>


<script type="text/javascript">
  var idcust = "<?=$detail['idcustomer']?>";
  var idres = "<?=$detail['idreservasi']?>";
  var idjb = "<?=$detail['idjobsheet']?>";
  $(function(){
    // alert(idres);
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        dateFormat: "yy-mm-dd"
    });
    selectresbycuste(idcust,idres);
    selectresjbste(idcust,idjb);
  });
</script>