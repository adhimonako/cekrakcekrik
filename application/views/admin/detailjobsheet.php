
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    // var_dump($this->data['controller_title']);
    $this->load->view('layout/section_header',$this->data);
    ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                <?=$controller_title?>
              </h3>
              
            </div>
            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body">
                <form name="tambahpaket" id="form_data" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/addsave">
                    <div class="col-md-6">
                    <?php
                    if(!empty($idjobsheet)){
                    ?>
                      <div class="form-group">
                        <label for="namacustomer">Nama Customer:</label>
                        <input required="harus diisi" type="text" name="namacustomer" class="form-control" id="namacustomer" disabled="disabled"  value="<?=$detail['namacustomer']?>">
                      </div>  

                      <div class="form-group">
                        <label for="idpaket">Reservasi :</label>
                        <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
                        <select name="idreservasi" id="idreservasi" class="form-control" disabled="">
                          <option value=""> -- Pilih Reservasi -- </option>
                          <?php
                            $select = "";
                            foreach ($tampilreservasi as $paket) {
                              if($paket->idreservasi == $detail['idreservasi']){
                                $select = "selected='selected'";
                              }
                              ?>
                            <option value="<?php echo $paket->idreservasi ?>" <?=$select?> > <?php echo $paket->namapaket." - ".$paket->namacustomer ?> </option>
                            <?php
                            }
                          ?>
                        </select>
                      </div>  

                      <div class="form-group">
                        <label for="kodejobsheet">ID Jobsheet:</label>
                        <input required="harus diisi" type="text" name="kodejobsheet" class="form-control" id="kodejobsheet" placeholder="Masukkan Total Tagihan" value="<?=$detail['kodejobsheet']?>" disabled="">
                      </div>                                                     
                    <?php
                    }else{
                    ?>
                                  <div class="form-group">
              <label for="idcustomer">Customer</label>
              <input type="text" name="idcustomer" id="idcustomer" class="form-control">
            </div>

                      <div class="form-group">
                        <label for="idpaket">Reservasi :</label>
                        <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
                        <select name="idreservasi" id="idreservasi" class="form-control">
                          <option value=""> -- Pilih Reservasi -- </option>
                          <?php
                            $select = "";
                            foreach ($tampilreservasi as $paket) {
                              if($paket->idreservasi == $detail['idreservasi']){
                                $select = "selected='selected'";
                              }
                              ?>
                            <option value="<?php echo $paket->idreservasi ?>" <?=$select?> > <?php echo $paket->namapaket." - ".$paket->namacustomer ?> </option>
                            <?php
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="kodejobsheet">ID Jobsheet:</label>
                        <input required="harus diisi" type="text" name="kodejobsheet" class="form-control" id="kodejobsheet" placeholder="Masukkan Total Tagihan" value="<?=$detail['kodejobsheet']?>" >
                      </div>       
                    <?php
                    }
                    ?>
                               
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="tglkeberangkatan">Tanggal Keberangkatan:</label>
                        <input required="harus diisi" type="text" name="tglkeberangkatan" class="form-control" id="tglkeberangkatan" disabled="disabled"  value="<?=$detail['tglkeberangkatan']?>" >
                      </div>                      
                      <div class="form-group">
                        <label for="totaltagihan">Total Tagihan:</label>
                        <input required="harus diisi" type="text" name="totaltagihan" class="form-control" id="totaltagihan" disabled="disabled"  value="<?=$detail['totaltagihan']?>">
                      </div>                      
                    </div>
                    <input type="hidden" name="idjobsheet" id="idjobsheet" value="<?=$idjobsheet?>">
                </form>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <a href="javascript:void(0)" class="btn btn-success btn-md" id="btnSave">
                  <i class="fa fa-save"> Simpan</i>
                </a>                
                <a href="<?=base_url().'index.php/'.$controller_class?>" class="btn btn-danger btn-md">
                  <i class="fa fa-arrow-left"> Kembali</i>
                </a>                
              </div>
            </div>
          </div>
          <?php
          if(!empty($idjobsheet)){
          ?>
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h4>Detail Jobsheet</h4>
            </div>
            <div class="box-body">
              <div class="col-md-8">
                <div class="table-responsive">
                  <table id="data-detail" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama Tagihan</th>
                        <th>Nominal</th>
                        <th>Vendor</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody id="bodyDetail">

                      </tbody>
                  </table>
                </div>
              </div>
              <div class="col-md-4">
                <form name="detailjob" id="detailjob">
                  <div class="form-group">
                    <label for="namatagihan">Nama Tagihan:</label>
                    <input required="harus diisi" type="text" name="namatagihan" class="form-control" id="namatagihan" >
                  </div>         
                  <div class="form-group">
                    <label for="nominal">Nominal:</label>
                    <input required="harus diisi" type="text" name="nominal" class="form-control" id="nominal" >
                  </div>         
                  <div class="form-group">
                    <label for="vendor">Vendor:</label>
                    <input required="harus diisi" type="text" name="vendor" class="form-control" id="vendor" >
                  </div>         
                  <input type="hidden" name="idjobsheet" value="<?=$idjobsheet?>">
                </form>                
                <button class="btn btn-md btn-success pull-right" type="button" id="btnAddDetail">Simpan</button>
              </div>
            </div>
          </div>
          <?php
          }
          ?>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> Data Jobsheet
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->

  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script>
  var controller_class = "<?=$controller_class?>";
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row

      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetail/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });

$(function(){
  $('#idreservasi').change(function(){
    var idreservasi = $(this).val();
    $.ajax({
      url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdatareservasi/",
      type : 'POST',
      data: {id : idreservasi},
      dataType : 'JSON',
      success : function(data){
        $('#kodejobsheet').val(data.kodejobsheet);
        $('#totaltagihan').val(data.totaltagihan);
        $('#tglkeberangkatan').val(data.tglkeberangkatan);
      }
    });
  });

  $('#btnSave').click(function(){
    $('#form_data').submit();
  });
})

$('#data-detail').dataTable( {
    paging: true,
    searching: false
} );

</script>

<script type='text/javascript'>
    $(function(){
        $('#idcustomer').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            selectresbycust(ui.item.key);
          },
        });
    });
</script>
<script type="text/javascript">
  
<?php
if (!empty($idjobsheet)) {
?>
$(function(){
  getTableDetail();
  $('#btnAddDetail').click(function(){
    saveDetail();
  });
});

function getTableDetail(){
  var idjobsheet = "<?=$idjobsheet?>";
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetailjob/",
    type : 'POST',
    data: {id : idjobsheet},
    success : function(data){
      $('#bodyDetail').empty();
      $('#bodyDetail').html(data);
      $('.delDetail').click(function(){
        var idd = $(this).attr('data-id');
        if (confirm('Yakin Dihapus ?')) {
          delDetail(idd);
        }
      });
    }
  });
}

function saveDetail(){
  var form = $('#detailjob').serialize();
  console.log(form);
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/savedetail/",
    type : 'POST',
    data: form,
    dataType : 'JSON',
    success : function(data){
      $('#nominal').val('');
      $('#vendor').val('');
      $('#namatagihan').val('');
      if(data.status){
        getTableDetail();
      }else{
        alert('Gagal menambahkan data.');
      }
    }
  });
}
function delDetail(id=''){
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/deletedetail/"+id,
    type : 'POST',
    data: {id : id},
    dataType : 'JSON',
    success : function(data){
      if(data.status){
        getTableDetail();
      }else{
        alert('Gagal hapus data.');
      }
    }
  });
}

<?php
}
?>

  function selectresbycust(user=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectrsv1"+'/'+user,
      success: function( data ) {
        $('#idreservasi').empty();
        $('#idreservasi').html(data);
      },
    });
  }
</script>