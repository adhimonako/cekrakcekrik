<?php
// var_dump($detail);
?>
		<div class="table-responsive">
    <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/updatesave">
        <input type="hidden" name="idtagihan" value="<?php echo $detail['idtagihan']; ?>">
            <div class="form-group">
              <label for="idcustomer">Customer</label>
              <input type="text" name="idcustomer_ve" id="idcustomer_ve" class="form-control" value="<?=$detail['namacustomer']?>">
              <input type="hidden" name="idcustomer" id="idcustomer_e" class="form-control"  value="<?=$detail['idcustomer']?>">
            </div>
            <div class="form-group">
              <label for="idreservasi">Reservasi :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idreservasi" id="idreservasie" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Customer Dahulu -- </option>
              </select>
            </div>       
          <div class="form-group">
              <label for="idjobsheet">Jobsheet :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idjobsheet" id="idjobsheete" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Jobsheet -- </option>
                <?php
                  $select = "";
                  foreach ($tampiljobsheet as $paket) {
                    if($paket->idjobsheet == $detail['idjobsheet']){
                      $select = "selected='selected'";
                    }
                    ?>
                  <option value="<?php echo $paket->idjobsheet ?>" <?=$select?> > <?php echo $paket->kodejobsheet; ?> </option>
                  <?php
                  }
                ?>
              </select>
            </div>      
            <div class="form-group">
              <label for="jenis_tagihan">Jenis Tagihan :</label>
              <select name="jenis_tagihan">
                <option value="1" <?=(($paket->jenis_tagihan == 1)? 'selected=""' : '')?>> DP </option>
                <option value="2" <?=(($paket->jenis_tagihan == 2)? 'selected=""' : '')?>> Pelunasan </option>
                <option value="3" <?=(($paket->jenis_tagihan == 3)? 'selected=""' : '')?>> Tambahan </option>
              </select>
            </div>
            <div class="form-group">
              <label for="nominal">Nominal :</label>
              <input required="harus diisi" type="text" name="nominal" class="form-control" id="nominal" placeholder="Masukkan Nominal"  value="<?=$detail['nominal']?>">
            </div>            <div class="form-group">
              <label for="tgltagihan">Tanggal Tagihan :</label>
              <input required="harus diisi" type="text" name="tgltagihan" class="form-control datepicker" id="tgltagihan" placeholder="Masukkan Tanggal Tagihan" value="<?=$detail['tgltagihan']?>">
            </div>
            <div class="form-group">
              <label for="tgldeadline">Tanggal Deadline :</label>
              <input required="harus diisi" type="text" name="tgldeadline" class="form-control datepicker" id="tgldeadline" placeholder="Masukkan Tanggal Deadline" value="<?=$detail['tgldeadline']?>">
            </div>

            <input type="submit" value="Simpan" class="btn btn-default" />
          </form>
		</div>

<script type='text/javascript'>
    $(function(){
        $('#idcustomer_ve').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            $('#idcustomer_e').val(ui.item.key);
            selectresbycuste(ui.item.key);

          },
        });

        $('#idreservasie').change(function(){
          selectresjbste($(this).val());
        });
    });


  function selectresbycuste(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectrsv"+'/'+user+'/'+val,
      success: function( data ) {
        $('#idreservasie').empty();
        $('#idreservasie').html(data);
      },
    });
  }

  function selectresjbste(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectjbst"+'/'+user+'/'+val,
      success: function( data ) {
        $('#idjobsheete').empty();
        $('#idjobsheete').html(data);
      },
    });
  }

</script>


<script type="text/javascript">
  var idcust = "<?=$detail['idcustomer']?>";
  var idres = "<?=$detail['idreservasi']?>";
  var idjb = "<?=$detail['idjobsheet']?>";
  $(function(){
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        dateFormat: "yy-mm-dd"
    });
    selectresbycuste(idcust,idres);
    selectresjbste(idcust,idjb);
  });
</script>