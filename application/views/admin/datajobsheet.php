
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    // var_dump($this->data['controller_title']);
    $this->load->view('layout/section_header',$this->data);
    ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                <?=$controller_title?>
              </h3>
              
            </div>
            <div class="col-xs-12">
            <a href="<?=base_url().'index.php/'.$controller_class.'/formjobsheet'?>" class="btn btn-success btn-sm" >Tambah</a>
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="tb_datapaket" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Paket</th>
                  <th>Costumer</th>
                  <th>Jumlah Peserta</th>
                  <th>Tgl. Keberangkatan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($tampilkan as $isipaket) {
                  $no++;
                  ?>
                <tr>
                  <td><?php echo $no ; ?></td>
                  <td><?php echo $isipaket->namapaket; ?></td>
                  <td><?php echo $isipaket->namacustomer; ?></td>
                  <td><?php echo $isipaket->jumlahpeserta; ?></td>
                  <td><?php echo $isipaket->tglkeberangkatan; ?></td>
                  <td>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isipaket->idjobsheet; ?>" id="getpaket" data-target="#view-modal">Detail</button>
                    <a href="<?php echo base_url(); ?>index.php/<?=$controller_class?>/delete/<?=$isipaket->idjobsheet ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button>
                  </td>
                </tr>
                <?php
                }
                ?>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- Modal tambah paket-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Tambah</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
            <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/addsave">
            <div class="form-group">
              <label for="idpaket">Reservasi :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idreservasi" name="idreservasi" class="form-control">
                <?php
                  foreach ($tampilreservasi as $paket) {
                    ?>
                  <option value="<?php echo $paket->idreservasi ?>"> <?php echo $paket->namapaket." - ".$paket->namacustomer ?> </option>
                  <?php
                  }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label for="totaltagihan">Total Tagihan:</label>
              <input required="harus diisi" type="text" name="totaltagihan" class="form-control" id="totaltagihan" placeholder="Masukkan Total Tagihan">
            </div>

            <button type="submit" class="btn btn-default">Simpan</button>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- Modal pop up ubah-->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> Data Jobsheet
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->

  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script>
  var controller_class = "<?=$controller_class?>";
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row
      window.location = "<?php echo base_url(); ?>index.php/<?=$controller_class?>/formjobsheet/"+uid;

    });
    
  });


$('#tb_datapaket').dataTable( {
    paging: true,
    // searching: false
} );
</script>