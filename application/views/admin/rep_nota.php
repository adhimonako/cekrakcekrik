<?php

    function terbilang( $num ,$dec=4){
      $stext = array(
        "Nol",
        "Satu",
        "Dua",
        "Tiga",
        "Empat",
        "Lima",
        "Enam",
        "Tujuh",
        "Delapan",
        "Sembilan",
        "Sepuluh",
        "Sebelas"
      );
      $say  = array(
        "Ribu",
        "Juta",
        "Milyar",
        "Triliun",
        "Biliun", // remember limitation of float
        "--apaan---" ///setelah biliun namanya apa?
      );
      $w = "";

      if ($num <0 ) {
        $w  = "Minus ";
        //make positive
        $num *= -1;
      }

      $snum = number_format($num,$dec,",",".");
      //die($snum);
      $strnum =  explode(".",substr($snum,0,strrpos($snum,",")));
      //parse decimalnya
      $koma = substr($snum,strrpos($snum,",")+1);

      $isone = substr($num,0,1)  ==1;
      if (count($strnum)==1) {
        $num = $strnum[0];
        switch (strlen($num)) {
          case 1:
          case 2:
            if (!isset($stext[$strnum[0]])){
              if($num<19){
                $w .=$stext[substr($num,1)]." Belas";
              }else{
                $w .= $stext[substr($num,0,1)]." Puluh ".
                  (intval(substr($num,1))==0 ? "" : $stext[substr($num,1)]);
              }
            }else{
              $w .= $stext[$strnum[0]];
            }
            break;
          case 3:
            $w .=  ($isone ? "Seratus" : terbilang(substr($num,0,1)) .
              " Ratus").
              " ".(intval(substr($num,1))==0 ? "" : terbilang(substr($num,1)));
            break;
          case 4:
            $w .=  ($isone ? "Seribu" : terbilang(substr($num,0,1)) .
              " Ribu").
              " ".(intval(substr($num,1))==0 ? "" : terbilang(substr($num,1)));
            break;
          default:
            break;
        }
      }else{
        $text = $say[count($strnum)-2];
        $w = ($isone && strlen($strnum[0])==1 && count($strnum) <=3? "Se".strtolower($text) : terbilang($strnum[0]).' '.$text);
        array_shift($strnum);
        $i =count($strnum)-2;
        foreach ($strnum as $k=>$v) {
          if (intval($v)) {
            $w.= ' '.terbilang($v).' '.($i >=0 ? $say[$i] : "");
          }
          $i--;
        }
      }
      $w = trim($w);
      if ($dec = intval($koma)) {
        $w .= " Koma ". terbilang($koma);
      }
      return trim($w);
    }
    
        function formatNumber($num, $dec = null, $ceknull = false) {
        if ($ceknull and ! isset($num))
            return null;

        list(, $left) = explode('.', strval($num));

        $left = (float) $left;
        if (empty($left))
            $len = 0;
        else
            $len = strlen($left);

        if (!isset($dec))
            $dec = $len;

        return number_format($num, $dec, ',', '.');
    }

    
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CV. Cekrakcekrik | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/assets/css/font-awesome.min.css">
  <!-- Ionicons -->

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/dist/css/skins/_all-skins.min.css"> 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.1.custom.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.ui.combogrid.css">

  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin/plugins/select2/select2.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/easy-autocomplete.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/easy-autocomplete.themes.min.css">

  <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
  <!-- <link href='<?php echo base_url();?>admin/autocomplete/js/jquery.autocomplete.css' rel='stylesheet' /> -->

  <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
  <!-- <link href='<?php echo base_url();?>admin/autocomplete/css/default.css' rel='stylesheet' /> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script>

function showHint(str) {
     if (str.length == 0) { 
         document.getElementById("txtHint").innerHTML = "";
         return;
     } else {
         var xmlhttp = new XMLHttpRequest();
         xmlhttp.onreadystatechange = function() {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
             }
         }
         xmlhttp.open("GET", "gethint.php?q="+str, true);
         xmlhttp.send();
     }
}
</script>

<!-- jQuery 2.2.3 -->

<script src="<?php echo base_url(); ?>admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>


<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>admin/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.easy-autocomplete.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/nominatim.autocomplete.js"></script>

<!-- coba css 2 kolom -->
  <style type="text/css">
#kiri
{
width:50%;
height:30px;
/*background-color:#FF0;*/
float:left;
}
#kanan
{
width:50%;
height:30px;
/*background-color:#0C0;*/
float:right;
}
/*
  .ui-autocomplete-loading { 
    background:url('../img/loading81.gif') no-repeat right center;
    background-size: 32px 32px;
  }*/
 .ui-autocomplete { z-index:2147483647; }
</style>
<!-- end coba -->

  
</head>
<body onload="window.print();">
<?php
$strtime = strtotime($detail['tglbayar']);
$tglbayar = date('d-m-Y',$strtime);
?>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> CV. CekrakCekrik.
          <small class="pull-right">Date: <?=date('d/m/Y',$strtime)?></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Invoice #<?=date('ym',$strtime).str_pad($detail['idreservasi'],3,"0",STR_PAD_LEFT)?></b><br>
        <br>
        <b>Order ID:</b> <?=$detail['kodejobsheet']?><br>
        <b>Payment Due:</b> <?=date('d/m/Y',$strtime)?><br>
      </div>
      <div class="col-sm-4 invoice-col">
        <h3 class="text-center">Bukti Pembayaran</h3>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <br>
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <!-- <thead>
          <tr>
            <th>Qty</th>
            <th>Product</th>
            <th>Serial #</th>
            <th>Description</th>
            <th>Subtotal</th>
          </tr>
          </thead> -->
          <tbody>
          <tr>
            <td width="30%">Terima Dari </td>
            <td>
              <h4>
                <?=$detail['namacustomer']?>
              </h4>
            </td>
          </tr>
          <tr>
            <td>Uang Sejumlah </td>
            <td>
              <h4>
                <strong>Rp. 
                 <?php
              echo formatNumber($detail['nominal'],2);
              ?>
              </strong>
              </h4>
              <h4>
              <?php
              echo terbilang($detail['nominal']);
              ?> Rupiah
              </h4>
            </td>
          </tr>
          <tr>
            <td>Untuk Pembayaran </td>
            <td>
              <?php
              if($detail['jenis_tagihan'] == 1){
              $jenis = "Uang Muka";
              }else if($detail['jenis_tagihan'] == 2){
              $jenis = "Pelunasan";
              }else{
              $jenis = "Tambahan";
              }
              ?>
              <h4><strong><?=$jenis?></strong></h4>
              <h4><?=$detail['namapaket']?></h4>
              <h4>Jumlah Peserta : <strong><?=$detail['jumlahpeserta']?></strong></h4>
              <h4>Tanggal Keberangkatan : <strong><?=date('d-M-Y',strtotime($detail['tglkeberangkatan']))?></strong></h4>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-6">

      </div>
      <!-- /.col -->
      <div class="col-xs-6">
        <p class="lead">Surabaya, <?=date('d-M-Y',$strtime)?></p>
        <br>
        <br>
        <p class="lead">CV. Cekrakcekrik</p>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->


</body>

</html>