
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    // var_dump($this->data['controller_title']);
    $this->load->view('layout/section_header',$this->data);
    ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                <?=$controller_title?>
              </h3>
              
            </div>
            <div class="col-xs-12">
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah">Tambah</button>
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="tb_datapaket" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Paket</th>
                  <th>Costumer</th>
                  <th>Nominal Tagihan</th>
                  <th>Nominal Bayar</th>
                  <th>Batal</th>
                  <th>Bukti Bayar</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($tampilkan as $key => $isipaket) {
                  $no++;
                  ?>
                <tr>
                  <td><?php echo $no ; ?></td>
                  <td><?php echo $isipaket->namapaket; ?></td>
                  <td><?php echo $isipaket->namacustomer; ?></td>
                  <td><?php echo $isipaket->nominaltagihan; ?></td>
                  <td><?php echo $isipaket->nominal; ?></td>
                  <td><?php echo (empty($isipaket->isvoid)? "" : "Batal"); ?></td>
                  <td>
                    <?php
                    if(getimagesize(base_url().'assets/images/'.$isipaket->filefoto)):
                    ?>
                    <img src="<?php echo base_url().'assets/images/'.$isipaket->filefoto;?>" style="width:90px;">
                    <?php
                    endif;
                    ?>
                  </td>
                  <td>
                    <?php
                    if(!empty($isipaket->ajukanbatal) and empty($isipaket->isvoid)) :
                    ?>
                    <button type="button" class="btn btn-warning btn-sm voidbayar" data-toggle="modal" data-id="<?php echo $isipaket->idpembayaran; ?>" >Setujui Batal</button>
                    <?php
                    endif;
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isipaket->idpembayaran; ?>" id="getpaket" data-target="#view-modal">Ubah</button>
                    <a href="<?php echo base_url(); ?>index.php/<?=$controller_class?>/cetaknota/<?=$isipaket->idpembayaran ?>" target="_blank"><button type="button" class="btn btn-sm btn-success">Cetak</button>
                    <a href="<?php echo base_url(); ?>index.php/<?=$controller_class?>/delete/<?=$isipaket->idpembayaran ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button>
                  </td>
                </tr>
                <?php
                }
                ?>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- Modal tambah paket-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Tambah</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">

            <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/addsave" enctype="multipart/form-data">
            <div class="form-group">
              <label for="idcustomer">Customer</label>
              <input type="text" name="idcustomer" id="idcustomer" class="form-control">
            </div>
            <div class="form-group">
              <label for="idreservasi">Reservasi :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idreservasi" id="idreservasi" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Customer Dahulu -- </option>
              </select>
            </div>      
            <div class="form-group">
              <label for="idjobsheet">Jobsheet :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idjobsheet" id="idjobsheet" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Jobsheet -- </option>
              </select>
            </div>      
            <div class="form-group">
              <label for="idtagihan">Tagihan :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idtagihan" id="idtagihan" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Jobsheet Dahulu -- </option>
              </select>
            </div>      
            <div class="form-group">
              <label for="nominal">Nominal :</label>
              <input required="harus diisi" type="text" name="nominal" class="form-control" id="nominal" placeholder="Masukkan Nominal">
            </div>            <div class="form-group">
              <label for="tglbayar">Tanggal Bayar :</label>
              <input required="harus diisi" type="text" name="tglbayar" class="form-control datepicker" id="tglbayar" placeholder="Masukkan Tanggal Bayar">
            </div>
            <div class="form-group">
                <label>Bukti Bayar</label>
                <input type="file" name="filefoto" style="width: 100%;" >
              </div>
            <button type="submit" class="btn btn-default">Simpan</button>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- Modal pop up ubah-->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> Data Pembayaran
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->

  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  
<script type='text/javascript'>
    $(function(){
        $('#idcustomer').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            selectresbycust(ui.item.key);
          },
        });

        $('#idreservasi').change(function(){
          selectresjbst($(this).val());
        });
        $('#idjobsheet').change(function(){
          selecttagihan($(this).val());
        });
        $('#idtagihan').change(function(){
          var val = $(this).val();
          var txt = $("#idtagihan option:selected").text();
          var arrTxt = txt.split(' - ');
          var nominal = arrTxt[2];
          // alert(nominal);
          $('#nominal').val(nominal);
        });
    });
</script>

<script>
  var controller_class = "<?=$controller_class?>";
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row

      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetail/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });


  function selectresbycust(user=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectrsv"+'/'+user,
      success: function( data ) {
        $('#idreservasi').empty();
        $('#idreservasi').html(data);
      },
    });
  }

  function selectresjbst(user=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectjbst"+'/'+user,
      success: function( data ) {
        $('#idjobsheet').empty();
        $('#idjobsheet').html(data);
      },
    });
  }

  function selecttagihan(user=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selecttagihan"+'/'+user,
      success: function( data ) {
        $('#idtagihan').empty();
        $('#idtagihan').html(data);
      },
    });
  }


$('#tb_datapaket').dataTable( {
    paging: true,
    searching: false
} );
</script>