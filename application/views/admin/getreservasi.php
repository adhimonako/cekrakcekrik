<?php
// var_dump($detail);
?>
		<div class="table-responsive">
    <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/updatesave">
        <input type="hidden" name="idreservasi" value="<?php echo $detail['idreservasi']; ?>">

          <div class="form-group">
              <label for="idpaket">Nama Paket :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idpaket" name="idpaket" class="form-control">
                <?php
                $val = "";
                  foreach ($tampilpaket as $paket) {
                    if($paket->idpaket == $detail['idpaket']){
                      $val = "select='select'";
                    }
                    ?>
                  <option value="<?php echo $paket->idpaket ?>" <?=$val?> > <?php echo $paket->namapaket ?> </option>
                  <?php
                  }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label for="idcustomer">Customer</label>
              <input type="text" name="idcustomer_ve" id="idcustomer_ve" class="form-control" value="<?=$detail['namacustomer']?>">
              <input type="hidden" name="idcustomer" id="idcustomer_e" class="form-control"  value="<?=$detail['idcustomer']?>">
            </div>
            <div class="form-group">
              <label for="jumlahpeserta">Jumlah Peserta :</label>
              <input required="harus diisi" type="text" name="jumlahpeserta" class="form-control" id="jumlahpeserta" placeholder="Masukkan Jumlah Peserta" value="<?php echo $detail['jumlahpeserta']; ?>">
            </div>
            <div class="form-group">
              <label for="tglkeberangkatan">Tanggal Reservasi :</label>
              <input required="harus diisi" type="text" name="tgl_dibuat" class="form-control datepicker" value="<?php echo $detail['tgl_dibuat']; ?>" id="tgl_dibuat" placeholder="Masukkan Tanggal Reservasi">
            </div>
            <div class="form-group">
              <label for="tglkeberangkatan">Tanggal Keberangkatan :</label>
              <input required="harus diisi" type="text" name="tglkeberangkatan" class="form-control datepicker" id="tglkeberangkatan" placeholder="Masukkan Tanggal Keberangkatan" value="<?php echo $detail['tglkeberangkatan']; ?>">
            </div>

            <input type="submit" value="Simpan" class="btn btn-default" />
          </form>
		</div>
			
<script type='text/javascript'>
    $(function(){
        $('#idcustomer_ve').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            $('#idcustomer_e').val(ui.item.key);
          },
        });

    });
</script>


<script type="text/javascript">
  $(function(){
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        dateFormat: "yy-mm-dd"
    });
  });
</script>