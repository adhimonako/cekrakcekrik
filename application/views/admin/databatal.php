
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    // var_dump($this->data['controller_title']);
    $this->load->view('layout/section_header',$this->data);
    ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                <?=$controller_title?>
              </h3>
              
            </div>
            <form name="form_list" id="form_list" method="POST" action="">

            <div class="col-xs-12">
              <div class="col-md-5">
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah">Tambah</button>
              </div>
              <div class="col-md-7">
                  
                <div class="form-group pull-right">
                  <label for="filter-status" class="col-md-4">
                    Status
                  </label>
                  <div class="col-md-8">
                    <select id="filter_status" name="filter_status" class="form-control">
                      <option value="all" <?=(($filter_status == 'all' )? 'selected=""' : '')?>> -- Semua --</option>
                      <option value="0" <?=(($filter_status == '0' )? 'selected=""' : '')?>>Belum bayar</option>
                      <option value="1" <?=(($filter_status == 1 )? 'selected=""' : '')?>>Lunas</option>
                      <option value="10" <?=(($filter_status == 10 )? 'selected=""' : '')?>>Pending</option>
                    </select>                    
                  </div>
                </div>                
              </div>
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="tb_datapaket" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Costumer</th>
                  <th>Nama Paket</th>
                  <th>Alasan</th>
                  <th>Nominal Kembali</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($tampilkan as $isipaket) {
                  $no++;
                  ?>
                <tr>
                  <td><?php echo $no ; ?></td>
                  <td><?php echo $isipaket->namacustomer; ?></td>
                  <td><?php echo $isipaket->namapaket; ?></td>
                  <td><?php echo $isipaket->alasanbatal; ?></td>
                  <td><?php echo $isipaket->nominalkembali; ?></td>
                  <td>
                    <?php 
                      $statusbatal = $isipaket->statusbatal;
                      if($statusbatal == '0'){
                        echo '<span class="alert-danger">Pengajuan Batal</span>';
                      }else if($statusbatal == '2'){
                         echo '<span class="alert-warning">Proses Batal</span>';
                      }else if($statusbatal == '1'){
                         echo '<span class="alert-success">Selesai</span>';
                      }
                      ?>
                  </td>
                  <td>
                    <?php
                    if($isipaket->statusbatal != '1'){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isipaket->idbatal; ?>" id="getpaket" data-target="#view-modal">Ubah</button>
                    <a href="<?php echo base_url(); ?>index.php/<?=$controller_class?>/delete/<?=$isipaket->idtagihan ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button>
                    <?php
                    }
                    ?>
                  </td>
                </tr>
                <?php
                }
                ?>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- Modal tambah paket-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Tambah Pembatalan</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">

            <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/addsave">        
            <div class="form-group">
              <label for="idcustomer">Customer</label>
              <input type="text" name="idcustomer" id="idcustomer" class="form-control">
            </div>
            <div class="form-group">
              <label for="idreservasi">Reservasi :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idreservasi" id="idreservasi" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Customer Dahulu -- </option>
              </select>
            </div>      
            <div class="form-group">
              <label for="nominal">Nominal Kembali :</label>
              <input required="harus diisi" type="text" name="nominalkembali" class="form-control" id="nominalkembali" placeholder="Masukkan Nominal">
            </div>            
            <div class="form-group">
              <label for="nominal">Alasan batal :</label>
              <textarea id="alasanbatal" name="alasanbatal" class="form-control"></textarea>
            </div>            
            <div class="form-group">
              <label for="statusbatal">Status Batal :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="statusbatal" id="statusbatal" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value="0"> -- Pilih Status Pembatalan -- </option>
                <option value="2"> Proses </option>
                <option value="1"> Selesai </option>
              </select>
            </div>      

            <button type="submit" class="btn btn-default">Simpan</button>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- Modal pop up ubah-->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> Data Pembatalan
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->

<script type='text/javascript'>
    $(function(){
        $('#idcustomer').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            selectresbycust(ui.item.key);
          },
        });

        $('#idreservasi').change(function(){
          selectresjbst($(this).val());
        });
    });
</script>
<script>
  var controller_class = "<?=$controller_class?>";
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row

      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetail/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });

  function selectresbycust(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectrsv"+'/'+user+'/'+val,
      success: function( data ) {
        $('#idreservasi').empty();
        $('#idreservasi').html(data);
      },
    });
  }

  function selectresjbst(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectjbst"+'/'+user+'/'+val,
      success: function( data ) {
        $('#idjobsheet').empty();
        $('#idjobsheet').html(data);
      },
    });
  }

  $(function(){
    $('#filter_status').on('change',function(){
      var status = $(this).val();
      // alert(status);
      $('#form_list').submit();
    });
  })


$('#tb_datapaket').dataTable( {
    paging: true,
    // searching: false
} );
</script>

