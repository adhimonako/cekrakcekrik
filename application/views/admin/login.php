<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
if ($this->session->userdata('logged_in')) {
	redirect('welcome/kosong');
	# code...
}
?>
<!DOCTYPE html>
<html>
<head>
<title>CV. Cekrakcekrik</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Credit Login / Register Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="<?php echo base_url();?>admin/login/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
<link href="<?php echo base_url();?>admin/login/css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>admin/login/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- //Custom Theme files -->
<!-- web font -->
<link href="//fonts.googleapis.com/css?family=Oswald:400,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

<!-- //web font -->
</head>
<body>
<h1>SIM Reservasi Paket Fotografi dan Tour</h1>
<div class="main-agileits">
<!--form-stars-here-->
		<div class="form-w3-agile">
			<h2>login form</h2>
			<?php
				if($this->session->flashdata('login_failed')){
			?>
			<p class="alert alert-dismissable alert-success"><?php echo $this->session->flashdata('login_failed');?></p>
			<?php
			}
			if ($this->session->flashdata('logout')) {
				# code...
			?>
			<p class="alert alert-dismissable alert-success"><?php echo $this->session->flashdata('logout');?></p>
			
    		<?php
    		}
    		?>

			<form action="<?php echo base_url();?>index.php/admin/cont_datauser/login" method="post">
				<div class="form-sub-w3">
					<input type="text" name="username" id="username" placeholder="username" required="" />
				<div class="icon-w3">
					<i class="fa fa-user" aria-hidden="true"></i>
				</div>
				</div>
				<div class="form-sub-w3">
					<input type="password" name="password" id="password" placeholder="Password" required="" />
				<div class="icon-w3">
					<i class="fa fa-unlock-alt" aria-hidden="true"></i>
				</div>
				</div>
				<div class="submit-w3l">
					<input type="submit" value="Login">
				</div>
			</form>
		</div>
<!--//form-ends-here-->
</div>

<!-- copyright -->
	<div class="copyright w3-agile">
		<p> © 2019 CV. Cekrakcekrik </p>
	</div>
	<!-- //copyright -->
	<script type="text/javascript" src="<?php echo base_url();?>admin/login/js/jquery-2.1.4.min.js"></script>
	<!-- pop-up-box -->
		<script src="<?php echo base_url();?>admin/login/js/jquery.magnific-popup.js" type="text/javascript"></script>
	<!--//pop-up-box -->
	<script>
		$(document).ready(function() {
		$('.w3_play_icon,.w3_play_icon1,.w3_play_icon2').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

		});
	</script>
</body>
</html>
