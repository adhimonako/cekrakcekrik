
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        W E L C O M E
        <small>Sistem Informasi Manajemen Reservasi CV. Cekrakcekrik</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>index.php/welcome/kosong"><i class="fa fa-dashboard"></i> Halaman Utama</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/cont_datafaq/datafaq">Data FAQ</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">Master Data Tabel FAQ Manager</h3>
              
            </div>
            <div class="col-xs-12">
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah">Tambah FAQ</button>
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="tb_datafaq" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Tanya</th>
                  <th>Jawab</th>
                  <th>Keterangan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($tampilkan as $isifaq) {
                  $no++;
                  ?>
                <tr>
                  <td><?php echo $no ; ?></td>
                  <td><?php echo $isifaq->tanya; ?></td>
                  <td><?php echo $isifaq->jawab; ?></td>
                  <td><?php echo $isifaq->keterangan; ?></td>
                  <td>
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isifaq->idfaq; ?>" id="getfaq" data-target="#view-modal">Ubah</button>
                    <a href="<?php echo base_url(); ?>index.php/admin/cont_datafaq/deletefaq/<?=$isifaq->idfaq ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button>
                  </td>
                </tr>
                <?php
                }
                ?>
                
                </tbody>
              </table>

              

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- Modal tambah faq-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Tambah FAQ</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
          <!-- <form name="uploadfoto" method="post" action="<?= base_url() ?>index.php/admin/cont_datafaq/upload" enctype="multipart/form-data" >
            <div class="form-group">
              <label>Foto</label><p></p>
              <input class="form-control"  type="file" name="foto_faq" onchange="submit();" />
              <img src="<?php base_url(); ?>admin/dist/img/faq2-160x160.jpg">

            </div>
            </form> -->
            <form name="tambahfaq" method="post" action="<?= base_url() ?>index.php/admin/cont_datafaq/addfaq">
            <div class="form-group">
              <label for="tanya">Tanya :</label>
              <input required="Harus Diisi" type="text" name="tanya" class="form-control" id="tanya" placeholder="Masukkan Pertanyaan">
            </div>
            <div class="form-group">
              <label for="jawab">Jawab :</label>
              <input required="Harus Diisi" type="text" name="jawab" class="form-control" id="jawab" placeholder="Masukkan Jawaban">
            </div>
            <div class="form-group">
              <label for="keterangan">Keterangan :</label>
              <input required="Harus Diisi" type="text" name="keterangan" class="form-control" id="keterangan" placeholder="Masukkan Keterangan">
            </div>


            <button type="submit" class="btn btn-default">Simpan</button>
          </form>
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


    <!-- Modal pop up ubah-->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> FAQ
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->
  <!--Upload Modal-->
  <div id="uploadfoto" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Foto</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
          <form name="uploadfoto" method="post" action="<?= base_url() ?>index.php/admin/cont_datafaq/upload" enctype="multipart/form-data" >
            <div class="form-group">
              <label>Foto</label><p></p>
              <input class="form-control"  type="file" name="foto_faq" onchange="submit();" />
            </div>
            </form>
            
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <!--End of Upload modal-->
  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script>
  $(document).ready(function(){
    
    $(document).on('click', '#getfaq', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row
      
      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/admin/cont_datafaq/getfaqid/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });


$('#tb_datafaq').dataTable( {
    paging: true,
    searching: false
} );
</script>