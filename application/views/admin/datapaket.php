
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        W E L C O M E
        <small>Sistem Informasi Manajemen Reservasi CV. Cekrakcekrik</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>index.php/welcome/kosong"><i class="fa fa-dashboard"></i> Halaman Utama</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/cont_datapaket/datapaket">Data paket</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">Master Data Tabel Paket Manager</h3>
              
            </div>
            <div class="col-xs-12">
            <a href="<?=base_url().'admin/cont_datapaket/'.'formpaket'?>" class="btn btn-success btn-sm">Tambah Paket</a>
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="tb_datapaket" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th></th>
                  <th>Nama Paket</th>
                  <th>Jenis Paket</th>
                  <th>Detail Paket</th>
                  <th>Harga / Pax</th>
                  <th>Minimal Peserta</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($tampilkan as $isipaket) {
                  $no++;
                  ?>
                <tr>
                  <td><?php echo $no ; ?></td>
                  <td><img src="<?php echo base_url().'assets/images/'.$isipaket->filefoto;?>" style="width:90px;"></td>
                  <td><?php echo $isipaket->namapaket; ?></td>
                  <td><?php echo $isipaket->namajenispaket; ?></td>
                  <td><?php echo $isipaket->detailpaket; ?></td>
                  <td><?php echo $isipaket->hargaperpax; ?></td>
                  <td><?php echo $isipaket->minimalpeserta; ?></td>
                  <td>
                    <!-- <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="<?php echo $isipaket->idpaket; ?>" id="getpaket" data-target="#view-modal">Ubah</button> -->
                    <a href="<?php echo base_url(); ?>index.php/admin/cont_datapaket/detailpaket/<?=$isipaket->idpaket ?>" class="btn btn-warning btn-sm" >Ubah</a>
                    <a href="<?php echo base_url(); ?>index.php/admin/cont_datapaket/deletepaket/<?=$isipaket->idpaket ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button>
                  </td>
                </tr>
                <?php
                }
                ?>
                
                </tbody>
              </table>

              

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- Modal tambah paket-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Tambah Paket</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
          <!-- <form name="uploadfoto" method="post" action="<?= base_url() ?>index.php/admin/cont_datapaket/upload" enctype="multipart/form-data" >
            <div class="form-group">
              <label>Foto</label><p></p>
              <input class="form-control"  type="file" name="foto_paket" onchange="submit();" />
              <img src="<?php base_url(); ?>admin/dist/img/paket2-160x160.jpg">

            </div>
            </form> -->
            <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/admin/cont_datapaket/addpaket" enctype="multipart/form-data">
            <div class="form-group">
              <label for="namapaket">Nama Paket :</label>
              <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket">
            </div>
            <div class="form-group">
              <label for="idjenispaket">Jenis Paket :</label>
              <select name="idjenispaket" id="idjenispaket">
                <?php
                  foreach ($tampilkanjenis as $jenispaket) {
                    ?>
                  <option value="<?php echo $jenispaket->idjenispaket ?>"> <?php echo $jenispaket->namajenispaket ?> </option>
                  <?php
                  }
                ?>
              </select>
            </div>
            <div class="form-group">
                <label>Gambar</label>
                <input type="file" name="filefoto" style="width: 100%;" >
              </div>
            <div class="form-group">
              <label for="detailpaket">Detail Paket :</label>
              <textarea name="detailpaket" id="detailpaket" required="harus diisi" class="form-control"></textarea>
              <!-- <input required="harus diisi" type="text" name="detailpaket" class="form-control" id="detailpaket" placeholder="Masukkan Detail paket"> -->
            </div>
            <div class="form-group">
              <label for="hargaperpax">Harga / Pax :</label>
              <input required="harus diisi" type="text" name="hargaperpax" class="form-control" id="hargaperpax" placeholder="Masukkan Harga / Pax">
            </div>
            <div class="form-group" id="rowMinimalPeserta">
              <label for="minimalpeserta">Minimal Peserta :</label>
              <input required="harus diisi" type="text" name="minimalpeserta" class="form-control" id="minimalpeserta" placeholder="Masukkan Minimal Peserta">
            </div>

            <button type="submit" class="btn btn-default">Simpan</button>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- Modal pop up ubah-->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> Data Paket
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->

  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script>
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row
      
      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/admin/cont_datapaket/getpaketid/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });

  $(function(){

    $('#idjenispaket').change(function(){
      var idjenispaket = $(this).val();
      if(idjenispaket > 1){
        $('#minimalpeserta').val(1);
        $('#minimalpeserta').prop('disabled','disabled');
      }else{
        $('#minimalpeserta').val('');
        $('#minimalpeserta').prop('disabled','');
      }
    });
  });


$('#tb_datapaket').dataTable( {
    paging: true,
    searching: false
} );
</script>
<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<!-- Page script -->

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
  
    CKEDITOR.replace('detailpaket');
   
  
  });
</script>
