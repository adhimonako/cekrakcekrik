<?php
$master = array("cont_datauser", "cont_paket");
?>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><b>MENU UTAMA</b></li>
        <?php 
          if($this->session->userdata('level') == '1'){ 
        ?>
        <li class="treeview <?php if (in_array($this->uri->segment(1), $master)){echo 'active';}?>">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Master Data</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(1) == "cont_datauser"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datauser/datauser"><i class="fa fa-users"></i> Data User Manager</a></li>
            <li <?php if($this->uri->segment(1) == "cont_datapaket"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datapaket/datapaket"><i class="fa fa-circle"></i> Data Paket</a></li>
            <li <?php if($this->uri->segment(1) == "cont_datajenispaket"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datajenispaket/datajenispaket"><i class="fa fa-circle"></i> Data Jenis Paket</a></li>
            <li <?php if($this->uri->segment(1) == "cont_datacustomer"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datacustomer/datacustomer"><i class="fa fa-circle"></i> Data Customer</a></li>
            <li <?php if($this->uri->segment(1) == "cont_datapegawai"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datapegawai/datapegawai"><i class="fa fa-circle"></i> Data Pegawai</a></li>
            <li <?php if($this->uri->segment(1) == "cont_datajabatan"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datajabatan/datajabatan"><i class="fa fa-circle"></i> Data Jabatan</a></li>
            <li <?php if($this->uri->segment(1) == "cont_dataprofile"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_dataprofile/dataprofile"><i class="fa fa-circle"></i> Data Profile</a></li>
            <li <?php if($this->uri->segment(1) == "cont_datacontact"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datacontact/datacontact"><i class="fa fa-circle"></i> Data Contact</a></li>
            <li <?php if($this->uri->segment(1) == "cont_datafaq"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datafaq/datafaq"><i class="fa fa-circle"></i> Data FAQ</a></li>
            <li <?php if($this->uri->segment(1) == "cont_databank"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_databank/databank"><i class="fa fa-circle"></i> Data Bank</a></li>
          </ul>
        </li>

        <li class="treeview <?php if (in_array($this->uri->segment(1), $master)){echo 'active';}?>">
          <a href="#">
              <i class="fa fa-files-o"></i> <span>Transaksi</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(1) == "cont_datareservasi"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datareservasi/datareservasi"><i class="fa fa-users"></i>Data Reservasi </a></li>
            <li <?php if($this->uri->segment(1) == "cont_datajobsheet"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_datajobsheet/datajobsheet"><i class="fa fa-users"></i>Data Jobsheet </a></li>
            <li <?php if($this->uri->segment(1) == "cont_dataitinerary"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_dataitinerary/dataitinerary"><i class="fa fa-circle"></i>Data Itinerary </a></li>
            <li <?php if($this->uri->segment(1) == "cont_datatagihan"){echo "class='active'";}?>><a 
            href="<?php echo base_url(); ?>index.php/admin/cont_datatagihan/datatagihan"><i class="fa fa-circle"></i>Data Tagihan </a></li>
            <li <?php if($this->uri->segment(1) == "cont_datapembayaran"){echo "class='active'";}?>><a 
            href="<?php echo base_url(); ?>index.php/admin/cont_datapembayaran/datapembayaran"><i class="fa fa-circle"></i>Data Pembayaran </a></li>
            <li <?php if($this->uri->segment(1) == "cont_databatal"){echo "class='active'";}?>><a 
            href="<?php echo base_url(); ?>index.php/admin/cont_databatal/"><i class="fa fa-circle"></i>Pembatalan </a></li>
          </ul>
        </li>

        <li class="treeview <?php if (in_array($this->uri->segment(1), $master)){echo 'active';}?>">
          <a href="#">
              <i class="fa fa-files-o"></i> <span>Laporan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(1) == "cont_lap_jumlahreservasi"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_lap_jumlahreservasi/"><i class="fa fa-circle"></i>Jumlah Reservasi </a></li>
            <li <?php if($this->uri->segment(1) == "cont_lap_jumlahjobsheet"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_lap_jumlahjobsheet/"><i class="fa fa-circle"></i>Jumlah Jobsheet </a></li>
            <li <?php if($this->uri->segment(1) == "cont_lap_jumlahtagihan"){echo "class='active'";}?>><a href="<?php echo base_url(); ?>index.php/admin/cont_lap_jumlahtagihan/"><i class="fa fa-circle"></i>Jumlah Tagihan </a></li>
            <li <?php if($this->uri->segment(1) == "cont_lap_jumlahpembayaran"){echo "class='active'";}?>> <a href="<?php echo base_url(); ?>index.php/admin/cont_lap_jumlahpembayaran/"><i class="fa fa-circle"></i>Jumlah Pembayaran </a></li>
            <!-- <li <?php if($this->uri->segment(1) == "cont_lap_jumlahpendapatan"){echo "class='active'";}?>><a ref="<?php echo base_url(); ?>index.php/admin/cont_lap_jumlahpendapatan/jumlahpendapatan"><i class="fa fa-circle"></i>Jumlah Pendapatan </a></li> -->
          </ul>
        </li>
        <?php
        }
        ?>
        <li>
          <a href="<?php echo base_url().'admin/dashboard'?>">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>Post</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/tulisan/add_tulisan'?>"><i class="fa fa-thumb-tack"></i> Add New</a></li>
            <li><a href="<?php echo base_url().'admin/tulisan'?>"><i class="fa fa-list"></i> Post Lists</a></li>
            <li><a href="<?php echo base_url().'admin/kategori'?>"><i class="fa fa-wrench"></i> Kategori</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-code"></i>
            <span>Portfolio</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/portfolio/add_portfolio'?>"><i class="fa fa-thumb-tack"></i> Add Portfolio</a></li>
            <li><a href="<?php echo base_url().'admin/portfolio'?>"><i class="fa fa-list"></i> Portfolio List</a></li>
          </ul>
        </li>

        <li>
          <a href="<?php echo base_url().'admin/pengguna'?>">
            <i class="fa fa-users"></i> <span>Pengguna</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-camera"></i>
            <span>Gallery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/album'?>"><i class="fa fa-clone"></i> Album</a></li>
            <li ><a href="<?php echo base_url().'admin/galeri'?>"><i class="fa fa-picture-o"></i> Photos</a></li>
          </ul>
        </li>

        <li>
          <a href="<?php echo base_url().'admin/komentar'?>">
            <i class="fa fa-comment"></i> <span>Komentar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_komentar;?></small>
            </span>
          </a>
        </li>
        
        <li>
          <a href="<?php echo base_url().'admin/inbox'?>">
            <i class="fa fa-envelope"></i> <span>Inbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_pesan;?></small>
            </span>
          </a>
        </li>

         <li>
          <a href="<?php echo base_url().'administrator/logout'?>">
            <i class="fa fa-sign-out"></i> <span>Sign Out</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        
        

        
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>