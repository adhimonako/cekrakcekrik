
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    // var_dump($this->data['controller_title']);
    $this->load->view('layout/section_header',$this->data);
    ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                <?=$controller_title?>
              </h3>
              
            </div>
            <div class="col-xs-12">
            <a href="<?=base_url().'index.php/'.$controller_class.'/formitinerary'?>" class="btn btn-success btn-sm" >Tambah</a>
            </div>

            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="tb_datapaket" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Paket</th>
                  <th>Costumer</th>
                  <th>Jumlah Peserta</th>
                  <th>Tgl. Keberangkatan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($tampilkan as $isipaket) {
                  $no++;
                  ?>
                <tr>
                  <td><?php echo $no ; ?></td>
                  <td><?php echo $isipaket->namapaket; ?></td>
                  <td><?php echo $isipaket->namacustomer; ?></td>
                  <td><?php echo $isipaket->jumlahpeserta; ?></td>
                  <td><?php echo $isipaket->tglkeberangkatan; ?></td>
                  <td>
                    <a href="<?php echo base_url(); ?>index.php/<?=$controller_class?>/formitinerary/<?=$isipaket->iditinerary ?>" class="btn btn-warning btn-sm"  >Ubah</a>
                    <button type="button" class="btn btn-success btn-sm cetakitil" data-toggle="modal" data-id="<?php echo $isipaket->iditinerary; ?>" >Cetak</button>
                    <a href="<?php echo base_url(); ?>index.php/<?=$controller_class?>/delete/<?=$isipaket->iditinerary ?>" onclick="return confirm('Yakin Dihapus ?');"><button type="button" class="btn btn-sm btn-danger">Hapus</button>
                  </td>
                </tr>
                <?php
                }
                ?>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- Modal tambah paket-->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Tambah Itinerary</h4>
      </div>
      <div class="modal-body">

        <div class="box-body">
          <!-- <form name="uploadfoto" method="post" action="<?= base_url() ?>index.php/admin/cont_datapaket/upload" enctype="multipart/form-data" >
            <div class="form-group">
              <label>Foto</label><p></p>
              <input class="form-control"  type="file" name="foto_paket" onchange="submit();" />
              <img src="<?php base_url(); ?>admin/dist/img/paket2-160x160.jpg">

            </div>
            </form> -->
            <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/admin/cont_datapaket/addpaket">
            <div class="form-group">
              <label for="namapaket">Nama Paket :</label>
              <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket">
            </div>
            <div class="form-group">
              <label for="idjenispaket">Jenis Paket :</label>
              <select name="idjenispaket">
                <?php
                  foreach ($tampilkanjenis as $jenispaket) {
                    ?>
                  <option value="<?php echo $jenispaket->idjenispaket ?>"> <?php echo $jenispaket->namajenispaket ?> </option>
                  <?php
                  }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label for="detailpaket">Detail Paket :</label>
              <input required="harus diisi" type="text" name="detailpaket" class="form-control" id="detailpaket" placeholder="Masukkan Detail paket">
            </div>
            <div class="form-group">
              <label for="hargaperpax">Harga / Pax :</label>
              <input required="harus diisi" type="text" name="hargaperpax" class="form-control" id="hargaperpax" placeholder="Masukkan Harga / Pax">
            </div>
            <div class="form-group">
              <label for="minimalpeserta">Minimal Peserta :</label>
              <input required="harus diisi" type="text" name="minimalpeserta" class="form-control" id="minimalpeserta" placeholder="Masukkan Minimal Peserta">
            </div>

            <button type="submit" class="btn btn-default">Simpan</button>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- Modal pop up ubah-->
<form target="_blank" id="form_itil" action="<?=base_url().'index.php/'.$controller_class.'/cetakitinerary'?>" method="POST" style="display: none;"> 
  
</form>
<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> Data Itinerary
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->

  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script>
  var controller_class = "<?=$controller_class?>";
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row
      window.location = "<?php echo base_url(); ?>index.php/<?=$controller_class?>/formitinerary/"+uid;

    });
    
  });


$('#tb_datapaket').dataTable( {
    paging: true,
    // searching: false
} );

$(function(){
  $('.cetakitil').click(function(){
    var id = $(this).attr("data-id");
    $('#form_itil').attr("action","<?php echo base_url(); ?>index.php/<?=$controller_class?>/cetakitinerary/"+id);
    $('#form_itil').submit();
  });
});
</script>