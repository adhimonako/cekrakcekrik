
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    // var_dump($this->data['controller_title']);
    $this->load->view('layout/section_header',$this->data);
    ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                <?=$controller_title?>
              </h3>
              
            </div>
            <?php
              if($this->session->flashdata('add_success')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_success');?>
              </div>
            </div>
            <?php
              }
              if($this->session->flashdata('add_failed')){
            ?>
            <br><br>
            <div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('add_failed');?>
              </div>
            </div>
            <?php
              }
            ?>

            <!-- /.box-header -->
            <div class="box-body">
                <form name="tambahpaket" id="form_data" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/addsave">
                    <div class="col-md-6">
                      <?php
                      if(empty($iditinerary)){
                      ?>
                      <div class="form-group">
                        <label>
                          Customer
                        </label>
                        <input type="text" name="idcustomer" id="idcustomer" class="form-control">
                      </div>
                      <div class="form-group">
                        <label for="idreservasi">Reservasi :</label>
                        <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
                        <select name="idreservasi" id="idreservasi" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                          <option value=""> -- Pilih Customer Dahulu -- </option>
                        </select>
                      </div>      
                      <div class="form-group">
                        <label for="idjobsheet">Jobsheet :</label>
                        <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
                        <select name="idjobsheet" id="idjobsheet" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                          <option value=""> -- Pilih Reservasi Dahulu -- </option>
                        </select>
                      </div>
                      <?php
                      }else{
                      ?>
                      <div class="form-group">
                        <label>
                          Customer
                        </label>
                        <input type="text" disabled="" value="<?=$detail['namacustomer']?>" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>
                          Reservasi
                        </label>
                        <input type="text" disabled="" value="<?=$detail['namapaket']?>" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>
                          Jobsheet
                        </label>
                        <input type="text" disabled="" value="<?=$detail['kodejobsheet']?>" class="form-control">
                      </div>
                      <?php
                      }
                      ?>                   
                    </div>
                    <div class="col-md-6">
                    </div>
                    <input type="hidden" name="iditinerary" id="iditinerary" value="<?=$iditinerary?>">
                </form>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <?php
                if(empty($iditinerary)){
                ?>
                <a href="javascript:void(0)" class="btn btn-success btn-md" id="btnSave">
                  <i class="fa fa-save"> Simpan</i>
                </a>                
                <?php
                }
                ?>
                <a href="<?=base_url().'index.php/'.$controller_class?>" class="btn btn-danger btn-md">
                  <i class="fa fa-arrow-left"> Kembali</i>
                </a>                
              </div>
            </div>
          </div>
          <?php
          if(!empty($iditinerary)){
          ?>
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h4>Detail Itinerary</h4>
            </div>
            <div class="box-body">
              <div class="col-md-8">
                <div class="table-responsive">
                  <table id="data-detail" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>waktumulai</th>
                        <th>waktuselesai</th>
                        <th>Deskripsi</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody id="bodyDetail">

                      </tbody>
                  </table>
                </div>
              </div>
              <div class="col-md-4">
                <form name="detailjob" id="detailjob">
                  <div class="form-group">
                    <label for="deskripsi">waktumulai:</label>
                    <input required="harus diisi" type="text" name="waktumulai" data-inputmask="##:##:##" class="form-control" id="waktumulai" >
                  </div>                           
                  <div class="form-group">
                    <label for="deskripsi">waktuselesai:</label>
                    <input required="harus diisi" type="text" name="waktuselesai" class="form-control" id="waktuselesai" >
                  </div>                           
                  <div class="form-group">
                    <label for="deskripsi">Deskripsi:</label>
                    <input required="harus diisi" type="text" name="deskripsi" class="form-control" id="deskripsi" >
                  </div>         
                  <div class="form-group">
                    <label for="nominal">Keterangan:</label>
                    <input required="harus diisi" type="text" name="keterangan" class="form-control" id="keterangan" >
                  </div>         
                  <input type="hidden" name="iditinerary" value="<?=$iditinerary?>">
                  <input type="hidden" name="idjobsheet" value="<?=$detail['idjobsheet']?>">
                </form>                
                <button class="btn btn-md btn-success pull-right" type="button" id="btnAddDetail">Simpan</button>
              </div>
            </div>
          </div>
          <?php
          }
          ?>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

<!--dinamik modal-->
<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
             <div class="modal-dialog"> 
                  <div class="modal-content"> 
                  
                       <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">
                              <i class="glyphicon glyphicon-edit"></i> Data Itinerary
                            </h4> 
                       </div> 
                       <div class="modal-body"> 
                       
                           <div id="modal-loader" style="display: none; text-align: center;">
                            <img src="ajax-loader.gif">
                           </div>
                            
                           <!-- content will be load here -->                          
                           <div id="dynamic-content"></div>
                             
                        </div> 
                        <div class="modal-footer"> 
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                        </div> 
                        
                 </div> 
              </div>
       </div><!-- /.modal -->    
    
    </div>

  <!-- /.content-wrapper -->

  <!-- DataTables -->
<script src="<?php base_url(); ?>admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url(); ?>admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
  

<script type='text/javascript'>
    $(function(){
        $('#idcustomer').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            selectresbycust(ui.item.key);
          },
        });

        $('#idreservasi').change(function(){
          selectresjbst($(this).val());
        });
    });
</script>
<script>
  var controller_class = "<?=$controller_class?>";
  $(document).ready(function(){
    
    $(document).on('click', '#getpaket', function(e){
      
      e.preventDefault();
      
      var uid = $(this).data('id');   // it will get id of clicked row

      $('#dynamic-content').html(''); // leave it blank before ajax call
      $('#modal-loader').show();      // load ajax loader
      
      $.ajax({
        url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetail/",
        type: 'POST',
        data: 'id='+uid,
        dataType: 'html'
      })
      .done(function(data){
        console.log(data);  
        $('#dynamic-content').html('');    
        $('#dynamic-content').html(data); // load response 
        $('#modal-loader').hide();      // hide ajax loader 
      })
      .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
        $('#modal-loader').hide();
      });
      
    });
    
  });

$(function(){
  $('#btnSave').click(function(){
    $('#form_data').submit();
  });
  $('#waktumulai').inputmask({ mask: "99:99:99"});
  $('#waktuselesai').inputmask({ mask: "99:99:99"});
})

$('#data-detail').dataTable( {
    paging: true,
    searching: false
} );

</script>

<script type="text/javascript">
  
<?php
if (!empty($iditinerary)) {
?>
$(function(){
  getTableDetail();
  $('#btnAddDetail').click(function(){
    saveDetail();
  });
});

function getTableDetail(){
  var idjobsheet = "<?=$iditinerary?>";
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/getdetailjob/",
    type : 'POST',
    data: {id : idjobsheet},
    success : function(data){
      $('#bodyDetail').empty();
      $('#bodyDetail').html(data);
      $('.delDetail').click(function(){
        var idd = $(this).attr('data-id');
        if (confirm('Yakin Dihapus ?')) {
          delDetail(idd);
        }
      });
    }
  });
}

function saveDetail(){
  var form = $('#detailjob').serialize();
  console.log(form);
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/savedetail/",
    type : 'POST',
    data: form,
    dataType : 'JSON',
    success : function(data){
        $('#waktumulai').val('');
        $('#waktuselesai').val('');
        $('#deskripsi').val('');
        $('#keterangan').val('');
      if(data.status){
        getTableDetail();
      }else{
        alert('Gagal menambahkan data.');
      }
    }
  });
}
function delDetail(id=''){
  $.ajax({
    url: "<?php echo base_url(); ?>"+"index.php/"+controller_class+"/deletedetail/"+id,
    type : 'POST',
    data: {id : id},
    dataType : 'JSON',
    success : function(data){
      if(data.status){
        getTableDetail();
      }else{
        alert('Gagal hapus data.');
      }
    }
  });
}

<?php
}
?>


  function selectresbycust(user=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectrsv"+'/'+user,
      success: function( data ) {
        $('#idreservasi').empty();
        $('#idreservasi').html(data);
      },
    });
  }

  function selectresjbst(user=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectjbst"+'/'+user,
      success: function( data ) {
        $('#idjobsheet').empty();
        $('#idjobsheet').html(data);
      },
    });
  }

  $(function(){
    $('#filter_status').on('change',function(){
      var status = $(this).val();
      // alert(status);
      $('#form_list').submit();
    });
  })

</script>