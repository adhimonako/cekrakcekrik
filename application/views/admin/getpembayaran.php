<?php
// var_dump($detail);
?>
		<div class="table-responsive">
    <form name="tambahpaket" method="post" action="<?= base_url() ?>index.php/<?=$controller_class?>/updatesave" enctype="multipart/form-data">
        <input type="hidden" name="idpembayaran" value="<?php echo $detail['idpembayaran']; ?>">
            <div class="form-group">
              <label for="idcustomer">Customer</label>
              <input type="text" name="idcustomer" id="idcustomere" class="form-control" value="<?=$detail['namacustomer']?>">
            </div>
            <div class="form-group">
              <label for="idreservasi">Reservasi :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idreservasi" id="idreservasie" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Customer Dahulu -- </option>
              </select>
            </div>      
            <div class="form-group">
              <label for="idjobsheet">Jobsheet :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idjobsheet" id="idjobsheete" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Jobsheet -- </option>
              </select>
            </div>      
            <div class="form-group">
              <label for="idtagihan">Tagihan :</label>
              <!-- <input required="harus diisi" type="text" name="namapaket" class="form-control" id="namapaket" placeholder="Masukkan Nama paket"> -->
              <select name="idtagihan" id="idtagihane" class="form-control" <?=(empty($iditinerary)? '' : 'disabled="disabled" ')?> >
                <option value=""> -- Pilih Jobsheet Dahulu -- </option>
              </select>
            </div>    
            <div class="form-group">
              <label for="nominal">Nominal :</label>
              <input required="harus diisi" type="text" name="nominal" class="form-control" id="nominal" placeholder="Masukkan Nominal" value="<?=$detail['nominal']?>">
            </div>            <div class="form-group">
              <label for="tglbayar">Tanggal Bayar :</label>
              <input required="harus diisi" type="text" name="tglbayar" class="form-control datepicker" id="tglbayar" placeholder="Masukkan Tanggal Bayar" value="<?=$detail['tglbayar']?>">
            </div>
            <div class="form-group">
                <label>Bukti Bayar</label>
                <input type="file" name="filefoto" style="width: 100%;" >
              </div>
            <input type="submit" value="Simpan" class="btn btn-default" />
          </form>
		</div>


<script type='text/javascript'>
    $(function(){
        $('#idcustomere').autocomplete({
          source :"<?php echo base_url();?>autocomplete/search/customer",
          select: function( event, ui ) {
            $('#idcustomere').val(ui.item.key);
            selectresbycuste(ui.item.key);

          },
        });

        $('#idreservasie').change(function(){
          selectresjbste($(this).val());
        });
    });


  function selectresbycuste(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectrsv"+'/'+user+'/'+val,
      success: function( data ) {
        $('#idreservasie').empty();
        $('#idreservasie').html(data);
      },
    });
  }

  function selectresjbste(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selectjbst"+'/'+user+'/'+val,
      success: function( data ) {
        $('#idjobsheete').empty();
        $('#idjobsheete').html(data);
      },
    });
  }

  function selecttagihane(user='',val=''){
    $.ajax({
      url: "<?php echo base_url();?>autocomplete/selecttagihan"+'/'+user+'/'+val+'/all',
      success: function( data ) {
        $('#idtagihane').empty();
        $('#idtagihane').html(data);
      },
    });
  }

</script>


<script type="text/javascript">
  var idcust = "<?=$detail['idcustomer']?>";
  var idres = "<?=$detail['idreservasi']?>";
  var idjb = "<?=$detail['idjobsheet']?>";
  var idtag = "<?=$detail['idtagihan']?>";
  $(function(){
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        dateFormat: "yy-mm-dd"
    });
    selectresbycuste(idcust,idres);
    selectresjbste(idcust,idjb);
    selecttagihane(idjb,idtag);
  });
</script>