<!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.0 
    </div>
    <?php
    if($this->session->userdata('level') == "Operator"){
    ?>
    <center><strong><a href="#">CV. Cekrakcekrik</a></strong></center>
    <?php
	}
	else{
    ?>
    <strong>Copyright &copy; 2018-2019 <a href="#">CV. Cekrakcekrik</a>.</strong> All rights
    reserved.
    <?php
	}
    ?>
  </footer>

<script type="text/javascript">
  $(function(){
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        dateFormat: "yy-mm-dd",
        minDate: 0
    });
  });
</script>