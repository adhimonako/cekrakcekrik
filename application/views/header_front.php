	<style type="text/css">
		.usermenu {
			display: block !important;
			padding: 3px 20px !important;
			clear: both !important;
			font-weight: normal !important;
			line-height: 1.42857 !important;
			color: #333333 !important;
			white-space: nowrap !important;
		}
	</style>
	<header id="fh5co-header" >
		<div class="container">
			<div class="header-inner">
		     	
				<h1><a href="<?php echo base_url().''?>">CekrakCekrik<span>.</span>Site</a></h1>
				<nav role="navigation" class="navbar">
					<ul >
						<li <?=((($this->uri->segment(1) == 'home') or ($this->uri->segment(1) == ''))? 'class="active"' : '')?>><a href="<?php echo base_url().''?>">Home</a></li>
						<li <?=(($this->uri->segment(1) == 'about')? 'class="active"' : '')?>><a href="<?php echo base_url().'about'?>">About</a></li>
						<li <?=(($this->uri->segment(1) == 'paket')? 'class="active"' : '')?>><a href="<?php echo base_url().'paket'?>">Paket</a></li>
						<li <?=(($this->uri->segment(1) == 'portfolio')? 'class="active"' : '')?>><a href="<?php echo base_url().'portfolio'?>">Portfolio</a></li>
						<li <?=(($this->uri->segment(1) == 'artikel')? 'class="active"' : '')?>><a href="<?php echo base_url().'artikel'?>">Blog</a></li>
						<li <?=(($this->uri->segment(1) == 'gallery')? 'class="active"' : '')?>><a href="<?php echo base_url().'gallery'?>">Gallery</a></li>
						<li <?=(($this->uri->segment(1) == 'kontak')? 'class="active"' : '')?>><a href="<?php echo base_url().'kontak'?>">Contact</a></li>
						<?php
						if(!empty($userdata)){
						?>
						<li <?=(($this->uri->segment(1) == 'reservasi')? 'class="active"' : '')?>><a href="<?php echo base_url().'reservasi'?>">Reservasi</a></li>

						<li class="dropdown ">
							<a href="javascript:void(0)" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
								<?php
								echo $userdata['nama'];
								?>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li <?=(($this->uri->segment(1) == 'history')? 'class="active"' : '')?>>
									<a class="usermenu" role="menuitem" href="<?php echo base_url().'history'?>">Riwayat Transaksi</a>
								</li>
								<li <?=(($this->uri->segment(1) == 'tagihan')? 'class="active"' : '')?>>
									<a  class="usermenu" role="menuitem" href="<?php echo base_url().'tagihan'?>">Tagihan</a>
								</li>
								<li <?=(($this->uri->segment(1) == 'pesan')? 'class="active"' : '')?>>
									<a  class="usermenu" role="menuitem" href="<?php echo base_url().'pesan'?>">Message</a>
								</li>
								<li <?=(($this->uri->segment(1) == 'logout')? 'class="active"' : '')?>>
									<a class="usermenu" role="menuitem" href="<?php echo base_url().'login/logout'?>">Logout</a>
								</li>
															
						    </ul>
						</li>
						<?php
						}else{
						?>
						<li class="cta">
							<a href="<?php echo base_url().'register'?>">Register</a>
							<a href="<?php echo base_url().'login'?>">Login</a>
						</li>
						<?php
						}
						?>
					</ul>
				</nav>
			</div>
		</div>
	</header>
